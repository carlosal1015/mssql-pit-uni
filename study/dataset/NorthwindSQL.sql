USE [master]
GO
IF EXISTS
(
	SELECT name
FROM sysdatabases
WHERE name='NorthWindSQL'
)
DROP DATABASE NorthWindSQL
GO
CREATE DATABASE [NorthWindSQL]
 ON  PRIMARY 
(
	NAME = N'NorthWindSQL', 
	FILENAME = N'c:\db\NorthWindSQL.mdf' , 
	SIZE = 30MB, 
	MAXSIZE = UNLIMITED, 
	FILEGROWTH = 10MB
)
LOG ON 
( 
	NAME = N'NorthWindSQL_log', 
	FILENAME = N'c:\db\NorthWindSQL_log.ldf' , 
	SIZE = 10MB, 
	MAXSIZE = UNLIMITED, 
	FILEGROWTH = 10%
)
GO
USE [NorthWindSQL]
GO
/****** Object:  Schema [Almacen]    Script Date: 15/03/2015 07:19:10 a.m. ******/
CREATE SCHEMA [Almacen]
GO
/****** Object:  Schema [Cobranzas]    Script Date: 15/03/2015 07:19:10 a.m. ******/
CREATE SCHEMA [Cobranzas]
GO
/****** Object:  Schema [Compras]    Script Date: 15/03/2015 07:19:10 a.m. ******/
CREATE SCHEMA [Compras]
GO
/****** Object:  Schema [Facturacion]    Script Date: 15/03/2015 07:19:10 a.m. ******/
CREATE SCHEMA [Facturacion]
GO
/****** Object:  Schema [Personal]    Script Date: 15/03/2015 07:19:10 a.m. ******/
CREATE SCHEMA [Personal]
GO
/****** Object:  Schema [Sistemas]    Script Date: 15/03/2015 07:19:10 a.m. ******/
CREATE SCHEMA [Sistemas]
GO
/****** Object:  Schema [Transporte]    Script Date: 15/03/2015 07:19:10 a.m. ******/
CREATE SCHEMA [Transporte]
GO
/****** Object:  Schema [Ventas]    Script Date: 15/03/2015 07:19:10 a.m. ******/
CREATE SCHEMA [Ventas]
GO
/****** Object:  Table [Almacen].[Producto]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Almacen].[Producto]
(
	[cod_prv_vc] [varchar](255) NULL,
	[cod_prd_in] [int] IDENTITY(1,1) NOT NULL,
	[cod_prd_vc] [varchar](25) NULL,
	[nom_prd_vc] [varchar](100) NULL,
	[des_prd_vc] [varchar](500) NULL,
	[pre_com_prd_mo] [money] NULL,
	[pre_ven_prd_mo] [money] NULL,
	[pto_ped_prd_si] [smallint] NULL,
	[niv_obj_prd_in] [int] NULL,
	[cnt_uni_prd_vc] [varchar](50) NULL,
	[est_prd_bi] [bit] NULL,
	[cnt_min_rep_prd_si] [smallint] NULL,
	[nom_cat_prd_vc] [varchar](50) NULL,
	[dat_adj_prd_vc] [varchar](8000) NULL,
	CONSTRAINT [pk_almacen_producto_cod_prd_in] PRIMARY KEY CLUSTERED 
(
	[cod_prd_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Almacen].[TipoTransaccionInventario]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Almacen].[TipoTransaccionInventario]
(
	[cod_tip_tra_inv_ti] [tinyint] NOT NULL,
	[nom_tip_tran_inv_vc] [varchar](50) NOT NULL,
	CONSTRAINT [pk_almacen_tipotransaccioninventario_cod_tip_tra_inv_ti] PRIMARY KEY CLUSTERED 
(
	[cod_tip_tra_inv_ti] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Almacen].[TransaccionInventario]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Almacen].[TransaccionInventario]
(
	[cod_tra_inv_in] [int] IDENTITY(1,1) NOT NULL,
	[cod_tip_tra_inv_ti] [tinyint] NOT NULL,
	[fec_cre_tra_inv_dt] [datetime] NULL,
	[fec_mod_tra_inv_dt] [datetime] NULL,
	[cod_prd_in] [int] NOT NULL,
	[cnt_prd_in] [int] NULL,
	[cod_ped_com_in] [int] NULL,
	[cod_ped_in] [int] NULL,
	[obs_tra_inv_vc] [varchar](8000) NULL,
	CONSTRAINT [pk_almacen_transaccioninventario_cod_tra_inv_in] PRIMARY KEY CLUSTERED 
(
	[cod_tra_inv_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Compras].[EstadoPedidoCompra]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Compras].[EstadoPedidoCompra]
(
	[cod_est_ped_com_ti] [tinyint] NOT NULL,
	[nom_est_ped_com_vc] [varchar](50) NOT NULL,
	CONSTRAINT [pk_compras_estadopedidocompra_cod_est_ped_com_ti] PRIMARY KEY CLUSTERED 
(
	[cod_est_ped_com_ti] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Compras].[PedidoCompra]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Compras].[PedidoCompra]
(
	[cod_ped_com_in] [int] IDENTITY(1,1) NOT NULL,
	[cod_prv_in] [int] NULL,
	[cod_emp_in] [int] NULL,
	[fec_env_ped_com_dt] [datetime] NULL,
	[fec_cre_ped_com_dt] [datetime] NULL,
	[cod_est_ped_com_ti] [tinyint] NULL,
	[fec_rec_ped_com_dt] [datetime] NULL,
	[gas_env_ped_com_mo] [money] NULL,
	[imp_ped_com_mo] [money] NULL,
	[fec_pag_ped_com_dt] [datetime] NULL,
	[imp_pag_ped_com_mo] [money] NULL,
	[tip_pag_ped_com_vc] [nvarchar](50) NULL,
	[obs_ped_com_vc] [ntext] NULL,
	[cod_emp_apr_ped_com_in] [int] NULL,
	[fec_apr_ped_com_dt] [datetime] NULL,
	[cod_emp_env_ped_com_in] [int] NULL,
	CONSTRAINT [pk_compras_pedidocompra_cod_ped_com_in] PRIMARY KEY CLUSTERED 
(
	[cod_ped_com_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [Compras].[PedidoCompraDetalle]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Compras].[PedidoCompraDetalle]
(
	[cod_ped_com_det_in] [int] IDENTITY(1,1) NOT NULL,
	[cod_ped_com_in] [int] NULL,
	[cod_prd_in] [int] NULL,
	[cnt_ped_com_det_de] [decimal](18, 4) NULL,
	[cos_uni_ped_com_det_mo] [money] NULL,
	[fec_rec_ped_com_det_dt] [datetime] NULL,
	[pub_tra_inv_ped_com_det_bi] [bit] NULL,
	[cod_tra_inv_in] [int] NULL,
	CONSTRAINT [pk_compras_pedidocompradetalle_cod_ped_com_det_in] PRIMARY KEY CLUSTERED 
(
	[cod_ped_com_det_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Compras].[Proveedor]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Compras].[Proveedor]
(
	[cod_prv_in] [int] IDENTITY(1,1) NOT NULL,
	[raz_soc_prv_vc] [varchar](100) NULL,
	[pat_prv_vc] [varchar](50) NULL,
	[nom_prv_vc] [varchar](40) NULL,
	[ema_prv_vc] [varchar](50) NULL,
	[car_prv_vc] [varchar](50) NULL,
	[tel_tra_prv_vc] [varchar](25) NULL,
	[tel_par_prv_vc] [varchar](50) NULL,
	[tel_mov_prv_vc] [varchar](50) NULL,
	[fax_prv_vc] [varchar](50) NULL,
	[dir_prv_vc] [varchar](100) NULL,
	[ciu_prv_vc] [varchar](50) NULL,
	[prv_prv_vc] [varchar](50) NULL,
	[cod_pos_prv_vc] [varchar](15) NULL,
	[pai_prv_vc] [varchar](50) NULL,
	[web_prv_vc] [varchar](100) NULL,
	[obs_prv_vc] [varchar](8000) NULL,
	[dat_adj_prv_vc] [varchar](8000) NULL,
	CONSTRAINT [pk_compras_proveedor_cod_prv_in] PRIMARY KEY CLUSTERED 
(
	[cod_prv_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente]
(
	[cod_cli_in] [int] NULL,
	[nom_cli_vc] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cuerda]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cuerda]
(
	[cod_hlp_in] [int] IDENTITY(1,1) NOT NULL,
	[des_hlp_vc] [varchar](255) NULL,
	CONSTRAINT [pk_dbo_cuerda_cod_hlp_in] PRIMARY KEY CLUSTERED 
(
	[cod_hlp_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EstadoPedido]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoPedido]
(
	[cod_est_ped_ti] [tinyint] NOT NULL,
	[nom_est_ped_vc] [varchar](50) NULL,
	CONSTRAINT [pk_dbo_estadopedido_cod_est_ped_ti] PRIMARY KEY CLUSTERED 
(
	[cod_est_ped_ti] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EstadoPedidoDetalle]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoPedidoDetalle]
(
	[cod_est_ped_det_ti] [tinyint] NOT NULL,
	[nom_est_ped_det_vc] [varchar](50) NULL,
	CONSTRAINT [pk_dbo_estadopedidodetalle_cod_est_ped_det_ti] PRIMARY KEY CLUSTERED 
(
	[cod_est_ped_det_ti] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Facturacion].[EstadoPedidoImpuesto]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Facturacion].[EstadoPedidoImpuesto]
(
	[cod_est_ped_imp_ti] [tinyint] NOT NULL,
	[nom_est_ped_imp_vc] [varchar](50) NOT NULL,
	CONSTRAINT [pk_facturacion_estadopedidoimpuesto_cod_est_ped_imp_ti] PRIMARY KEY CLUSTERED 
(
	[cod_est_ped_imp_ti] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Facturacion].[Factura]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Facturacion].[Factura]
(
	[cod_fac_in] [int] IDENTITY(1,1) NOT NULL,
	[cod_ped_in] [int] NULL,
	[fec_ped_dt] [datetime] NULL,
	[fec_ven_ped_dt] [datetime] NULL,
	[imp_ped_mo] [money] NULL,
	[cos_env_ped_mon] [money] NULL,
	[imp_tot_ped_mon] [money] NULL,
	CONSTRAINT [pk_facturacion_factura_cod_fac_in] PRIMARY KEY CLUSTERED 
(
	[cod_fac_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Personal].[Empleado]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Personal].[Empleado]
(
	[cod_emp_in] [int] IDENTITY(1,1) NOT NULL,
	[raz_soc_emp_vc] [varchar](100) NULL,
	[pat_emp_vc] [varchar](30) NULL,
	[nom_emp_vc] [varchar](40) NULL,
	[ema_emp_vc] [varchar](50) NULL,
	[car_emp_vc] [varchar](50) NULL,
	[tel_tra_emp_vc] [varchar](25) NULL,
	[tel_par_emp_vc] [varchar](50) NULL,
	[tel_mov_emp_vc] [varchar](50) NULL,
	[fax_emp_vc] [varchar](50) NULL,
	[dir_emp_vc] [varchar](200) NULL,
	[ciu_emp_vc] [varchar](50) NULL,
	[prov_emp_vc] [varchar](50) NULL,
	[cod_pos_emp_vc] [varchar](15) NULL,
	[pai_emp_vc] [varchar](50) NULL,
	[web_emp_vc] [varchar](100) NULL,
	[obs_emp_vc] [varchar](8000) NULL,
	[dat_adj_emp_vc] [varchar](8000) NULL,
	CONSTRAINT [pk_personal_empleado_cod_emp_in] PRIMARY KEY CLUSTERED 
(
	[cod_emp_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Sistemas].[Privilegio]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Sistemas].[Privilegio]
(
	[cod_pri_in] [int] IDENTITY(1,1) NOT NULL,
	[nom_pri_vc] [varchar](50) NOT NULL,
	CONSTRAINT [pk_sistemas_privilegio_cod_pri_in] PRIMARY KEY CLUSTERED 
(
	[cod_pri_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Sistemas].[PrivilegioEmpleado]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Sistemas].[PrivilegioEmpleado]
(
	[cod_emp_in] [int] NOT NULL,
	[cod_pri_in] [int] NOT NULL,
	CONSTRAINT [pk_sistemas_privilegioempleado_cod_emp_in_cod_pri_in] PRIMARY KEY CLUSTERED 
(
	[cod_emp_in] ASC,
	[cod_pri_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Transporte].[Transportista]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Transporte].[Transportista]
(
	[cod_tra_in] [int] IDENTITY(1,1) NOT NULL,
	[raz_soc_tra_vc] [varchar](100) NULL,
	[pat_tra_vc] [varchar](30) NULL,
	[nom_tra_vc] [varchar](40) NULL,
	[ema_tra_vc] [varchar](50) NULL,
	[car_tra_vc] [varchar](50) NULL,
	[tel_tra_tra_vc] [varchar](25) NULL,
	[tel_par_tra_vc] [varchar](25) NULL,
	[tel_mov_tra_vc] [varchar](25) NULL,
	[fax_tra_vc] [varchar](25) NULL,
	[dir_tra_vc] [varchar](200) NULL,
	[ciu_tra_vc] [varchar](50) NULL,
	[prv_tra_vc] [varchar](50) NULL,
	[cod_pos_tra_vc] [varchar](15) NULL,
	[pai_tra_vc] [varchar](50) NULL,
	[web_tra_vc] [varchar](100) NULL,
	[obs_tra_vc] [varchar](8000) NULL,
	[dat_adj_tra_vc] [varchar](8000) NULL,
	CONSTRAINT [pk_transporte_transportista_cod_tra_in] PRIMARY KEY CLUSTERED 
(
	[cod_tra_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Ventas].[Cliente]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Ventas].[Cliente]
(
	[cod_cli_in] [int] IDENTITY(1,1) NOT NULL,
	[raz_soc_cli_vc] [varchar](100) NULL,
	[pat_cli_vc] [varchar](30) NULL,
	[nom_cli_vc] [varchar](40) NULL,
	[ema_cli_vc] [varchar](50) NULL,
	[car_cli_vc] [varchar](50) NULL,
	[tel_tra_cli_vc] [varchar](50) NULL,
	[tel_par_cli_vc] [varchar](50) NULL,
	[tel_mov_cli_vc] [varchar](50) NULL,
	[fax_cli_vc] [varchar](50) NULL,
	[dir_cli_vc] [varchar](200) NULL,
	[ciu_cli_vc] [varchar](50) NULL,
	[prv_cli_vc] [varchar](50) NULL,
	[cod_pos_cli_vc] [varchar](50) NULL,
	[pai_cli_vc] [varchar](50) NULL,
	[web_cli_vc] [varchar](50) NULL,
	[obs_cli_vc] [varchar](8000) NULL,
	[dat_adj_cli_vc] [varchar](8000) NULL,
	CONSTRAINT [pk_ventas_cliente_cod_cli_in] PRIMARY KEY CLUSTERED 
(
	[cod_cli_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Ventas].[Informe]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Ventas].[Informe]
(
	[Agrupar por] [nvarchar](50) NULL,
	[Mostrar] [nvarchar](50) NULL,
	[Título] [nvarchar](50) NULL,
	[Origen de fila de filtro] [ntext] NULL,
	[Predeterminado] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [Ventas].[Pedido]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Ventas].[Pedido]
(
	[cod_ped_in] [int] IDENTITY(1,1) NOT NULL,
	[cod_emp_in] [int] NULL,
	[cod_cli_in] [int] NULL,
	[fec_ped_dt] [datetime] NULL,
	[fec_env_ped_dt] [datetime] NULL,
	[cod_tra_in] [int] NULL,
	[nom_env_ped_vc] [varchar](50) NULL,
	[dir_des_env_ped_vc] [varchar](200) NULL,
	[ciu_des_env_ped_vc] [varchar](50) NULL,
	[prv_des_env_ped_vc] [varchar](50) NULL,
	[cod_pos_des_env_ped_vc] [varchar](50) NULL,
	[pai_des_env_ped_vc] [varchar](50) NULL,
	[gas_env_ped_mo] [money] NULL,
	[imp_ped_mo] [money] NULL,
	[tip_pag_ped_vc] [varchar](50) NULL,
	[fec_pag_ped_dt] [datetime] NULL,
	[obs_ped_vc] [varchar](8000) NULL,
	[tip_imp_ped_in] [int] NULL,
	[cod_est_ped_imp_ti] [tinyint] NULL,
	[cod_est_ped_ti] [tinyint] NULL,
	CONSTRAINT [pk_ventas_pedido_cod_ped_in] PRIMARY KEY CLUSTERED 
(
	[cod_ped_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Ventas].[PedidoDetalle]    Script Date: 15/03/2015 07:19:10 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Ventas].[PedidoDetalle]
(
	[cod_ped_det_in] [int] IDENTITY(1,1) NOT NULL,
	[cod_ped_in] [int] NULL,
	[cod_prd_in] [int] NULL,
	[cnt_ped_det_de] [decimal](12, 2) NULL,
	[pre_ven_ped_det_de] [money] NULL,
	[des_ped_det_de] [decimal](12, 2) NULL,
	[cod_est_ped_det_ti] [tinyint] NULL,
	[fec_ent_ped_det_dt] [datetime] NULL,
	[cod_ped_com_in] [int] NULL,
	[cod_tra_inv_in] [int] NULL,
	CONSTRAINT [pk_ventas_pedidodetalle_cod_ped_det_in] PRIMARY KEY CLUSTERED 
(
	[cod_ped_det_in] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [Almacen].[Producto] ON

INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'4', 1, N'NWTB-1', N'Té Chai Northwind Traders', NULL, 13.5000, 18.0000, 10, 40, N'10 cajas x 20 bolsas', 0, 10, N'Bebidas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'10', 3, N'NWTCO-3', N'Almíbar Northwind Traders', NULL, 7.5000, 10.0000, 25, 100, N'12 botellas de 550 ml', 0, 25, N'Condimentos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'10', 4, N'NWTCO-4', N'Condimentos de Louisiana Northwind Traders', NULL, 16.5000, 22.0000, 10, 40, N'48 botes de 180 g', 0, 10, N'Condimentos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'10', 5, N'NWTO-5', N'Aceite de oliva Northwind Traders', NULL, 16.0125, 21.3500, 10, 40, N'36 cajas', 0, 10, N'Aceite', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'2;6', 6, N'NWTJP-6', N'Mermelada de moras y frambuesas Northwind Traders', NULL, 18.7500, 25.0000, 25, 100, N'12 botes de 250 g', 0, 25, N'Mermeladas y confituras', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'2', 7, N'NWTDFN-7', N'Peras secas Northwind Traders', NULL, 22.5000, 30.0000, 10, 40, N'12 paq. de 1/2 kg', 0, 10, N'Frutos secos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'8', 8, N'NWTS-8', N'Salsa curry Northwind Traders', NULL, 30.0000, 40.0000, 10, 40, N'12 botes de 360 g', 0, 10, N'Salsas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'2;6', 14, N'NWTDFN-14', N'Nueces Northwind Traders', NULL, 17.4375, 23.2500, 10, 40, N'40 paq. de 100 g', 0, 10, N'Frutos secos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 17, N'NWTCFV-17', N'Macedonia Northwind Traders', NULL, 29.2500, 39.0000, 10, 40, N'45,75 g', 0, 10, N'Frutas y verduras enlatadas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'1', 19, N'NWTBGM-19', N'Galletas de chocolate surtidas Northwind Traders', NULL, 6.9000, 9.2000, 5, 20, N'10 cajas x 12 piezas', 0, 5, N'Productos horneados', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'2;6', 20, N'NWTJP-6', N'Mermelada Northwind Traders', NULL, 60.7500, 81.0000, 10, 40, N'30 cajas regalo', 0, 10, N'Mermeladas y confituras', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'1', 21, N'NWTBGM-21', N'Bollos de pan Northwind Traders', NULL, 7.5000, 10.0000, 5, 20, N'24 paq. x 4 piezas', 0, 5, N'Productos horneados', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'4', 34, N'NWTB-34', N'Cerveza Northwind Traders', NULL, 10.5000, 14.0000, 15, 60, N'24 botellas de 350 ml', 0, 15, N'Bebidas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'7', 40, N'NWTCM-40', N'Carne de cangrejo Northwind Traders', NULL, 13.8000, 18.4000, 30, 120, N'24 latas de 120 g', 0, 30, N'Carne enlatada', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 41, N'NWTSO-41', N'Sopa de almejas Northwind Traders', NULL, 7.2375, 9.6500, 10, 40, N'12 latas de 360 g', 0, 10, N'Sopas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'3;4', 43, N'NWTB-43', N'Café Northwind Traders', NULL, 34.5000, 46.0000, 25, 100, N'16 latas de 500 g', 0, 25, N'Bebidas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'10', 48, N'NWTCA-48', N'Chocolate Northwind Traders', NULL, 9.5625, 12.7500, 25, 100, N'10 paq.', 0, 25, N'Golosinas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'2', 51, N'NWTDFN-51', N'Manzanas secas Northwind Traders', NULL, 39.7500, 53.0000, 10, 40, N'50 paq. de 300 g', 0, 10, N'Frutos secos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'1', 52, N'NWTG-52', N'Arroz de grano largo Northwind Traders', NULL, 5.2500, 7.0000, 25, 100, N'16 cajas de 2 kg', 0, 25, N'Granos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'1', 56, N'NWTP-56', N'Ñoquis Northwind Traders', NULL, 28.5000, 38.0000, 30, 120, N'24 paq. de 250 g', 0, 30, N'Pasta', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'1', 57, N'NWTP-57', N'Ravioli Northwind Traders', NULL, 14.6250, 19.5000, 20, 80, N'24 paq. de 250 g', 0, 20, N'Pasta', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'8', 65, N'NWTS-65', N'Salsa picante Northwind Traders', NULL, 15.7875, 21.0500, 10, 40, N'32 botellas de 250 ml', 0, 10, N'Salsas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'8', 66, N'NWTS-66', N'Salsa de tomate Northwind Traders', NULL, 12.7500, 17.0000, 20, 80, N'24 botes de 240 g', 0, 20, N'Salsas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'5', 72, N'NWTD-72', N'Mozzarella Northwind Traders', NULL, 26.1000, 34.8000, 10, 40, N'24 paq. de 200 g', 0, 10, N'Productos lácteos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'2;6', 74, N'NWTDFN-74', N'Almendras Northwind Traders', NULL, 7.5000, 10.0000, 5, 20, N'Paq. de 5 kg', 0, 5, N'Frutos secos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'10', 77, N'NWTCO-77', N'Mostaza Northwind Traders', NULL, 9.7500, 13.0000, 15, 60, N'12 cajas', 0, 15, N'Condimentos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'2', 80, N'NWTDFN-80', N'Ciruelas pasas Northwind Traders', NULL, 3.0000, 3.5000, 50, 75, N'Bolsa de 500 g', 0, 25, N'Frutos secos', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'3', 81, N'NWTB-81', N'Té verde Northwind Traders', NULL, 2.0000, 2.9900, 100, 125, N'20 bolsas por caja', 0, 25, N'Bebidas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'1', 82, N'NWTC-82', N'Cereales de avena Northwind Traders', NULL, 2.0000, 4.0000, 20, 100, NULL, 0, NULL, N'Cereales', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'9', 83, N'NWTCS-83', N'Patatas fritas Northwind Traders', NULL, 0.5000, 1.8000, 30, 200, NULL, 0, NULL, N'Patatas y tentempiés', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'1', 85, N'NWTBGM-85', N'Bizcocho Northwind Traders', NULL, 9.0000, 12.4900, 10, 20, N'3 cajas', 0, 5, N'Productos horneados', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'1', 86, N'NWTBGM-86', N'Pastel Northwind Traders', NULL, 10.5000, 15.9900, 10, 20, N'4 cajas', 0, 5, N'Productos horneados', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'7', 87, N'NWTB-87', N'Té Northwind Traders', NULL, 2.0000, 4.0000, 20, 50, N'100 por caja', 0, NULL, N'Bebidas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 88, N'NWTCFV-88', N'Peras Northwind Traders', NULL, 1.0000, 1.3000, 10, 40, N'45,75 g', 0, NULL, N'Frutas y verduras enlatadas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 89, N'NWTCFV-89', N'Melocotones Northwind Traders', NULL, 1.0000, 1.5000, 10, 40, N'45,75 g', 0, NULL, N'Frutas y verduras enlatadas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 90, N'NWTCFV-90', N'Piña Northwind Traders', NULL, 1.0000, 1.8000, 10, 40, N'45,75 g', 0, NULL, N'Frutas y verduras enlatadas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 91, N'NWTCFV-91', N'Relleno para tarta de cerezas Northwind Traders', NULL, 1.0000, 2.0000, 10, 40, N'45,75 g', 0, NULL, N'Frutas y verduras enlatadas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 92, N'NWTCFV-92', N'Judías verdes Northwind Traders', NULL, 1.0000, 1.2000, 10, 40, N'435 g', 0, NULL, N'Frutas y verduras enlatadas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 93, N'NWTCFV-93', N'Maíz Northwind Traders', NULL, 1.0000, 1.2000, 10, 40, N'435 g', 0, NULL, N'Frutas y verduras enlatadas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 94, N'NWTCFV-94', N'Guisantes Northwind Traders', NULL, 1.0000, 1.5000, 10, 40, N'435 g', 0, NULL, N'Frutas y verduras enlatadas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'7', 95, N'NWTCM-95', N'Atún Northwind Traders', NULL, 0.5000, 2.0000, 30, 50, N'150 g', 0, NULL, N'Carne enlatada', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'7', 96, N'NWTCM-96', N'Salmón ahumado Northwind Traders', NULL, 2.0000, 4.0000, 30, 50, N'150 g', 0, NULL, N'Carne enlatada', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'1', 97, N'NWTC-82', N'Copos de avena Northwind Traders', NULL, 3.0000, 5.0000, 50, 200, NULL, 0, NULL, N'Cereales', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 98, N'NWTSO-98', N'Sopa de verduras Northwind Traders', NULL, 1.0000, 1.8900, 100, 200, NULL, 0, NULL, N'Sopas', NULL)
INSERT [Almacen].[Producto]
	([cod_prv_vc], [cod_prd_in], [cod_prd_vc], [nom_prd_vc], [des_prd_vc], [pre_com_prd_mo], [pre_ven_prd_mo], [pto_ped_prd_si], [niv_obj_prd_in], [cnt_uni_prd_vc], [est_prd_bi], [cnt_min_rep_prd_si], [nom_cat_prd_vc], [dat_adj_prd_vc])
VALUES
	(N'6', 99, N'NWTSO-99', N'Sopa de pollo Northwind Traders', NULL, 1.0000, 1.9500, 100, 200, NULL, 0, NULL, N'Sopas', NULL)
SET IDENTITY_INSERT [Almacen].[Producto] OFF
INSERT [Almacen].[TipoTransaccionInventario]
	([cod_tip_tra_inv_ti], [nom_tip_tran_inv_vc])
VALUES
	(1, N'Comprado')
INSERT [Almacen].[TipoTransaccionInventario]
	([cod_tip_tra_inv_ti], [nom_tip_tran_inv_vc])
VALUES
	(2, N'Vendido')
INSERT [Almacen].[TipoTransaccionInventario]
	([cod_tip_tra_inv_ti], [nom_tip_tran_inv_vc])
VALUES
	(3, N'Retenido')
INSERT [Almacen].[TipoTransaccionInventario]
	([cod_tip_tra_inv_ti], [nom_tip_tran_inv_vc])
VALUES
	(4, N'Sobrante')
SET IDENTITY_INSERT [Almacen].[TransaccionInventario] ON

INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(35, 1, CAST(N'2006-03-22 16:02:28.000' AS DateTime), CAST(N'2006-03-22 16:02:28.000' AS DateTime), 80, 75, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(36, 1, CAST(N'2006-03-22 16:02:48.000' AS DateTime), CAST(N'2006-03-22 16:02:48.000' AS DateTime), 72, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(37, 1, CAST(N'2006-03-22 16:03:04.000' AS DateTime), CAST(N'2006-03-22 16:03:04.000' AS DateTime), 52, 100, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(38, 1, CAST(N'2006-03-22 16:03:09.000' AS DateTime), CAST(N'2006-03-22 16:03:09.000' AS DateTime), 56, 120, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(39, 1, CAST(N'2006-03-22 16:03:14.000' AS DateTime), CAST(N'2006-03-22 16:03:14.000' AS DateTime), 57, 80, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(40, 1, CAST(N'2006-03-22 16:03:40.000' AS DateTime), CAST(N'2006-03-22 16:03:40.000' AS DateTime), 6, 100, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(41, 1, CAST(N'2006-03-22 16:03:47.000' AS DateTime), CAST(N'2006-03-22 16:03:47.000' AS DateTime), 7, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(42, 1, CAST(N'2006-03-22 16:03:54.000' AS DateTime), CAST(N'2006-03-22 16:03:54.000' AS DateTime), 8, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(43, 1, CAST(N'2006-03-22 16:04:02.000' AS DateTime), CAST(N'2006-03-22 16:04:02.000' AS DateTime), 14, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(44, 1, CAST(N'2006-03-22 16:04:07.000' AS DateTime), CAST(N'2006-03-22 16:04:07.000' AS DateTime), 17, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(45, 1, CAST(N'2006-03-22 16:04:12.000' AS DateTime), CAST(N'2006-03-22 16:04:12.000' AS DateTime), 19, 20, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(46, 1, CAST(N'2006-03-22 16:04:17.000' AS DateTime), CAST(N'2006-03-22 16:04:17.000' AS DateTime), 20, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(47, 1, CAST(N'2006-03-22 16:04:20.000' AS DateTime), CAST(N'2006-03-22 16:04:20.000' AS DateTime), 21, 20, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(48, 1, CAST(N'2006-03-22 16:04:24.000' AS DateTime), CAST(N'2006-03-22 16:04:24.000' AS DateTime), 40, 120, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(49, 1, CAST(N'2006-03-22 16:04:28.000' AS DateTime), CAST(N'2006-03-22 16:04:28.000' AS DateTime), 41, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(50, 1, CAST(N'2006-03-22 16:04:31.000' AS DateTime), CAST(N'2006-03-22 16:04:31.000' AS DateTime), 48, 100, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(51, 1, CAST(N'2006-03-22 16:04:38.000' AS DateTime), CAST(N'2006-03-22 16:04:38.000' AS DateTime), 51, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(52, 1, CAST(N'2006-03-22 16:04:41.000' AS DateTime), CAST(N'2006-03-22 16:04:41.000' AS DateTime), 74, 20, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(53, 1, CAST(N'2006-03-22 16:04:45.000' AS DateTime), CAST(N'2006-03-22 16:04:45.000' AS DateTime), 77, 60, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(54, 1, CAST(N'2006-03-22 16:05:07.000' AS DateTime), CAST(N'2006-03-22 16:05:07.000' AS DateTime), 3, 100, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(55, 1, CAST(N'2006-03-22 16:05:11.000' AS DateTime), CAST(N'2006-03-22 16:05:11.000' AS DateTime), 4, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(56, 1, CAST(N'2006-03-22 16:05:14.000' AS DateTime), CAST(N'2006-03-22 16:05:14.000' AS DateTime), 5, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(57, 1, CAST(N'2006-03-22 16:05:26.000' AS DateTime), CAST(N'2006-03-22 16:05:26.000' AS DateTime), 65, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(58, 1, CAST(N'2006-03-22 16:05:32.000' AS DateTime), CAST(N'2006-03-22 16:05:32.000' AS DateTime), 66, 80, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(59, 1, CAST(N'2006-03-22 16:05:47.000' AS DateTime), CAST(N'2006-03-22 16:05:47.000' AS DateTime), 1, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(60, 1, CAST(N'2006-03-22 16:05:51.000' AS DateTime), CAST(N'2006-03-22 16:05:51.000' AS DateTime), 34, 60, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(61, 1, CAST(N'2006-03-22 16:06:00.000' AS DateTime), CAST(N'2006-03-22 16:06:00.000' AS DateTime), 43, 100, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(62, 1, CAST(N'2006-03-22 16:06:03.000' AS DateTime), CAST(N'2006-03-22 16:06:03.000' AS DateTime), 81, 125, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(63, 2, CAST(N'2006-03-22 16:07:56.000' AS DateTime), CAST(N'2006-03-24 11:03:00.000' AS DateTime), 80, 30, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(64, 2, CAST(N'2006-03-22 16:08:19.000' AS DateTime), CAST(N'2006-03-22 16:08:59.000' AS DateTime), 7, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(65, 2, CAST(N'2006-03-22 16:08:29.000' AS DateTime), CAST(N'2006-03-22 16:08:59.000' AS DateTime), 51, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(66, 2, CAST(N'2006-03-22 16:08:37.000' AS DateTime), CAST(N'2006-03-22 16:08:59.000' AS DateTime), 80, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(67, 2, CAST(N'2006-03-22 16:09:46.000' AS DateTime), CAST(N'2006-03-22 16:10:27.000' AS DateTime), 1, 15, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(68, 2, CAST(N'2006-03-22 16:10:06.000' AS DateTime), CAST(N'2006-03-22 16:10:27.000' AS DateTime), 43, 20, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(69, 2, CAST(N'2006-03-22 16:11:39.000' AS DateTime), CAST(N'2006-03-24 11:00:55.000' AS DateTime), 19, 20, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(70, 2, CAST(N'2006-03-22 16:11:56.000' AS DateTime), CAST(N'2006-03-24 10:59:41.000' AS DateTime), 48, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(71, 2, CAST(N'2006-03-22 16:12:29.000' AS DateTime), CAST(N'2006-03-24 10:57:38.000' AS DateTime), 8, 17, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(72, 1, CAST(N'2006-03-24 10:41:30.000' AS DateTime), CAST(N'2006-03-24 10:41:30.000' AS DateTime), 81, 200, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(73, 2, CAST(N'2006-03-24 10:41:33.000' AS DateTime), CAST(N'2006-03-24 10:41:42.000' AS DateTime), 81, 200, NULL, NULL, N'Servir producto en lista de espera, pedido nº 40')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(74, 1, CAST(N'2006-03-24 10:53:13.000' AS DateTime), CAST(N'2006-03-24 10:53:13.000' AS DateTime), 48, 100, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(75, 2, CAST(N'2006-03-24 10:53:16.000' AS DateTime), CAST(N'2006-03-24 10:55:46.000' AS DateTime), 48, 100, NULL, NULL, N'Servir producto en lista de espera, pedido nº 39')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(76, 1, CAST(N'2006-03-24 10:53:36.000' AS DateTime), CAST(N'2006-03-24 10:53:36.000' AS DateTime), 43, 300, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(77, 2, CAST(N'2006-03-24 10:53:39.000' AS DateTime), CAST(N'2006-03-24 10:56:57.000' AS DateTime), 43, 300, NULL, NULL, N'Servir producto en lista de espera, pedido nº 38')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(78, 1, CAST(N'2006-03-24 10:54:04.000' AS DateTime), CAST(N'2006-03-24 10:54:04.000' AS DateTime), 41, 200, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(79, 2, CAST(N'2006-03-24 10:54:07.000' AS DateTime), CAST(N'2006-03-24 10:58:40.000' AS DateTime), 41, 200, NULL, NULL, N'Servir producto en lista de espera, pedido nº 36')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(80, 1, CAST(N'2006-03-24 10:54:33.000' AS DateTime), CAST(N'2006-03-24 10:54:33.000' AS DateTime), 19, 30, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(81, 2, CAST(N'2006-03-24 10:54:35.000' AS DateTime), CAST(N'2006-03-24 11:02:02.000' AS DateTime), 19, 30, NULL, NULL, N'Servir producto en lista de espera, pedido nº 33')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(82, 1, CAST(N'2006-03-24 10:54:58.000' AS DateTime), CAST(N'2006-03-24 10:54:58.000' AS DateTime), 34, 100, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(83, 2, CAST(N'2006-03-24 10:55:02.000' AS DateTime), CAST(N'2006-03-24 11:03:00.000' AS DateTime), 34, 100, NULL, NULL, N'Servir producto en lista de espera, pedido nº 30')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(84, 2, CAST(N'2006-03-24 14:48:15.000' AS DateTime), CAST(N'2006-04-04 11:41:14.000' AS DateTime), 6, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(85, 2, CAST(N'2006-03-24 14:48:23.000' AS DateTime), CAST(N'2006-04-04 11:41:14.000' AS DateTime), 4, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(86, 3, CAST(N'2006-03-24 14:49:16.000' AS DateTime), CAST(N'2006-03-24 14:49:16.000' AS DateTime), 80, 20, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(87, 3, CAST(N'2006-03-24 14:49:20.000' AS DateTime), CAST(N'2006-03-24 14:49:20.000' AS DateTime), 81, 50, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(88, 3, CAST(N'2006-03-24 14:50:09.000' AS DateTime), CAST(N'2006-03-24 14:50:09.000' AS DateTime), 1, 25, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(89, 3, CAST(N'2006-03-24 14:50:14.000' AS DateTime), CAST(N'2006-03-24 14:50:14.000' AS DateTime), 43, 25, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(90, 3, CAST(N'2006-03-24 14:50:18.000' AS DateTime), CAST(N'2006-03-24 14:50:18.000' AS DateTime), 81, 25, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(91, 2, CAST(N'2006-03-24 14:51:03.000' AS DateTime), CAST(N'2006-04-04 11:09:24.000' AS DateTime), 40, 50, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(92, 2, CAST(N'2006-03-24 14:55:03.000' AS DateTime), CAST(N'2006-04-04 11:06:56.000' AS DateTime), 21, 20, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(93, 2, CAST(N'2006-03-24 14:55:39.000' AS DateTime), CAST(N'2006-04-04 11:06:13.000' AS DateTime), 5, 25, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(94, 2, CAST(N'2006-03-24 14:55:52.000' AS DateTime), CAST(N'2006-04-04 11:06:13.000' AS DateTime), 41, 30, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(95, 2, CAST(N'2006-03-24 14:56:09.000' AS DateTime), CAST(N'2006-04-04 11:06:13.000' AS DateTime), 40, 30, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(96, 3, CAST(N'2006-03-30 16:46:34.000' AS DateTime), CAST(N'2006-03-30 16:46:34.000' AS DateTime), 34, 12, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(97, 3, CAST(N'2006-03-30 17:23:27.000' AS DateTime), CAST(N'2006-03-30 17:23:27.000' AS DateTime), 34, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(98, 3, CAST(N'2006-03-30 17:24:33.000' AS DateTime), CAST(N'2006-03-30 17:24:33.000' AS DateTime), 34, 1, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(99, 2, CAST(N'2006-04-03 13:50:08.000' AS DateTime), CAST(N'2006-04-03 13:50:15.000' AS DateTime), 48, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(100, 1, CAST(N'2006-04-04 11:00:54.000' AS DateTime), CAST(N'2006-04-04 11:00:54.000' AS DateTime), 57, 100, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(101, 2, CAST(N'2006-04-04 11:00:56.000' AS DateTime), CAST(N'2006-04-04 11:08:49.000' AS DateTime), 57, 100, NULL, NULL, N'Servir producto en lista de espera, pedido nº 46')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(102, 1, CAST(N'2006-04-04 11:01:14.000' AS DateTime), CAST(N'2006-04-04 11:01:14.000' AS DateTime), 34, 50, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(103, 1, CAST(N'2006-04-04 11:01:35.000' AS DateTime), CAST(N'2006-04-04 11:01:35.000' AS DateTime), 43, 250, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(104, 3, CAST(N'2006-04-04 11:01:37.000' AS DateTime), CAST(N'2006-04-04 11:01:37.000' AS DateTime), 43, 300, NULL, NULL, N'Servir producto en lista de espera, pedido nº 41')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(105, 1, CAST(N'2006-04-04 11:01:55.000' AS DateTime), CAST(N'2006-04-04 11:01:55.000' AS DateTime), 8, 25, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(106, 2, CAST(N'2006-04-04 11:01:58.000' AS DateTime), CAST(N'2006-04-04 11:07:37.000' AS DateTime), 8, 25, NULL, NULL, N'Servir producto en lista de espera, pedido nº 48')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(107, 1, CAST(N'2006-04-04 11:02:17.000' AS DateTime), CAST(N'2006-04-04 11:02:17.000' AS DateTime), 34, 300, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(108, 2, CAST(N'2006-04-04 11:02:19.000' AS DateTime), CAST(N'2006-04-04 11:08:14.000' AS DateTime), 34, 300, NULL, NULL, N'Servir producto en lista de espera, pedido nº 47')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(109, 1, CAST(N'2006-04-04 11:02:37.000' AS DateTime), CAST(N'2006-04-04 11:02:37.000' AS DateTime), 19, 25, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(110, 2, CAST(N'2006-04-04 11:02:39.000' AS DateTime), CAST(N'2006-04-04 11:41:14.000' AS DateTime), 19, 10, NULL, NULL, N'Servir producto en lista de espera, pedido nº 42')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(111, 1, CAST(N'2006-04-04 11:02:56.000' AS DateTime), CAST(N'2006-04-04 11:02:56.000' AS DateTime), 19, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(112, 2, CAST(N'2006-04-04 11:02:58.000' AS DateTime), CAST(N'2006-04-04 11:07:37.000' AS DateTime), 19, 25, NULL, NULL, N'Servir producto en lista de espera, pedido nº 48')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(113, 1, CAST(N'2006-04-04 11:03:12.000' AS DateTime), CAST(N'2006-04-04 11:03:12.000' AS DateTime), 72, 50, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(114, 2, CAST(N'2006-04-04 11:03:14.000' AS DateTime), CAST(N'2006-04-04 11:08:49.000' AS DateTime), 72, 50, NULL, NULL, N'Servir producto en lista de espera, pedido nº 46')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(115, 1, CAST(N'2006-04-04 11:03:38.000' AS DateTime), CAST(N'2006-04-04 11:03:38.000' AS DateTime), 41, 50, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(116, 2, CAST(N'2006-04-04 11:03:39.000' AS DateTime), CAST(N'2006-04-04 11:09:24.000' AS DateTime), 41, 50, NULL, NULL, N'Servir producto en lista de espera, pedido nº 45')
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(117, 2, CAST(N'2006-04-04 11:04:55.000' AS DateTime), CAST(N'2006-04-04 11:05:04.000' AS DateTime), 34, 87, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(118, 2, CAST(N'2006-04-04 11:35:50.000' AS DateTime), CAST(N'2006-04-04 11:35:54.000' AS DateTime), 51, 30, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(119, 2, CAST(N'2006-04-04 11:35:51.000' AS DateTime), CAST(N'2006-04-04 11:35:54.000' AS DateTime), 7, 30, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(120, 2, CAST(N'2006-04-04 11:36:15.000' AS DateTime), CAST(N'2006-04-04 11:36:21.000' AS DateTime), 17, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(121, 2, CAST(N'2006-04-04 11:36:39.000' AS DateTime), CAST(N'2006-04-04 11:36:47.000' AS DateTime), 6, 90, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(122, 2, CAST(N'2006-04-04 11:37:06.000' AS DateTime), CAST(N'2006-04-04 11:37:09.000' AS DateTime), 4, 30, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(123, 2, CAST(N'2006-04-04 11:37:45.000' AS DateTime), CAST(N'2006-04-04 11:37:49.000' AS DateTime), 48, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(124, 2, CAST(N'2006-04-04 11:38:07.000' AS DateTime), CAST(N'2006-04-04 11:38:11.000' AS DateTime), 48, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(125, 2, CAST(N'2006-04-04 11:38:27.000' AS DateTime), CAST(N'2006-04-04 11:38:32.000' AS DateTime), 41, 10, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(126, 2, CAST(N'2006-04-04 11:38:48.000' AS DateTime), CAST(N'2006-04-04 11:38:53.000' AS DateTime), 43, 5, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(127, 2, CAST(N'2006-04-04 11:39:12.000' AS DateTime), CAST(N'2006-04-04 11:39:29.000' AS DateTime), 40, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(128, 2, CAST(N'2006-04-04 11:39:50.000' AS DateTime), CAST(N'2006-04-04 11:39:53.000' AS DateTime), 8, 20, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(129, 2, CAST(N'2006-04-04 11:40:13.000' AS DateTime), CAST(N'2006-04-04 11:40:16.000' AS DateTime), 80, 15, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(130, 2, CAST(N'2006-04-04 11:40:32.000' AS DateTime), CAST(N'2006-04-04 11:40:38.000' AS DateTime), 74, 20, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(131, 2, CAST(N'2006-04-04 11:41:39.000' AS DateTime), CAST(N'2006-04-04 11:41:45.000' AS DateTime), 72, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(132, 2, CAST(N'2006-04-04 11:42:17.000' AS DateTime), CAST(N'2006-04-04 11:42:26.000' AS DateTime), 3, 50, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(133, 2, CAST(N'2006-04-04 11:42:24.000' AS DateTime), CAST(N'2006-04-04 11:42:26.000' AS DateTime), 8, 3, NULL, NULL, NULL)
GO
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(134, 2, CAST(N'2006-04-04 11:42:48.000' AS DateTime), CAST(N'2006-04-04 11:43:08.000' AS DateTime), 20, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(135, 2, CAST(N'2006-04-04 11:43:05.000' AS DateTime), CAST(N'2006-04-04 11:43:08.000' AS DateTime), 52, 40, NULL, NULL, NULL)
INSERT [Almacen].[TransaccionInventario]
	([cod_tra_inv_in], [cod_tip_tra_inv_ti], [fec_cre_tra_inv_dt], [fec_mod_tra_inv_dt], [cod_prd_in], [cnt_prd_in], [cod_ped_com_in], [cod_ped_in], [obs_tra_inv_vc])
VALUES
	(136, 3, CAST(N'2006-04-25 17:04:05.000' AS DateTime), CAST(N'2006-04-25 17:04:57.000' AS DateTime), 56, 110, NULL, NULL, NULL)
SET IDENTITY_INSERT [Almacen].[TransaccionInventario] OFF
INSERT [Compras].[EstadoPedidoCompra]
	([cod_est_ped_com_ti], [nom_est_ped_com_vc])
VALUES
	(0, N'Nuevo')
INSERT [Compras].[EstadoPedidoCompra]
	([cod_est_ped_com_ti], [nom_est_ped_com_vc])
VALUES
	(1, N'Enviado')
INSERT [Compras].[EstadoPedidoCompra]
	([cod_est_ped_com_ti], [nom_est_ped_com_vc])
VALUES
	(2, N'Aprobado')
INSERT [Compras].[EstadoPedidoCompra]
	([cod_est_ped_com_ti], [nom_est_ped_com_vc])
VALUES
	(3, N'Cerrado')
SET IDENTITY_INSERT [Compras].[PedidoCompra] ON

INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(90, 1, 2, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(91, 3, 2, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(92, 2, 2, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(93, 5, 2, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(94, 6, 2, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(95, 4, 2, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(96, 1, 5, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según pedido nº  30', 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 5)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(97, 2, 7, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 33', 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 7)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(98, 2, 4, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 36', 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 4)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(99, 1, 3, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 38', 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 3)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(100, 2, 9, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 39', 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 9)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(101, 1, 2, CAST(N'2006-01-14 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 40', 2, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(102, 1, 1, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 41', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 1)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(103, 2, 1, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 42', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 1)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(104, 2, 1, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 45', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 1)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(105, 5, 7, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, N'Cheque', N'Compra generada según el pedido nº 46', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 7)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(106, 6, 7, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 46', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 7)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(107, 1, 6, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 47', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 6)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(108, 2, 4, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 48', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 4)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(109, 2, 4, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 48', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 4)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(110, 1, 3, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 49', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 3)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(111, 1, 2, CAST(N'2006-03-31 00:00:00.000' AS DateTime), CAST(N'2006-03-31 00:00:00.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, N'Compra generada según el pedido nº 56', 2, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(140, 6, NULL, CAST(N'2006-04-25 00:00:00.000' AS DateTime), CAST(N'2006-04-25 16:40:51.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 2, CAST(N'2006-04-25 16:41:33.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(141, 8, NULL, CAST(N'2006-04-25 00:00:00.000' AS DateTime), CAST(N'2006-04-25 17:10:35.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 2, CAST(N'2006-04-25 17:10:55.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(142, 8, NULL, CAST(N'2006-04-25 00:00:00.000' AS DateTime), CAST(N'2006-04-25 17:18:29.000' AS DateTime), 2, NULL, 0.0000, 0.0000, NULL, 0.0000, N'Cheque', NULL, 2, CAST(N'2006-04-25 17:18:51.000' AS DateTime), 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(146, 2, 2, CAST(N'2006-04-26 18:26:37.000' AS DateTime), CAST(N'2006-04-26 18:26:37.000' AS DateTime), 1, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, NULL, NULL, 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(147, 7, 2, CAST(N'2006-04-26 18:33:28.000' AS DateTime), CAST(N'2006-04-26 18:33:28.000' AS DateTime), 1, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, NULL, NULL, 2)
INSERT [Compras].[PedidoCompra]
	([cod_ped_com_in], [cod_prv_in], [cod_emp_in], [fec_env_ped_com_dt], [fec_cre_ped_com_dt], [cod_est_ped_com_ti], [fec_rec_ped_com_dt], [gas_env_ped_com_mo], [imp_ped_com_mo], [fec_pag_ped_com_dt], [imp_pag_ped_com_mo], [tip_pag_ped_com_vc], [obs_ped_com_vc], [cod_emp_apr_ped_com_in], [fec_apr_ped_com_dt], [cod_emp_env_ped_com_in])
VALUES
	(148, 5, 2, CAST(N'2006-04-26 18:33:52.000' AS DateTime), CAST(N'2006-04-26 18:33:52.000' AS DateTime), 1, NULL, 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, NULL, NULL, 2)
SET IDENTITY_INSERT [Compras].[PedidoCompra] OFF
SET IDENTITY_INSERT [Compras].[PedidoCompraDetalle] ON

INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(238, 90, 1, CAST(40.0000 AS Decimal(18, 4)), 14.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 59)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(239, 91, 3, CAST(100.0000 AS Decimal(18, 4)), 8.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 54)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(240, 91, 4, CAST(40.0000 AS Decimal(18, 4)), 16.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 55)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(241, 91, 5, CAST(40.0000 AS Decimal(18, 4)), 16.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 56)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(242, 92, 6, CAST(100.0000 AS Decimal(18, 4)), 19.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 40)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(243, 92, 7, CAST(40.0000 AS Decimal(18, 4)), 22.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 41)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(244, 92, 8, CAST(40.0000 AS Decimal(18, 4)), 30.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 42)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(245, 92, 14, CAST(40.0000 AS Decimal(18, 4)), 17.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 43)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(246, 92, 17, CAST(40.0000 AS Decimal(18, 4)), 29.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 44)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(247, 92, 19, CAST(20.0000 AS Decimal(18, 4)), 7.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 45)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(248, 92, 20, CAST(40.0000 AS Decimal(18, 4)), 61.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 46)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(249, 92, 21, CAST(20.0000 AS Decimal(18, 4)), 8.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 47)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(250, 90, 34, CAST(60.0000 AS Decimal(18, 4)), 10.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 60)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(251, 92, 40, CAST(120.0000 AS Decimal(18, 4)), 14.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 48)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(252, 92, 41, CAST(40.0000 AS Decimal(18, 4)), 7.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 49)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(253, 90, 43, CAST(100.0000 AS Decimal(18, 4)), 34.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 61)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(254, 92, 48, CAST(100.0000 AS Decimal(18, 4)), 10.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 50)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(255, 92, 51, CAST(40.0000 AS Decimal(18, 4)), 40.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 51)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(256, 93, 52, CAST(100.0000 AS Decimal(18, 4)), 5.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 37)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(257, 93, 56, CAST(120.0000 AS Decimal(18, 4)), 28.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 38)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(258, 93, 57, CAST(80.0000 AS Decimal(18, 4)), 15.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 39)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(259, 91, 65, CAST(40.0000 AS Decimal(18, 4)), 16.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 57)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(260, 91, 66, CAST(80.0000 AS Decimal(18, 4)), 13.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 58)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(261, 94, 72, CAST(40.0000 AS Decimal(18, 4)), 26.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 36)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(262, 92, 74, CAST(20.0000 AS Decimal(18, 4)), 8.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 52)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(263, 92, 77, CAST(60.0000 AS Decimal(18, 4)), 10.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 53)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(264, 95, 80, CAST(75.0000 AS Decimal(18, 4)), 3.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 35)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(265, 90, 81, CAST(125.0000 AS Decimal(18, 4)), 2.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 62)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(266, 96, 34, CAST(100.0000 AS Decimal(18, 4)), 10.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 82)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(267, 97, 19, CAST(30.0000 AS Decimal(18, 4)), 7.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 80)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(268, 98, 41, CAST(200.0000 AS Decimal(18, 4)), 7.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 78)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(269, 99, 43, CAST(300.0000 AS Decimal(18, 4)), 34.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 76)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(270, 100, 48, CAST(100.0000 AS Decimal(18, 4)), 10.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 74)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(271, 101, 81, CAST(200.0000 AS Decimal(18, 4)), 2.0000, CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, 72)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(272, 102, 43, CAST(300.0000 AS Decimal(18, 4)), 34.0000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(273, 103, 19, CAST(10.0000 AS Decimal(18, 4)), 7.0000, CAST(N'2006-04-17 00:00:00.000' AS DateTime), 1, 111)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(274, 104, 41, CAST(50.0000 AS Decimal(18, 4)), 7.0000, CAST(N'2006-04-06 00:00:00.000' AS DateTime), 1, 115)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(275, 105, 57, CAST(100.0000 AS Decimal(18, 4)), 15.0000, CAST(N'2006-04-05 00:00:00.000' AS DateTime), 1, 100)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(276, 106, 72, CAST(50.0000 AS Decimal(18, 4)), 26.0000, CAST(N'2006-04-05 00:00:00.000' AS DateTime), 1, 113)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(277, 107, 34, CAST(300.0000 AS Decimal(18, 4)), 10.0000, CAST(N'2006-04-05 00:00:00.000' AS DateTime), 1, 107)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(278, 108, 8, CAST(25.0000 AS Decimal(18, 4)), 30.0000, CAST(N'2006-04-05 00:00:00.000' AS DateTime), 1, 105)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(279, 109, 19, CAST(25.0000 AS Decimal(18, 4)), 7.0000, CAST(N'2006-04-05 00:00:00.000' AS DateTime), 1, 109)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(280, 110, 43, CAST(250.0000 AS Decimal(18, 4)), 34.0000, CAST(N'2006-04-10 00:00:00.000' AS DateTime), 1, 103)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(281, 90, 1, CAST(40.0000 AS Decimal(18, 4)), 14.0000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(282, 92, 19, CAST(20.0000 AS Decimal(18, 4)), 7.0000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(283, 111, 34, CAST(50.0000 AS Decimal(18, 4)), 10.0000, CAST(N'2006-04-04 00:00:00.000' AS DateTime), 1, 102)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(285, 91, 3, CAST(50.0000 AS Decimal(18, 4)), 8.0000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(286, 91, 4, CAST(40.0000 AS Decimal(18, 4)), 16.0000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(288, 140, 85, CAST(10.0000 AS Decimal(18, 4)), 9.0000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(289, 141, 6, CAST(10.0000 AS Decimal(18, 4)), 18.7500, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(290, 142, 1, CAST(1.0000 AS Decimal(18, 4)), 13.5000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(292, 146, 20, CAST(40.0000 AS Decimal(18, 4)), 60.0000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(293, 146, 51, CAST(40.0000 AS Decimal(18, 4)), 39.0000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(294, 147, 40, CAST(120.0000 AS Decimal(18, 4)), 13.0000, NULL, 0, NULL)
INSERT [Compras].[PedidoCompraDetalle]
	([cod_ped_com_det_in], [cod_ped_com_in], [cod_prd_in], [cnt_ped_com_det_de], [cos_uni_ped_com_det_mo], [fec_rec_ped_com_det_dt], [pub_tra_inv_ped_com_det_bi], [cod_tra_inv_in])
VALUES
	(295, 148, 72, CAST(40.0000 AS Decimal(18, 4)), 26.0000, NULL, 0, NULL)
SET IDENTITY_INSERT [Compras].[PedidoCompraDetalle] OFF
SET IDENTITY_INSERT [Compras].[Proveedor] ON

INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(1, N'Proveedor A', N'García', N'Miguel A.', NULL, N'Jefe de ventas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(2, N'Proveedor B', N'Cornejo', N'Cecilia', NULL, N'Jefe de ventas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(3, N'Proveedor C', N'González', N'Nuria', NULL, N'Representante de ventas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(4, N'Proveedor D', N'Arteaga Torreira', N'Diego', NULL, N'Jefe de marketing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(5, N'Proveedor E', N'Hernández-Echevarría', N'Amaya', NULL, N'Jefe de ventas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(6, N'Proveedor F', N'Gil', N'Enrique', NULL, N'Ayudante de marketing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(7, N'Proveedor G', N'Navarro', N'Tomás', NULL, N'Jefe de marketing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(8, N'Proveedor H', N'Noriega', N'Fabricio', NULL, N'Representante de ventas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(9, N'Proveedor I', N'Peiro Alba', N'José Ignacio', NULL, N'Jefe de ventas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Compras].[Proveedor]
	([cod_prv_in], [raz_soc_prv_vc], [pat_prv_vc], [nom_prv_vc], [ema_prv_vc], [car_prv_vc], [tel_tra_prv_vc], [tel_par_prv_vc], [tel_mov_prv_vc], [fax_prv_vc], [dir_prv_vc], [ciu_prv_vc], [prv_prv_vc], [cod_pos_prv_vc], [pai_prv_vc], [web_prv_vc], [obs_prv_vc], [dat_adj_prv_vc])
VALUES
	(10, N'Proveedor J', N'Bastidas Patiño', N'Jaime Humberto', NULL, N'Jefe de ventas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [Compras].[Proveedor] OFF
SET IDENTITY_INSERT [dbo].[Cuerda] ON

INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(2, N'Northwind Traders')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(3, N'No se puede quitar el inventario publicado.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(4, N'Producto en lista de espera servido para el pedido nº |')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(5, N'El precio con descuento es inferior al costo.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(6, N'Inventario insuficiente.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(7, N'Inventario insuficiente. ¿Desea crear un pedido de compra?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(8, N'Se crearon correctamente pedidos de compra para | productos')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(9, N'No hay productos por debajo de sus respectivos niveles de reposición')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(10, N'Debe especificarse el nombre del cliente.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(11, N'La reposición generará pedidos de compra para todos los productos por debajo de los niveles de inventario deseados. ¿Desea continuar?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(12, N'No se puede crear el pedido de compra. No hay proveedores para el producto especificado.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(13, N'El precio con descuento es inferior al costo.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(14, N'¿Desea continuar?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(15, N'El pedido ya se ha facturado. ¿Desea imprimir la factura?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(16, N'El pedido no contiene ningún elemento de línea')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(17, N'No se puede crear la factura. No se ha asignado inventario para cada producto especificado.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(18, N'No hay ventas en el período de tiempo especificado')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(19, N'La reposición del producto ha sido correcta.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(21, N'No es necesario reponer el producto. Ya tiene el nivel de inventario deseado.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(22, N'Error de reposición de productos.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(23, N'El inicio de sesión especificado no es válido.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(24, N'Primero deben seleccionarse elementos para el informe.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(25, N'Al cambiar el proveedor, se eliminarán los elementos de línea de compra. ¿Desea continuar?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(26, N'Se enviaron correctamente pedidos de compra para | productos. ¿Desea ver el informe de reposición?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(27, N'Error al intentar reponer los niveles de inventario.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(28, N'Reposición correcta de | productos. ¿Desea ver el informe de reposición?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(29, N'No se pueden quitar elementos de línea de compra que ya se han publicado en el inventario.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(30, N'Error al quitar uno o varios elementos de línea de compra.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(31, N'No se puede modificar la cantidad de los productos comprados que ya se han recibido o publicado en el inventario.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(32, N'No se puede modificar el precio de los productos comprados que ya se han recibido o publicado en el inventario.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(33, N'El producto se ha publicado correctamente en el inventario.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(34, N'El producto no se puede publicar correctamente en el inventario.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(35, N'Hay pedidos de este producto en lista de espera. ¿Desea servirlos ahora?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(36, N'No se puede publicar el producto en el inventario sin especificar la fecha de recepción.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(37, N'¿Desea publicar el producto recibido en el inventario?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(38, N'¿Desea inicializar los datos de compras, pedidos e inventario?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(39, N'Primero debe especificarse el nombre del empleado.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(40, N'El usuario especificado debe haber iniciado sesión para aprobar la compra.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(41, N'El pedido de compra debe contener elementos de línea completados para poder aprobarlo')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(42, N'No tiene permiso para aprobar compras.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(43, N'Compra aprobada correctamente')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(44, N'No se puede aprobar la compra.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(45, N'La compra se ha enviado correctamente para su aprobación')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(46, N'No se puede enviar la compra para su aprobación')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(47, N'El pedido de compra no contiene elementos de línea')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(48, N'¿Desea cancelar este pedido?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(49, N'Al cancelar un pedido, se eliminará permanentemente. ¿Confirma que desea cancelarlo?')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(100, N'El pedido se canceló correctamente.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(101, N'No se puede cancelar un pedido que tiene elementos recibidos y publicados en el inventario.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(102, N'Error al intentar cancelar este pedido.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(103, N'Todavía no se ha creado la factura de este pedido.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(104, N'La información de envío está incompleta. Especifique toda la información de envío y vuelva a intentarlo.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(105, N'No se puede marcar como enviado: primero debe facturarse el pedido.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(106, N'No se puede cancelar un pedido que ya se ha enviado.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(107, N'Primero debe especificarse el vendedor.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(108, N'El pedido está marcado como cerrado.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(109, N'Primero el pedido debe marcarse como enviado antes de cerrarlo.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(110, N'Primero debe especificarse la información de pago.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(111, N'Error al intentar reponer los niveles de inventario. La reposición fue correcta para | productos.')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(112, N'Debe proporciona un coste unitario')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(113, N'Servir producto en lista de espera, pedido nº|')
INSERT [dbo].[Cuerda]
	([cod_hlp_in], [des_hlp_vc])
VALUES
	(114, N'Compra generada basada en el pedido nº|')
SET IDENTITY_INSERT [dbo].[Cuerda] OFF
INSERT [dbo].[EstadoPedido]
	([cod_est_ped_ti], [nom_est_ped_vc])
VALUES
	(0, N'Nuevo')
INSERT [dbo].[EstadoPedido]
	([cod_est_ped_ti], [nom_est_ped_vc])
VALUES
	(1, N'Facturado')
INSERT [dbo].[EstadoPedido]
	([cod_est_ped_ti], [nom_est_ped_vc])
VALUES
	(2, N'Enviado')
INSERT [dbo].[EstadoPedido]
	([cod_est_ped_ti], [nom_est_ped_vc])
VALUES
	(3, N'Cerrado')
INSERT [dbo].[EstadoPedidoDetalle]
	([cod_est_ped_det_ti], [nom_est_ped_det_vc])
VALUES
	(0, N'Ninguno')
INSERT [dbo].[EstadoPedidoDetalle]
	([cod_est_ped_det_ti], [nom_est_ped_det_vc])
VALUES
	(1, N'Asignado')
INSERT [dbo].[EstadoPedidoDetalle]
	([cod_est_ped_det_ti], [nom_est_ped_det_vc])
VALUES
	(2, N'Facturado')
INSERT [dbo].[EstadoPedidoDetalle]
	([cod_est_ped_det_ti], [nom_est_ped_det_vc])
VALUES
	(3, N'Enviado')
INSERT [dbo].[EstadoPedidoDetalle]
	([cod_est_ped_det_ti], [nom_est_ped_det_vc])
VALUES
	(4, N'En pedido')
INSERT [dbo].[EstadoPedidoDetalle]
	([cod_est_ped_det_ti], [nom_est_ped_det_vc])
VALUES
	(5, N'Sin existencias')
INSERT [Facturacion].[EstadoPedidoImpuesto]
	([cod_est_ped_imp_ti], [nom_est_ped_imp_vc])
VALUES
	(0, N'Exento de impuestos')
INSERT [Facturacion].[EstadoPedidoImpuesto]
	([cod_est_ped_imp_ti], [nom_est_ped_imp_vc])
VALUES
	(1, N'Sujeto a impuestos')
SET IDENTITY_INSERT [Facturacion].[Factura] ON

INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(5, 31, CAST(N'2006-03-22 16:08:59.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(6, 32, CAST(N'2006-03-22 16:10:27.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(7, 40, CAST(N'2006-03-24 10:41:41.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(8, 39, CAST(N'2006-03-24 10:55:46.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(9, 38, CAST(N'2006-03-24 10:56:57.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(10, 37, CAST(N'2006-03-24 10:57:38.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(11, 36, CAST(N'2006-03-24 10:58:40.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(12, 35, CAST(N'2006-03-24 10:59:41.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(13, 34, CAST(N'2006-03-24 11:00:55.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(14, 33, CAST(N'2006-03-24 11:02:02.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(15, 30, CAST(N'2006-03-24 11:03:00.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(16, 56, CAST(N'2006-04-03 13:50:15.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(17, 55, CAST(N'2006-04-04 11:05:04.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(18, 51, CAST(N'2006-04-04 11:06:13.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(19, 50, CAST(N'2006-04-04 11:06:56.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(20, 48, CAST(N'2006-04-04 11:07:37.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(21, 47, CAST(N'2006-04-04 11:08:14.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(22, 46, CAST(N'2006-04-04 11:08:49.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(23, 45, CAST(N'2006-04-04 11:09:24.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(24, 79, CAST(N'2006-04-04 11:35:54.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(25, 78, CAST(N'2006-04-04 11:36:21.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(26, 77, CAST(N'2006-04-04 11:36:47.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(27, 76, CAST(N'2006-04-04 11:37:09.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(28, 75, CAST(N'2006-04-04 11:37:49.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(29, 74, CAST(N'2006-04-04 11:38:11.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(30, 73, CAST(N'2006-04-04 11:38:32.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(31, 72, CAST(N'2006-04-04 11:38:53.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(32, 71, CAST(N'2006-04-04 11:39:29.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(33, 70, CAST(N'2006-04-04 11:39:53.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(34, 69, CAST(N'2006-04-04 11:40:16.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(35, 67, CAST(N'2006-04-04 11:40:38.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(36, 42, CAST(N'2006-04-04 11:41:14.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(37, 60, CAST(N'2006-04-04 11:41:45.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(38, 63, CAST(N'2006-04-04 11:42:26.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
INSERT [Facturacion].[Factura]
	([cod_fac_in], [cod_ped_in], [fec_ped_dt], [fec_ven_ped_dt], [imp_ped_mo], [cos_env_ped_mon], [imp_tot_ped_mon])
VALUES
	(39, 58, CAST(N'2006-04-04 11:43:08.000' AS DateTime), NULL, 0.0000, 0.0000, 0.0000)
SET IDENTITY_INSERT [Facturacion].[Factura] OFF
SET IDENTITY_INSERT [Personal].[Empleado] ON

INSERT [Personal].[Empleado]
	([cod_emp_in], [raz_soc_emp_vc], [pat_emp_vc], [nom_emp_vc], [ema_emp_vc], [car_emp_vc], [tel_tra_emp_vc], [tel_par_emp_vc], [tel_mov_emp_vc], [fax_emp_vc], [dir_emp_vc], [ciu_emp_vc], [prov_emp_vc], [cod_pos_emp_vc], [pai_emp_vc], [web_emp_vc], [obs_emp_vc], [dat_adj_emp_vc])
VALUES
	(1, N'Northwind Traders', N'González', N'María', N'nancy@northwindtraders.com', N'Representante de ventas', N'987 654 321', N'987 654 321', NULL, N'987 654 321', N'Avenida Primera, 123', N'Seattle', N'Madrid', N'99999', N'España', N'#http://northwindtraders.com#', NULL, NULL)
INSERT [Personal].[Empleado]
	([cod_emp_in], [raz_soc_emp_vc], [pat_emp_vc], [nom_emp_vc], [ema_emp_vc], [car_emp_vc], [tel_tra_emp_vc], [tel_par_emp_vc], [tel_mov_emp_vc], [fax_emp_vc], [dir_emp_vc], [ciu_emp_vc], [prov_emp_vc], [cod_pos_emp_vc], [pai_emp_vc], [web_emp_vc], [obs_emp_vc], [dat_adj_emp_vc])
VALUES
	(2, N'Northwind Traders', N'Escolar', N'Jesús', N'andrew@northwindtraders.com', N'Vicepresidente de ventas', N'987 654 321', N'987 654 321', NULL, N'987 654 321', N'Avenida Segunda, 123', N'Bellevue', N'Madrid', N'99999', N'España', N'http://northwindtraders.com#http://northwindtraders.com/#', N'Empezó en la compañía como representante de ventas, ascendió a director comercial y, después, fue nombrado vicepresidente de ventas.', NULL)
INSERT [Personal].[Empleado]
	([cod_emp_in], [raz_soc_emp_vc], [pat_emp_vc], [nom_emp_vc], [ema_emp_vc], [car_emp_vc], [tel_tra_emp_vc], [tel_par_emp_vc], [tel_mov_emp_vc], [fax_emp_vc], [dir_emp_vc], [ciu_emp_vc], [prov_emp_vc], [cod_pos_emp_vc], [pai_emp_vc], [web_emp_vc], [obs_emp_vc], [dat_adj_emp_vc])
VALUES
	(3, N'Northwind Traders', N'Pinilla Gallego', N'Pilar', N'jan@northwindtraders.com', N'Representante de ventas', N'987 654 321', N'987 654 321', NULL, N'987 654 321', N'Avenida Tercera, 123', N'Redmond', N'Madrid', N'99999', N'España', N'http://northwindtraders.com#http://northwindtraders.com/#', N'Fue contratado como ayudante de ventas y ascendió a representante de ventas.', NULL)
INSERT [Personal].[Empleado]
	([cod_emp_in], [raz_soc_emp_vc], [pat_emp_vc], [nom_emp_vc], [ema_emp_vc], [car_emp_vc], [tel_tra_emp_vc], [tel_par_emp_vc], [tel_mov_emp_vc], [fax_emp_vc], [dir_emp_vc], [ciu_emp_vc], [prov_emp_vc], [cod_pos_emp_vc], [pai_emp_vc], [web_emp_vc], [obs_emp_vc], [dat_adj_emp_vc])
VALUES
	(4, N'Northwind Traders', N'Jesús Cuesta', N'María', N'mariya@northwindtraders.com', N'Representante de ventas', N'987 654 321', N'987 654 321', NULL, N'987 654 321', N'Avenida Cuarta, 123', N'Kirkland', N'Madrid', N'99999', N'España', N'http://northwindtraders.com#http://northwindtraders.com/#', NULL, NULL)
INSERT [Personal].[Empleado]
	([cod_emp_in], [raz_soc_emp_vc], [pat_emp_vc], [nom_emp_vc], [ema_emp_vc], [car_emp_vc], [tel_tra_emp_vc], [tel_par_emp_vc], [tel_mov_emp_vc], [fax_emp_vc], [dir_emp_vc], [ciu_emp_vc], [prov_emp_vc], [cod_pos_emp_vc], [pai_emp_vc], [web_emp_vc], [obs_emp_vc], [dat_adj_emp_vc])
VALUES
	(5, N'Northwind Traders', N'San Juan', N'Patricia', N'steven@northwindtraders.com', N'Jefe de ventas', N'987 654 321', N'987 654 321', NULL, N'987 654 321', N'Avenida Quinta, 123', N'Seattle', N'Madrid', N'99999', N'España', N'http://northwindtraders.com#http://northwindtraders.com/#', N'Empezó en la empresa como representante de ventas y ascendió a director comercial. Habla francés.', NULL)
INSERT [Personal].[Empleado]
	([cod_emp_in], [raz_soc_emp_vc], [pat_emp_vc], [nom_emp_vc], [ema_emp_vc], [car_emp_vc], [tel_tra_emp_vc], [tel_par_emp_vc], [tel_mov_emp_vc], [fax_emp_vc], [dir_emp_vc], [ciu_emp_vc], [prov_emp_vc], [cod_pos_emp_vc], [pai_emp_vc], [web_emp_vc], [obs_emp_vc], [dat_adj_emp_vc])
VALUES
	(6, N'Northwind Traders', N'Rivas', N'Juan Carlos', N'michael@northwindtraders.com', N'Representante de ventas', N'987 654 321', N'987 654 321', NULL, N'987 654 321', N'Avenida Sexta, 123', N'Redmond', N'Madrid', N'99999', N'España', N'http://northwindtraders.com#http://northwindtraders.com/#', N'Habla japonés y lee y escribe en francés, portugués y español.', NULL)
INSERT [Personal].[Empleado]
	([cod_emp_in], [raz_soc_emp_vc], [pat_emp_vc], [nom_emp_vc], [ema_emp_vc], [car_emp_vc], [tel_tra_emp_vc], [tel_par_emp_vc], [tel_mov_emp_vc], [fax_emp_vc], [dir_emp_vc], [ciu_emp_vc], [prov_emp_vc], [cod_pos_emp_vc], [pai_emp_vc], [web_emp_vc], [obs_emp_vc], [dat_adj_emp_vc])
VALUES
	(7, N'Northwind Traders', N'Acevedo', N'Humberto', N'robert@northwindtraders.com', N'Representante de ventas', N'987 654 321', N'987 654 321', NULL, N'987 654 321', N'Avenida Séptima, 123', N'Seattle', N'Madrid', N'99999', N'España', N'http://northwindtraders.com#http://northwindtraders.com/#', NULL, NULL)
INSERT [Personal].[Empleado]
	([cod_emp_in], [raz_soc_emp_vc], [pat_emp_vc], [nom_emp_vc], [ema_emp_vc], [car_emp_vc], [tel_tra_emp_vc], [tel_par_emp_vc], [tel_mov_emp_vc], [fax_emp_vc], [dir_emp_vc], [ciu_emp_vc], [prov_emp_vc], [cod_pos_emp_vc], [pai_emp_vc], [web_emp_vc], [obs_emp_vc], [dat_adj_emp_vc])
VALUES
	(8, N'Northwind Traders', N'Bonifaz', N'Luis', N'laura@northwindtraders.com', N'Coordinador de ventas', N'987 654 321', N'987 654 321', NULL, N'987 654 321', N'Avenida Octava, 123', N'Redmond', N'Madrid', N'99999', N'España', N'http://northwindtraders.com#http://northwindtraders.com/#', N'Lee y escribe francés.', NULL)
INSERT [Personal].[Empleado]
	([cod_emp_in], [raz_soc_emp_vc], [pat_emp_vc], [nom_emp_vc], [ema_emp_vc], [car_emp_vc], [tel_tra_emp_vc], [tel_par_emp_vc], [tel_mov_emp_vc], [fax_emp_vc], [dir_emp_vc], [ciu_emp_vc], [prov_emp_vc], [cod_pos_emp_vc], [pai_emp_vc], [web_emp_vc], [obs_emp_vc], [dat_adj_emp_vc])
VALUES
	(9, N'Northwind Traders', N'Chaves', N'Francisco', N'anne@northwindtraders.com', N'Representante de ventas', N'987 654 321', N'987 654 321', NULL, N'987 654 321', N'Avenida Novena, 123', N'Seattle', N'Madrid', N'99999', N'España', N'http://northwindtraders.com#http://northwindtraders.com/#', N'Habla francés y alemán.', NULL)
SET IDENTITY_INSERT [Personal].[Empleado] OFF
SET IDENTITY_INSERT [Sistemas].[Privilegio] ON

INSERT [Sistemas].[Privilegio]
	([cod_pri_in], [nom_pri_vc])
VALUES
	(2, N'Aprobaciones de compras')
SET IDENTITY_INSERT [Sistemas].[Privilegio] OFF
INSERT [Sistemas].[PrivilegioEmpleado]
	([cod_emp_in], [cod_pri_in])
VALUES
	(2, 2)
SET IDENTITY_INSERT [Transporte].[Transportista] ON

INSERT [Transporte].[Transportista]
	([cod_tra_in], [raz_soc_tra_vc], [pat_tra_vc], [nom_tra_vc], [ema_tra_vc], [car_tra_vc], [tel_tra_tra_vc], [tel_par_tra_vc], [tel_mov_tra_vc], [fax_tra_vc], [dir_tra_vc], [ciu_tra_vc], [prv_tra_vc], [cod_pos_tra_vc], [pai_tra_vc], [web_tra_vc], [obs_tra_vc], [dat_adj_tra_vc])
VALUES
	(1, N'Compañía de transportes A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Calle Principal, 123', N'Memphis', N'TN', N'99999', N'España', NULL, NULL, NULL)
INSERT [Transporte].[Transportista]
	([cod_tra_in], [raz_soc_tra_vc], [pat_tra_vc], [nom_tra_vc], [ema_tra_vc], [car_tra_vc], [tel_tra_tra_vc], [tel_par_tra_vc], [tel_mov_tra_vc], [fax_tra_vc], [dir_tra_vc], [ciu_tra_vc], [prv_tra_vc], [cod_pos_tra_vc], [pai_tra_vc], [web_tra_vc], [obs_tra_vc], [dat_adj_tra_vc])
VALUES
	(2, N'Compañía de transportes B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Calle Principal, 123', N'Memphis', N'TN', N'99999', N'España', NULL, NULL, NULL)
INSERT [Transporte].[Transportista]
	([cod_tra_in], [raz_soc_tra_vc], [pat_tra_vc], [nom_tra_vc], [ema_tra_vc], [car_tra_vc], [tel_tra_tra_vc], [tel_par_tra_vc], [tel_mov_tra_vc], [fax_tra_vc], [dir_tra_vc], [ciu_tra_vc], [prv_tra_vc], [cod_pos_tra_vc], [pai_tra_vc], [web_tra_vc], [obs_tra_vc], [dat_adj_tra_vc])
VALUES
	(3, N'Compañía de transportes C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Calle Principal, 123', N'Memphis', N'TN', N'99999', N'España', NULL, NULL, NULL)
SET IDENTITY_INSERT [Transporte].[Transportista] OFF
SET IDENTITY_INSERT [Ventas].[Cliente] ON

INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(1, N'Compañía A', N'Benito', N'Almudena', NULL, N'Propietario', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Primera, 123', N'Seattle', N'Madrid', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(2, N'Compañía B', N'Bermejo', N'Antonio', NULL, N'Propietario', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Segunda, 123', N'Boston', N'MA', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(3, N'Compañía C', N'López', N'Arturo', NULL, N'Representante de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Tercera, 123', N'Los Ángeles', N'CA', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(4, N'Compañía D', N'García', N'Vanessa', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Cuarta, 123', N'Nueva York', N'NY', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(5, N'Compañía E', N'Gratacós Solsona', N'Antonio', NULL, N'Propietario', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Quinta, 123', N'Minneapolis', N'MN', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(6, N'Compañía F', N'Pérez-Olaeta', N'Francisco', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Sexta, 123', N'Milwaukee', N'WI', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(7, N'Compañía G', N'Sánchez Sánchez', N'Yolanda', NULL, N'Propietario', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Séptima, 123', N'Boise', N'ID', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(8, N'Compañía H', N'Valdés', N'Rene', NULL, N'Representante de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Octava, 123', N'Portland', N'OR', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(9, N'Compañía I', N'Maldonado Guerra', N'Alfredo', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Novena, 123', N'Salt Lake City', N'UT', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(10, N'Compañía J', N'Alverca', N'Luis', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Décima, 123', N'Chicago', N'IL', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(11, N'Compañía K', N'Caro', N'Fernando', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Decimoprimera, 123', N'Miami', N'FL', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(12, N'Compañía L', N'De Camargo', N'Gustavo', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Decimosegunda, 123', N'Las Vegas', N'NV', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(13, N'Compañía M', N'Fuentes Espinosa', N'Alfredo', NULL, N'Representante de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Decimotercera, 123', N'Memphis', N'TN', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(14, N'Compañía N', N'Estrada', N'Modesto', NULL, N'Representante de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Decimocuarta, 123', N'Denver', N'CO', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(15, N'Compañía O', N'Chaves Ferreira', N'Pedro', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Decimoquinta, 123', N'Honolulu', N'HI', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(16, N'Compañía P', N'López García', N'Avelino', NULL, N'Representante de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Decimosexta, 123', N'San Francisco', N'CA', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(17, N'Compañía Q', N'García', N'César', NULL, N'Propietario', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Decimoséptima, 123', N'Seattle', N'Madrid', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(18, N'Compañía R', N'García', N'Miguel Ángel', NULL, N'Representante de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Decimoctava, 123', N'Boston', N'MA', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(19, N'Compañía S', N'Hurtado', N'Begoña', NULL, N'Ayudante de contabilidad', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Decimonovena, 123', N'Los Ángeles', N'CA', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(20, N'Compañía T', N'Castrejón', N'Francisco Javier', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigésima, 123', N'Nueva York', N'NY', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(21, N'Compañía U', N'Junca', N'David', NULL, N'Jefe de contabilidad', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigesimoprimera, 123', N'Minneapolis', N'MN', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(22, N'Compañía V', N'Ramos', N'Luciana', NULL, N'Ayudante de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigesimosegunda, 123', N'Milwaukee', N'WI', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(23, N'Compañía W', N'Lugo', N'José', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigesimotercera, 123', N'Portland', N'OR', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(24, N'Compañía X', N'Machado', N'Manuel', NULL, N'Propietario', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigesimoprimera, 123', N'Salt Lake City', N'UT', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(25, N'Compañía Y', N'Martínez', N'Sandra I.', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigesimoquinta, 123', N'Chicago', N'IL', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(26, N'Compañía Z', N'Pinto', N'Armando', NULL, N'Ayudante de contabilidad', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigesimosexta, 123', N'Miami', N'FL', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(27, N'Compañía AA', N'Potra', N'Cristina', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigesimoséptima, 123', N'Las Vegas', N'NV', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(28, N'Compañía BB', N'Lacerda', N'Carlos', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigesimoséptima, 123', N'Memphis', N'TN', N'99999', N'España', NULL, NULL, NULL)
INSERT [Ventas].[Cliente]
	([cod_cli_in], [raz_soc_cli_vc], [pat_cli_vc], [nom_cli_vc], [ema_cli_vc], [car_cli_vc], [tel_tra_cli_vc], [tel_par_cli_vc], [tel_mov_cli_vc], [fax_cli_vc], [dir_cli_vc], [ciu_cli_vc], [prv_cli_vc], [cod_pos_cli_vc], [pai_cli_vc], [web_cli_vc], [obs_cli_vc], [dat_adj_cli_vc])
VALUES
	(29, N'Compañía CC', N'Saraiva', N'José', NULL, N'Jefe de compras', N'987 654 321', NULL, NULL, N'987 654 321', N'Calle Vigesimonovena, 123', N'Denver', N'CO', N'99999', N'España', NULL, NULL, NULL)
SET IDENTITY_INSERT [Ventas].[Cliente] OFF
INSERT [Ventas].[Informe]
	([Agrupar por], [Mostrar], [Título], [Origen de fila de filtro], [Predeterminado])
VALUES
	(N'Categoría', N'Categoría', N'Ventas por categoría', N'SELECT DISTINCT [Categoría] FROM [Productos] ORDER BY [Categoría];', 0)
INSERT [Ventas].[Informe]
	([Agrupar por], [Mostrar], [Título], [Origen de fila de filtro], [Predeterminado])
VALUES
	(N'Id de cliente', N'Cliente', N'Ventas por cliente', N'SELECT DISTINCT [Compañía] FROM [Clientes ampliados] ORDER BY [Compañía];', 0)
INSERT [Ventas].[Informe]
	([Agrupar por], [Mostrar], [Título], [Origen de fila de filtro], [Predeterminado])
VALUES
	(N'Id de empleado', N'Empleado', N'Ventas por empleado', N'SELECT DISTINCT [Nombre del empleado] FROM [Empleados ampliados] ORDER BY [Nombre del empleado];', 0)
INSERT [Ventas].[Informe]
	([Agrupar por], [Mostrar], [Título], [Origen de fila de filtro], [Predeterminado])
VALUES
	(N'Id de producto', N'Producto', N'Ventas por producto', N'SELECT DISTINCT [Nombre del producto] FROM [Productos] ORDER BY [Nombre del producto];', 1)
INSERT [Ventas].[Informe]
	([Agrupar por], [Mostrar], [Título], [Origen de fila de filtro], [Predeterminado])
VALUES
	(N'País o región', N'País o región', N'Ventas por país', N'SELECT DISTINCT [País o región] FROM [Clientes ampliados] ORDER BY [País o región];', 0)
SET IDENTITY_INSERT [Ventas].[Pedido] ON

INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(30, 9, 27, CAST(N'2006-01-15 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, N'Karen Toh', N'Calle Vigesimoséptima, 123', N'Las Vegas', N'NV', N'99999', N'España', 200.0000, 0.0000, N'Cheque', CAST(N'2006-01-15 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(31, 3, 4, CAST(N'2006-01-20 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 1, N'Christina Lee', N'Calle Cuarta, 123', N'Nueva York', N'NY', N'99999', N'España', 5.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-01-20 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(32, 4, 12, CAST(N'2006-01-22 00:00:00.000' AS DateTime), CAST(N'2006-01-22 00:00:00.000' AS DateTime), 2, N'John Edwards', N'Calle Decimosegunda, 123', N'Las Vegas', N'NV', N'99999', N'España', 5.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-01-22 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(33, 6, 8, CAST(N'2006-01-30 00:00:00.000' AS DateTime), CAST(N'2006-01-31 00:00:00.000' AS DateTime), 3, N'Elizabeth Andersen', N'Calle Octava, 123', N'Portland', N'OR', N'99999', N'España', 50.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-01-30 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(34, 9, 4, CAST(N'2006-02-06 00:00:00.000' AS DateTime), CAST(N'2006-02-07 00:00:00.000' AS DateTime), 3, N'Christina Lee', N'Calle Cuarta, 123', N'Nueva York', N'NY', N'99999', N'España', 4.0000, 0.0000, N'Cheque', CAST(N'2006-02-06 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(35, 3, 29, CAST(N'2006-02-10 00:00:00.000' AS DateTime), CAST(N'2006-02-12 00:00:00.000' AS DateTime), 2, N'Soo Jung Lee', N'Calle Vigesimonovena, 123', N'Denver', N'CO', N'99999', N'España', 7.0000, 0.0000, N'Cheque', CAST(N'2006-02-10 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(36, 4, 3, CAST(N'2006-02-23 00:00:00.000' AS DateTime), CAST(N'2006-02-25 00:00:00.000' AS DateTime), 2, N'Thomas Axen', N'Calle Tercera, 123', N'Los Ángeles', N'CA', N'99999', N'España', 7.0000, 0.0000, N'Efectivo', CAST(N'2006-02-23 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(37, 8, 6, CAST(N'2006-03-06 00:00:00.000' AS DateTime), CAST(N'2006-03-09 00:00:00.000' AS DateTime), 2, N'Francisco Pérez-Olaeta', N'Calle Sexta, 123', N'Milwaukee', N'WI', N'99999', N'España', 12.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-03-06 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(38, 9, 28, CAST(N'2006-03-10 00:00:00.000' AS DateTime), CAST(N'2006-03-11 00:00:00.000' AS DateTime), 3, N'Amritansh Raghav', N'Calle Vigesimoséptima, 123', N'Memphis', N'TN', N'99999', N'España', 10.0000, 0.0000, N'Cheque', CAST(N'2006-03-10 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(39, 3, 8, CAST(N'2006-03-22 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 3, N'Elizabeth Andersen', N'Calle Octava, 123', N'Portland', N'OR', N'99999', N'España', 5.0000, 0.0000, N'Cheque', CAST(N'2006-03-22 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(40, 4, 10, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-03-24 00:00:00.000' AS DateTime), 2, N'Roland Wacker', N'Calle Décima, 123', N'Chicago', N'IL', N'99999', N'España', 9.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-03-24 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(41, 1, 7, CAST(N'2006-03-24 00:00:00.000' AS DateTime), NULL, NULL, N'Ming-Yang Xie', N'Calle Séptima, 123', N'Boise', N'ID', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(42, 1, 10, CAST(N'2006-03-24 00:00:00.000' AS DateTime), CAST(N'2006-04-07 00:00:00.000' AS DateTime), 1, N'Roland Wacker', N'Calle Décima, 123', N'Chicago', N'IL', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 2)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(43, 1, 11, CAST(N'2006-03-24 00:00:00.000' AS DateTime), NULL, 3, N'Peter Krschne', N'Calle Decimoprimera, 123', N'Miami', N'FL', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(44, 1, 1, CAST(N'2006-03-24 00:00:00.000' AS DateTime), NULL, NULL, N'Anna Bedecs', N'Calle Primera, 123', N'Seattle', N'Madrid', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(45, 1, 28, CAST(N'2006-04-07 00:00:00.000' AS DateTime), CAST(N'2006-04-07 00:00:00.000' AS DateTime), 3, N'Amritansh Raghav', N'Calle Vigesimoséptima, 123', N'Memphis', N'TN', N'99999', N'España', 40.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-04-07 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(46, 7, 9, CAST(N'2006-04-05 00:00:00.000' AS DateTime), CAST(N'2006-04-05 00:00:00.000' AS DateTime), 1, N'Sven Mortensen', N'Calle Novena, 123', N'Salt Lake City', N'UT', N'99999', N'España', 100.0000, 0.0000, N'Cheque', CAST(N'2006-04-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(47, 6, 6, CAST(N'2006-04-08 00:00:00.000' AS DateTime), CAST(N'2006-04-08 00:00:00.000' AS DateTime), 2, N'Francisco Pérez-Olaeta', N'Calle Sexta, 123', N'Milwaukee', N'WI', N'99999', N'España', 300.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-04-08 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(48, 4, 8, CAST(N'2006-04-05 00:00:00.000' AS DateTime), CAST(N'2006-04-05 00:00:00.000' AS DateTime), 2, N'Elizabeth Andersen', N'Calle Octava, 123', N'Portland', N'OR', N'99999', N'España', 50.0000, 0.0000, N'Cheque', CAST(N'2006-04-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(50, 9, 25, CAST(N'2006-04-05 00:00:00.000' AS DateTime), CAST(N'2006-04-05 00:00:00.000' AS DateTime), 1, N'John Rodman', N'Calle Vigesimoquinta, 123', N'Chicago', N'IL', N'99999', N'España', 5.0000, 0.0000, N'Efectivo', CAST(N'2006-04-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(51, 9, 26, CAST(N'2006-04-05 00:00:00.000' AS DateTime), CAST(N'2006-04-05 00:00:00.000' AS DateTime), 3, N'Run Liu', N'Calle Vigesimosexta, 123', N'Miami', N'FL', N'99999', N'España', 60.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-04-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(55, 1, 29, CAST(N'2006-04-05 00:00:00.000' AS DateTime), CAST(N'2006-04-05 00:00:00.000' AS DateTime), 2, N'Soo Jung Lee', N'Calle Vigesimonovena, 123', N'Denver', N'CO', N'99999', N'España', 200.0000, 0.0000, N'Cheque', CAST(N'2006-04-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(56, 2, 6, CAST(N'2006-04-03 00:00:00.000' AS DateTime), CAST(N'2006-04-03 00:00:00.000' AS DateTime), 3, N'Francisco Pérez-Olaeta', N'Calle Sexta, 123', N'Milwaukee', N'WI', N'99999', N'España', 0.0000, 0.0000, N'Cheque', CAST(N'2006-04-03 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(57, 9, 27, CAST(N'2006-04-22 00:00:00.000' AS DateTime), CAST(N'2006-04-22 00:00:00.000' AS DateTime), 2, N'Karen Toh', N'Calle Vigesimoséptima, 123', N'Las Vegas', N'NV', N'99999', N'España', 200.0000, 0.0000, N'Cheque', CAST(N'2006-04-22 00:00:00.000' AS DateTime), NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(58, 3, 4, CAST(N'2006-04-22 00:00:00.000' AS DateTime), CAST(N'2006-04-22 00:00:00.000' AS DateTime), 1, N'Christina Lee', N'Calle Cuarta, 123', N'Nueva York', N'NY', N'99999', N'España', 5.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-04-22 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(59, 4, 12, CAST(N'2006-04-22 00:00:00.000' AS DateTime), CAST(N'2006-04-22 00:00:00.000' AS DateTime), 2, N'John Edwards', N'Calle Decimosegunda, 123', N'Las Vegas', N'NV', N'99999', N'España', 5.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-04-22 00:00:00.000' AS DateTime), NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(60, 6, 8, CAST(N'2006-04-30 00:00:00.000' AS DateTime), CAST(N'2006-04-30 00:00:00.000' AS DateTime), 3, N'Elizabeth Andersen', N'Calle Octava, 123', N'Portland', N'OR', N'99999', N'España', 50.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-04-30 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(61, 9, 4, CAST(N'2006-04-07 00:00:00.000' AS DateTime), CAST(N'2006-04-07 00:00:00.000' AS DateTime), 3, N'Christina Lee', N'Calle Cuarta, 123', N'Nueva York', N'NY', N'99999', N'España', 4.0000, 0.0000, N'Cheque', CAST(N'2006-04-07 00:00:00.000' AS DateTime), NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(62, 3, 29, CAST(N'2006-04-12 00:00:00.000' AS DateTime), CAST(N'2006-04-12 00:00:00.000' AS DateTime), 2, N'Soo Jung Lee', N'Calle Vigesimonovena, 123', N'Denver', N'CO', N'99999', N'España', 7.0000, 0.0000, N'Cheque', CAST(N'2006-04-12 00:00:00.000' AS DateTime), NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(63, 4, 3, CAST(N'2006-04-25 00:00:00.000' AS DateTime), CAST(N'2006-04-25 00:00:00.000' AS DateTime), 2, N'Thomas Axen', N'Calle Tercera, 123', N'Los Ángeles', N'CA', N'99999', N'España', 7.0000, 0.0000, N'Efectivo', CAST(N'2006-04-25 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(64, 8, 6, CAST(N'2006-05-09 00:00:00.000' AS DateTime), CAST(N'2006-05-09 00:00:00.000' AS DateTime), 2, N'Francisco Pérez-Olaeta', N'Calle Sexta, 123', N'Milwaukee', N'WI', N'99999', N'España', 12.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-05-09 00:00:00.000' AS DateTime), NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(65, 9, 28, CAST(N'2006-05-11 00:00:00.000' AS DateTime), CAST(N'2006-05-11 00:00:00.000' AS DateTime), 3, N'Amritansh Raghav', N'Calle Vigesimoséptima, 123', N'Memphis', N'TN', N'99999', N'España', 10.0000, 0.0000, N'Cheque', CAST(N'2006-05-11 00:00:00.000' AS DateTime), NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(66, 3, 8, CAST(N'2006-05-24 00:00:00.000' AS DateTime), CAST(N'2006-05-24 00:00:00.000' AS DateTime), 3, N'Elizabeth Andersen', N'Calle Octava, 123', N'Portland', N'OR', N'99999', N'España', 5.0000, 0.0000, N'Cheque', CAST(N'2006-05-24 00:00:00.000' AS DateTime), NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(67, 4, 10, CAST(N'2006-05-24 00:00:00.000' AS DateTime), CAST(N'2006-05-24 00:00:00.000' AS DateTime), 2, N'Roland Wacker', N'Calle Décima, 123', N'Chicago', N'IL', N'99999', N'España', 9.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-05-24 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(68, 1, 7, CAST(N'2006-05-24 00:00:00.000' AS DateTime), NULL, NULL, N'Ming-Yang Xie', N'Calle Séptima, 123', N'Boise', N'ID', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(69, 1, 10, CAST(N'2006-05-24 00:00:00.000' AS DateTime), NULL, 1, N'Roland Wacker', N'Calle Décima, 123', N'Chicago', N'IL', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(70, 1, 11, CAST(N'2006-05-24 00:00:00.000' AS DateTime), NULL, 3, N'Peter Krschne', N'Calle Decimoprimera, 123', N'Miami', N'FL', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(71, 1, 1, CAST(N'2006-05-24 00:00:00.000' AS DateTime), NULL, 3, N'Anna Bedecs', N'Calle Primera, 123', N'Seattle', N'Madrid', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(72, 1, 28, CAST(N'2006-06-07 00:00:00.000' AS DateTime), CAST(N'2006-06-07 00:00:00.000' AS DateTime), 3, N'Amritansh Raghav', N'Calle Vigesimoséptima, 123', N'Memphis', N'TN', N'99999', N'España', 40.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-06-07 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(73, 7, 9, CAST(N'2006-06-05 00:00:00.000' AS DateTime), CAST(N'2006-06-05 00:00:00.000' AS DateTime), 1, N'Sven Mortensen', N'Calle Novena, 123', N'Salt Lake City', N'UT', N'99999', N'España', 100.0000, 0.0000, N'Cheque', CAST(N'2006-06-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(74, 6, 6, CAST(N'2006-06-08 00:00:00.000' AS DateTime), CAST(N'2006-06-08 00:00:00.000' AS DateTime), 2, N'Francisco Pérez-Olaeta', N'Calle Sexta, 123', N'Milwaukee', N'WI', N'99999', N'España', 300.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-06-08 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(75, 4, 8, CAST(N'2006-06-05 00:00:00.000' AS DateTime), CAST(N'2006-06-05 00:00:00.000' AS DateTime), 2, N'Elizabeth Andersen', N'Calle Octava, 123', N'Portland', N'OR', N'99999', N'España', 50.0000, 0.0000, N'Cheque', CAST(N'2006-06-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(76, 9, 25, CAST(N'2006-06-05 00:00:00.000' AS DateTime), CAST(N'2006-06-05 00:00:00.000' AS DateTime), 1, N'John Rodman', N'Calle Vigesimoquinta, 123', N'Chicago', N'IL', N'99999', N'España', 5.0000, 0.0000, N'Efectivo', CAST(N'2006-06-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(77, 9, 26, CAST(N'2006-06-05 00:00:00.000' AS DateTime), CAST(N'2006-06-05 00:00:00.000' AS DateTime), 3, N'Run Liu', N'Calle Vigesimosexta, 123', N'Miami', N'FL', N'99999', N'España', 60.0000, 0.0000, N'Tarjeta de crédito', CAST(N'2006-06-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(78, 1, 29, CAST(N'2006-06-05 00:00:00.000' AS DateTime), CAST(N'2006-06-05 00:00:00.000' AS DateTime), 2, N'Soo Jung Lee', N'Calle Vigesimonovena, 123', N'Denver', N'CO', N'99999', N'España', 200.0000, 0.0000, N'Cheque', CAST(N'2006-06-05 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(79, 2, 6, CAST(N'2006-06-23 00:00:00.000' AS DateTime), CAST(N'2006-06-23 00:00:00.000' AS DateTime), 3, N'Francisco Pérez-Olaeta', N'Calle Sexta, 123', N'Milwaukee', N'WI', N'99999', N'España', 0.0000, 0.0000, N'Cheque', CAST(N'2006-06-23 00:00:00.000' AS DateTime), NULL, 0, NULL, 3)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(80, 2, 4, CAST(N'2006-04-25 17:03:55.000' AS DateTime), NULL, NULL, N'Christina Lee', N'Calle Cuarta, 123', N'Nueva York', N'NY', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 0)
INSERT [Ventas].[Pedido]
	([cod_ped_in], [cod_emp_in], [cod_cli_in], [fec_ped_dt], [fec_env_ped_dt], [cod_tra_in], [nom_env_ped_vc], [dir_des_env_ped_vc], [ciu_des_env_ped_vc], [prv_des_env_ped_vc], [cod_pos_des_env_ped_vc], [pai_des_env_ped_vc], [gas_env_ped_mo], [imp_ped_mo], [tip_pag_ped_vc], [fec_pag_ped_dt], [obs_ped_vc], [tip_imp_ped_in], [cod_est_ped_imp_ti], [cod_est_ped_ti])
VALUES
	(81, 2, 3, CAST(N'2006-04-25 17:26:53.000' AS DateTime), NULL, NULL, N'Thomas Axen', N'Calle Tercera, 123', N'Los Ángeles', N'CA', N'99999', N'España', 0.0000, 0.0000, NULL, NULL, NULL, 0, NULL, 0)
SET IDENTITY_INSERT [Ventas].[Pedido] OFF
SET IDENTITY_INSERT [Ventas].[PedidoDetalle] ON

INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(27, 30, 34, CAST(100.00 AS Decimal(12, 2)), 14.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 96, 83)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(28, 30, 80, CAST(30.00 AS Decimal(12, 2)), 3.5000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 63)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(29, 31, 7, CAST(10.00 AS Decimal(12, 2)), 30.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 64)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(30, 31, 51, CAST(10.00 AS Decimal(12, 2)), 53.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 65)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(31, 31, 80, CAST(10.00 AS Decimal(12, 2)), 3.5000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 66)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(32, 32, 1, CAST(15.00 AS Decimal(12, 2)), 18.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 67)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(33, 32, 43, CAST(20.00 AS Decimal(12, 2)), 46.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 68)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(34, 33, 19, CAST(30.00 AS Decimal(12, 2)), 9.2000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 97, 81)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(35, 34, 19, CAST(20.00 AS Decimal(12, 2)), 9.2000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 69)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(36, 35, 48, CAST(10.00 AS Decimal(12, 2)), 12.7500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 70)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(37, 36, 41, CAST(200.00 AS Decimal(12, 2)), 9.6500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 98, 79)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(38, 37, 8, CAST(17.00 AS Decimal(12, 2)), 40.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 71)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(39, 38, 43, CAST(300.00 AS Decimal(12, 2)), 46.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 99, 77)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(40, 39, 48, CAST(100.00 AS Decimal(12, 2)), 12.7500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 100, 75)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(41, 40, 81, CAST(200.00 AS Decimal(12, 2)), 2.9900, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 101, 73)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(42, 41, 43, CAST(300.00 AS Decimal(12, 2)), 46.0000, CAST(0.00 AS Decimal(12, 2)), 1, NULL, 102, 104)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(43, 42, 6, CAST(10.00 AS Decimal(12, 2)), 25.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 84)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(44, 42, 4, CAST(10.00 AS Decimal(12, 2)), 22.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 85)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(45, 42, 19, CAST(10.00 AS Decimal(12, 2)), 9.2000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 103, 110)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(46, 43, 80, CAST(20.00 AS Decimal(12, 2)), 3.5000, CAST(0.00 AS Decimal(12, 2)), 1, NULL, NULL, 86)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(47, 43, 81, CAST(50.00 AS Decimal(12, 2)), 2.9900, CAST(0.00 AS Decimal(12, 2)), 1, NULL, NULL, 87)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(48, 44, 1, CAST(25.00 AS Decimal(12, 2)), 18.0000, CAST(0.00 AS Decimal(12, 2)), 1, NULL, NULL, 88)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(49, 44, 43, CAST(25.00 AS Decimal(12, 2)), 46.0000, CAST(0.00 AS Decimal(12, 2)), 1, NULL, NULL, 89)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(50, 44, 81, CAST(25.00 AS Decimal(12, 2)), 2.9900, CAST(0.00 AS Decimal(12, 2)), 1, NULL, NULL, 90)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(51, 45, 41, CAST(50.00 AS Decimal(12, 2)), 9.6500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 104, 116)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(52, 45, 40, CAST(50.00 AS Decimal(12, 2)), 18.4000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 91)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(53, 46, 57, CAST(100.00 AS Decimal(12, 2)), 19.5000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 105, 101)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(54, 46, 72, CAST(50.00 AS Decimal(12, 2)), 34.8000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 106, 114)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(55, 47, 34, CAST(300.00 AS Decimal(12, 2)), 14.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 107, 108)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(56, 48, 8, CAST(25.00 AS Decimal(12, 2)), 40.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 108, 106)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(57, 48, 19, CAST(25.00 AS Decimal(12, 2)), 9.2000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 109, 112)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(59, 50, 21, CAST(20.00 AS Decimal(12, 2)), 10.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 92)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(60, 51, 5, CAST(25.00 AS Decimal(12, 2)), 21.3500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 93)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(61, 51, 41, CAST(30.00 AS Decimal(12, 2)), 9.6500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 94)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(62, 51, 40, CAST(30.00 AS Decimal(12, 2)), 18.4000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 95)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(66, 56, 48, CAST(10.00 AS Decimal(12, 2)), 12.7500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, 111, 99)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(67, 55, 34, CAST(87.00 AS Decimal(12, 2)), 14.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 117)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(68, 79, 7, CAST(30.00 AS Decimal(12, 2)), 30.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 119)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(69, 79, 51, CAST(30.00 AS Decimal(12, 2)), 53.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 118)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(70, 78, 17, CAST(40.00 AS Decimal(12, 2)), 39.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 120)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(71, 77, 6, CAST(90.00 AS Decimal(12, 2)), 25.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 121)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(72, 76, 4, CAST(30.00 AS Decimal(12, 2)), 22.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 122)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(73, 75, 48, CAST(40.00 AS Decimal(12, 2)), 12.7500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 123)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(74, 74, 48, CAST(40.00 AS Decimal(12, 2)), 12.7500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 124)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(75, 73, 41, CAST(10.00 AS Decimal(12, 2)), 9.6500, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 125)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(76, 72, 43, CAST(5.00 AS Decimal(12, 2)), 46.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 126)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(77, 71, 40, CAST(40.00 AS Decimal(12, 2)), 18.4000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 127)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(78, 70, 8, CAST(20.00 AS Decimal(12, 2)), 40.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 128)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(79, 69, 80, CAST(15.00 AS Decimal(12, 2)), 3.5000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 129)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(80, 67, 74, CAST(20.00 AS Decimal(12, 2)), 10.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 130)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(81, 60, 72, CAST(40.00 AS Decimal(12, 2)), 34.8000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 131)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(82, 63, 3, CAST(50.00 AS Decimal(12, 2)), 10.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 132)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(83, 63, 8, CAST(3.00 AS Decimal(12, 2)), 40.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 133)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(84, 58, 20, CAST(40.00 AS Decimal(12, 2)), 81.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 134)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(85, 58, 52, CAST(40.00 AS Decimal(12, 2)), 7.0000, CAST(0.00 AS Decimal(12, 2)), 2, NULL, NULL, 135)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(86, 80, 56, CAST(10.00 AS Decimal(12, 2)), 38.0000, CAST(0.00 AS Decimal(12, 2)), 1, NULL, NULL, 136)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(90, 81, 81, CAST(0.00 AS Decimal(12, 2)), 2.9900, CAST(0.00 AS Decimal(12, 2)), 5, NULL, NULL, NULL)
INSERT [Ventas].[PedidoDetalle]
	([cod_ped_det_in], [cod_ped_in], [cod_prd_in], [cnt_ped_det_de], [pre_ven_ped_det_de], [des_ped_det_de], [cod_est_ped_det_ti], [fec_ent_ped_det_dt], [cod_ped_com_in], [cod_tra_inv_in])
VALUES
	(91, 81, 56, CAST(0.00 AS Decimal(12, 2)), 38.0000, CAST(0.00 AS Decimal(12, 2)), 0, NULL, NULL, NULL)
SET IDENTITY_INSERT [Ventas].[PedidoDetalle] OFF
ALTER TABLE [Almacen].[TransaccionInventario]  WITH CHECK ADD  CONSTRAINT [fk_transaccioninventario_producto_cod_prd_in] FOREIGN KEY([cod_prd_in])
REFERENCES [Almacen].[Producto] ([cod_prd_in])
GO
ALTER TABLE [Almacen].[TransaccionInventario] CHECK CONSTRAINT [fk_transaccioninventario_producto_cod_prd_in]
GO
ALTER TABLE [Almacen].[TransaccionInventario]  WITH CHECK ADD  CONSTRAINT [fk_transaccioninventario_tipotransaccionInventario_cod_tip_tra_inv_ti] FOREIGN KEY([cod_tip_tra_inv_ti])
REFERENCES [Almacen].[TipoTransaccionInventario] ([cod_tip_tra_inv_ti])
GO
ALTER TABLE [Almacen].[TransaccionInventario] CHECK CONSTRAINT [fk_transaccioninventario_tipotransaccionInventario_cod_tip_tra_inv_ti]
GO
ALTER TABLE [Compras].[PedidoCompra]  WITH CHECK ADD  CONSTRAINT [fk_pedidocompra_empleado_cod_emp_in] FOREIGN KEY([cod_emp_in])
REFERENCES [Personal].[Empleado] ([cod_emp_in])
GO
ALTER TABLE [Compras].[PedidoCompra] CHECK CONSTRAINT [fk_pedidocompra_empleado_cod_emp_in]
GO
ALTER TABLE [Compras].[PedidoCompra]  WITH CHECK ADD  CONSTRAINT [fk_pedidocompra_estadopedidocompra_cod_est_ped_com_ti] FOREIGN KEY([cod_est_ped_com_ti])
REFERENCES [Compras].[EstadoPedidoCompra] ([cod_est_ped_com_ti])
GO
ALTER TABLE [Compras].[PedidoCompra] CHECK CONSTRAINT [fk_pedidocompra_estadopedidocompra_cod_est_ped_com_ti]
GO
ALTER TABLE [Compras].[PedidoCompra]  WITH CHECK ADD  CONSTRAINT [fk_pedidocompra_proveedor_cod_prv_in] FOREIGN KEY([cod_prv_in])
REFERENCES [Compras].[Proveedor] ([cod_prv_in])
GO
ALTER TABLE [Compras].[PedidoCompra] CHECK CONSTRAINT [fk_pedidocompra_proveedor_cod_prv_in]
GO
ALTER TABLE [Compras].[PedidoCompraDetalle]  WITH CHECK ADD  CONSTRAINT [fk_pedidocompradetalle_pedidocompra_cod_ped_com_in] FOREIGN KEY([cod_ped_com_in])
REFERENCES [Compras].[PedidoCompra] ([cod_ped_com_in])
GO
ALTER TABLE [Compras].[PedidoCompraDetalle] CHECK CONSTRAINT [fk_pedidocompradetalle_pedidocompra_cod_ped_com_in]
GO
ALTER TABLE [Compras].[PedidoCompraDetalle]  WITH CHECK ADD  CONSTRAINT [fk_pedidocompradetalle_transaccioninventario_cod_tra_inv_in] FOREIGN KEY([cod_tra_inv_in])
REFERENCES [Almacen].[TransaccionInventario] ([cod_tra_inv_in])
GO
ALTER TABLE [Compras].[PedidoCompraDetalle] CHECK CONSTRAINT [fk_pedidocompradetalle_transaccioninventario_cod_tra_inv_in]
GO
ALTER TABLE [Facturacion].[Factura]  WITH CHECK ADD  CONSTRAINT [fk_factura_pedido_cod_ped_in] FOREIGN KEY([cod_ped_in])
REFERENCES [Ventas].[Pedido] ([cod_ped_in])
GO
ALTER TABLE [Facturacion].[Factura] CHECK CONSTRAINT [fk_factura_pedido_cod_ped_in]
GO
ALTER TABLE [Sistemas].[PrivilegioEmpleado]  WITH CHECK ADD  CONSTRAINT [fk_privilegioempleado_empleado_cod_emp_in] FOREIGN KEY([cod_emp_in])
REFERENCES [Personal].[Empleado] ([cod_emp_in])
GO
ALTER TABLE [Sistemas].[PrivilegioEmpleado] CHECK CONSTRAINT [fk_privilegioempleado_empleado_cod_emp_in]
GO
ALTER TABLE [Sistemas].[PrivilegioEmpleado]  WITH CHECK ADD  CONSTRAINT [fk_privilegioempleado_privilegio_cod_pri_in] FOREIGN KEY([cod_pri_in])
REFERENCES [Sistemas].[Privilegio] ([cod_pri_in])
GO
ALTER TABLE [Sistemas].[PrivilegioEmpleado] CHECK CONSTRAINT [fk_privilegioempleado_privilegio_cod_pri_in]
GO
ALTER TABLE [Ventas].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_pedido_cliente_cod_cli_in] FOREIGN KEY([cod_cli_in])
REFERENCES [Ventas].[Cliente] ([cod_cli_in])
GO
ALTER TABLE [Ventas].[Pedido] CHECK CONSTRAINT [fk_pedido_cliente_cod_cli_in]
GO
ALTER TABLE [Ventas].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_pedido_empleado_cod_emp_in] FOREIGN KEY([cod_emp_in])
REFERENCES [Personal].[Empleado] ([cod_emp_in])
GO
ALTER TABLE [Ventas].[Pedido] CHECK CONSTRAINT [fk_pedido_empleado_cod_emp_in]
GO
ALTER TABLE [Ventas].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_pedido_estadopedido_cod_est_ped_ti] FOREIGN KEY([cod_est_ped_ti])
REFERENCES [dbo].[EstadoPedido] ([cod_est_ped_ti])
GO
ALTER TABLE [Ventas].[Pedido] CHECK CONSTRAINT [fk_pedido_estadopedido_cod_est_ped_ti]
GO
ALTER TABLE [Ventas].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_pedido_estadopedidoimpuesto_cod_est_ped_imp_ti] FOREIGN KEY([cod_est_ped_imp_ti])
REFERENCES [Facturacion].[EstadoPedidoImpuesto] ([cod_est_ped_imp_ti])
GO
ALTER TABLE [Ventas].[Pedido] CHECK CONSTRAINT [fk_pedido_estadopedidoimpuesto_cod_est_ped_imp_ti]
GO
ALTER TABLE [Ventas].[Pedido]  WITH CHECK ADD  CONSTRAINT [fk_pedido_transportista_cod_tra_in] FOREIGN KEY([cod_tra_in])
REFERENCES [Transporte].[Transportista] ([cod_tra_in])
GO
ALTER TABLE [Ventas].[Pedido] CHECK CONSTRAINT [fk_pedido_transportista_cod_tra_in]
GO
ALTER TABLE [Ventas].[PedidoDetalle]  WITH CHECK ADD  CONSTRAINT [fk_pedidodetalle_estadopedidodetalle_cod_est_ped_det_ti] FOREIGN KEY([cod_est_ped_det_ti])
REFERENCES [dbo].[EstadoPedidoDetalle] ([cod_est_ped_det_ti])
GO
ALTER TABLE [Ventas].[PedidoDetalle] CHECK CONSTRAINT [fk_pedidodetalle_estadopedidodetalle_cod_est_ped_det_ti]
GO
ALTER TABLE [Ventas].[PedidoDetalle]  WITH CHECK ADD  CONSTRAINT [fk_pedidodetalle_pedido_cod_ped_in] FOREIGN KEY([cod_ped_in])
REFERENCES [Ventas].[Pedido] ([cod_ped_in])
GO
ALTER TABLE [Ventas].[PedidoDetalle] CHECK CONSTRAINT [fk_pedidodetalle_pedido_cod_ped_in]
GO
USE [master]
GO
ALTER DATABASE [NorthWindSQL] SET  READ_WRITE 
GO
