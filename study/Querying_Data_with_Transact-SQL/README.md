## [Querying Data with Transact-SQL](https://github.com/MicrosoftLearning/QueryingT-SQL)

Transact-SQL is an essential skill for database professionals and
developers working with SQL databases.
This course takes you from your first `SELECT` statement to
implementing transactional programmatic logic through a combination
of expert instruction, demonstrations, and practical labs.

- [Before You Start](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/study/Querying_Data_with_Transact-SQL/before_you_start)
  - [Introduction]()
- [Section $`1`$: Modules $`1-2`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/study/Querying_Data_with_Transact-SQL/section_1)
  - Module $`1`$: Introduction to Transact-SQL
  - Lab $`1`$: Introduction to Transact-SQL *Assessment*
  - Module $`2`$: Querying Tables with `SELECT`
  - Lab $`2`$: Querying Tables with `SELECT` *Assessment*
- [Section $`2`$: Modules $`3-5`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/study/Querying_Data_with_Transact-SQL/section_2)
  - Module $`3`$: Querying Multiple Tables with Joins
  - Lab $`3`$: Querying Multiple Tables with Joins *Assessment*
  - Module $`4`$: Using Set Operators
  - Lab $`4`$: Using Set Operators *Assessment*
  - Module $`5`$: Using Functions and Aggregating Data
  - Lab $`5`$: Using Functions and Aggregating Data *Assessment*
- [Section $`3`$: Modules $`6-8`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/study/Querying_Data_with_Transact-SQL/section_3)
  - Module $`6`$: Using Subqueries and APPLY
  - Lab $`6`$: Using Subqueries and APPLY *Assessment*
  - Module $`7`$: Using Table Expressions
  - Lab $`7`$: Using Table Expressions *Assessment*
  - Module $`8`$: Grouping Sets and Pivoting Data
  - Lab $`8`$: Grouping Sets and Pivoting Data *Assessment*
- [Section $`4`$: Modules $`9-11`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/study/Querying_Data_with_Transact-SQL/section_4)
  - Module $`9`$: Modifying Data
  - Lab $`9`$: Modifying Data *Assessment*
  - Module $`10`$: Programming with Transact-SQL
  - Lab $`10`$: Programming with Transact-SQL *Assessment*
  - Module $`11`$: Error Handling and Transactions
  - Lab $`11`$: Error Handling and Transactions *Assessment*
- [Final Assessment]()