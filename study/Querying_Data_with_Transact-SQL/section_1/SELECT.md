## The SELECT Statement

![T-SQL_M01_V02-en](../../../videos/english/T-SQL_M01_V02-en.mp4)
[Captions-T-SQL_M01_V02-en](../../../videos/english/T-SQL_M01_V02-en.txt)

## Demo: Using SELECT

![T-SQL_M01_V03-en](../../../videos/english/T-SQL_M01_V03-en.mp4)
[Captions-T-SQL_M01_V03-en](../../../videos/english/T-SQL_M01_V03-en.txt)

```sql
SELECT 'Hello Graeme';
```

```sql
SELECT * FROM SalesLT.Product;
```

```sql
SELECT ProductID, Name, ListPrice, StandardCost
FROM SalesLT.Product;
```