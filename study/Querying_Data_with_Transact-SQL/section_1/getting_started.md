## Getting Started with Transact-SQL

![T-SQL_M01_V01-en](../../../videos/english/T-SQL_M01_V01-en.mp4)
[Captions-T-SQL_M01_V01-en](../../../videos/english/T-SQL_M01_V01-en.txt)

## Key Points and Additional Reading

### Key Points
* Transact-SQL is the language used to query data in Microsoft SQL Server and Azure SQL Database.
* Data is stored in tables, which may be related to one another through common key fields.
* Objects in a database are organized into schemas.
* The fully qualified naming syntax for an object is *server_name.database_name.schema_name.object_name*, but in most cases you can abbreviate this to *schema_name.object_name*.

### Additional Reading
Throughout this course, links to specific sections in the
Transact-SQL Reference documentation will be provided to supplement
the course materials.
Take a look at the [overview page](https://docs.microsoft.com/en-us/sql/t-sql/language-reference?view=sql-server-ver15)
for this reference.