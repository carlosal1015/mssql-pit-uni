/* List databases in the system. */
SELECT name FROM master.dbo.sysdatabases;
GO

USE AdventureWorks;
GO

/* 
* Check the size of current database
* https://stackoverflow.com/a/18014581/9302545
*/
SELECT 
      database_name = DB_NAME(database_id)
    , log_size_mb = CAST(SUM(CASE WHEN type_desc = 'LOG' THEN size END) * 8. / 1024 AS DECIMAL(8,2))
    , row_size_mb = CAST(SUM(CASE WHEN type_desc = 'ROWS' THEN size END) * 8. / 1024 AS DECIMAL(8,2))
    , total_size_mb = CAST(SUM(size) * 8. / 1024 AS DECIMAL(8,2))
FROM sys.master_files WITH(NOWAIT)
WHERE database_id = DB_ID() -- for current db 
GROUP BY database_id;
GO

/* List tables on the database. */
SELECT * FROM information_schema.tables;
GO

SELECT Name, StandardCost, ListPrice
FROM Production.Product;


SELECT Name, ListPrice - StandardCost
FROM Production.Product;


SELECT Name, ListPrice - StandardCost AS Markup
FROM Production.Product;


SELECT ProductNumber, Color, Size, Color + ', ' + Size AS ProductDetails
FROM Production.Product;


SELECT ProductID + ': ' + Name
FROM Production.Product;