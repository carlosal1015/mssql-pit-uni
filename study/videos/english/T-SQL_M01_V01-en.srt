0
00:00:02,149 --> 00:00:06,259
So here we are in module one. Jeff we're raring to go, we're ready

1
00:00:06,259 --> 00:00:09,320
to cover some Transact-SQL and will cover some

2
00:00:09,320 --> 00:00:13,840
basic selecting in this module. We'll cover the basics of

3
00:00:13,840 --> 00:00:15,240
using the SELECT keyword

4
00:00:15,240 --> 00:00:20,240
and getting started with Transact-SQL. Here's what we're going to cover in  the

5
00:00:20,240 --> 00:00:20,740
module.

6
00:00:20,740 --> 00:00:24,800
There's an introduction to Transact-SQL itself, 

7
00:00:24,800 --> 00:00:28,060
we'll talk a little bit about relational databases. This isn't a database design

8
00:00:28,060 --> 00:00:30,060
course. Jeff I know you're dying to

9
00:00:30,060 --> 00:00:34,219
dive in there and talk about normalization and all that stuff. We'll really just give you

10
00:00:34,219 --> 00:00:37,699
enough information to understand the context within which we will use

11
00:00:37,699 --> 00:00:38,210
Transact-SQL.

12
00:00:38,210 --> 00:00:41,589
If you do want to learn more about  creating databases and designing

13
00:00:41,589 --> 00:00:42,249
databases,

14
00:00:42,249 --> 00:00:45,579
there's lots of other courses that you can take a look at that will

15
00:00:45,579 --> 00:00:46,530
cover that.

16
00:00:46,530 --> 00:00:51,039
So we'll look at that, we'll look at some details of how we relate to objects in the

17
00:00:51,039 --> 00:00:51,649
database

18
00:00:51,649 --> 00:00:55,600
and then we'll dive into actually using Transact-SQL itself and then get

19
00:00:55,600 --> 00:00:59,809
ourselves started. So by the end of this module, we'll have written our first Transact-SQL queries.

20
00:00:59,809 --> 00:01:05,280
So let's start with a little bit about  what actually Transact-SQL is.

21
00:01:05,280 --> 00:01:08,840
And the SQL part of it or the S Q L

22
00:01:08,840 --> 00:01:13,039
stands for Structured Query  Language and it was developed

23
00:01:13,039 --> 00:01:17,619
by IBM in the 1970s as a way of interrogating

24
00:01:17,619 --> 00:01:21,719
databases which is where we are storing information that applications use

25
00:01:21,719 --> 00:01:22,320
typically.

26
00:01:22,320 --> 00:01:26,750
So it has been since adopted as a standard, the

27
00:01:26,750 --> 00:01:29,640
the American National Standards Institute and the International

28
00:01:29,640 --> 00:01:30,780
Standards Organization

29
00:01:30,780 --> 00:01:35,100
have adopted it and as a result of that, you'll find that it's widely used across lots

30
00:01:35,100 --> 00:01:37,240
of different database platforms. It isn't

31
00:01:37,240 --> 00:01:41,740
a Microsoft specific thing, it's a language that is used by database

32
00:01:41,740 --> 00:01:43,820
platforms from all different vendors and

33
00:01:43,820 --> 00:01:47,679
and all over the world. So

34
00:01:47,679 --> 00:01:51,469
having said that each vendor tends to add it's own

35
00:01:51,469 --> 00:01:55,340
little idiosyncrasies to the language or little extensions to the

36
00:01:55,340 --> 00:01:57,030
language. And Microsoft is no different.

37
00:01:57,030 --> 00:02:00,579
Microsoft's implementation is referred to as Transact-SQL or

38
00:02:00,579 --> 00:02:06,749
sometimes T-SQL. And it's largely similar to SQL on any other

39
00:02:06,749 --> 00:02:11,009
platform that you might have used. If you've used Oracle with PL/SQL which is the the language

40
00:02:11,008 --> 00:02:11,500
there

41
00:02:11,500 --> 00:02:13,980
or you've used other SQL based databases,

42
00:02:13,980 --> 00:02:17,239
you'll find a lot of the syntax  very similar and pretty much

43
00:02:17,239 --> 00:02:17,849
standard.

44
00:02:17,849 --> 00:02:22,180
But there are a few Microsoft specific twists on it in Transact-SQL.

45
00:02:22,180 --> 00:02:25,599
And it's the query language that is used for Microsoft's database

46
00:02:25,599 --> 00:02:29,920
products. And those database products include SQL Server which is the

47
00:02:29,920 --> 00:02:35,239
enterprise scale database server that you can install on your own hardware or now

48
00:02:35,239 --> 00:02:36,220
you can run it in

49
00:02:36,220 --> 00:02:40,260
a virtual machine in Azure. And there is also Azure SQL Database 

50
00:02:40,260 --> 00:02:42,099
which is our platform as a service

51
00:02:42,099 --> 00:02:45,700
so that's some the place we can use it. So, do you get all the T-SQL in the Azure

52
00:02:45,700 --> 00:02:50,260
SQL Database? Is it exactly the same as SQL Server? That's a good point.

53
00:02:50,260 --> 00:02:55,269
Azure SQL Database supports a pretty large subset

54
00:02:55,269 --> 00:02:59,400
of the full syntax that's supported in SQL Server.

55
00:02:59,400 --> 00:03:03,170
There are a number things the are specific to SQL Server because they

56
00:03:03,170 --> 00:03:04,989
relate to local files or

57
00:03:04,989 --> 00:03:08,579
to the other are is the common language runtime

58
00:03:08,579 --> 00:03:12,049
data types. There are things we can do in SQL Server that involve

59
00:03:12,049 --> 00:03:15,859
writing code in .NET rather than Transact-SQL and then wrapping them up.

60
00:03:15,859 --> 00:03:19,540
Which we don't necessarily want to do in Azure SQL Database.

61
00:03:19,540 --> 00:03:21,340
So, there are some things that are SQL Server only,

62
00:03:21,340 --> 00:03:25,590
there's an increasing body of things that are supported in Azure SQL

63
00:03:25,590 --> 00:03:26,120
Database.

64
00:03:26,120 --> 00:03:30,260
And what you'll find is Azure SQL Database is constantly being updated

65
00:03:30,260 --> 00:03:32,919
with new features, and new support for

66
00:03:32,919 --> 00:03:36,590
Transact-SQL constructs that were previously only available in SQL

67
00:03:36,590 --> 00:03:37,329
Server.

68
00:03:37,329 --> 00:03:41,819
So as you use it, check with the latest version, have a look at the documentation

69
00:03:41,819 --> 00:03:42,530
and see what's

70
00:03:42,530 --> 00:03:47,799
available to use. The other point that's worth making about

71
00:03:47,799 --> 00:03:52,030
the SQL language generally is it's a declarative language not a

72
00:03:52,030 --> 00:03:53,440
procedural language.

73
00:03:53,440 --> 00:03:57,069
What do I mean by that? Well if your programmer,

74
00:03:57,069 --> 00:04:01,190
you've probably written some procedural programming. So maybe Jeff in

75
00:04:01,190 --> 00:04:04,620
C# or something you might write a procedure where you do one thing then do

76
00:04:04,620 --> 00:04:05,459
the next thing, and

77
00:04:05,459 --> 00:04:08,790
maybe you loop around to do a number things a certain number times,

78
00:04:08,790 --> 00:04:12,480
but you're specifying the steps. You're specifying the procedure.

79
00:04:12,480 --> 00:04:16,099
SQL is slightly different from that. In SQL

80
00:04:16,099 --> 00:04:19,739
we generally work with what we call sets of data.

81
00:04:19,738 --> 00:04:22,849
If you're familiar with set theory which is

82
00:04:22,849 --> 00:04:25,940
a branch of mathematics, you're use to the idea of collections of objects.

83
00:04:25,940 --> 00:04:30,190
And you typically draw venn diagrams to categorize different sets

84
00:04:30,190 --> 00:04:30,760
together.

85
00:04:30,760 --> 00:04:35,030
SQL is a language that builds on that branch of mathematics. It is very much

86
00:04:35,030 --> 00:04:35,920
about dealing with

87
00:04:35,920 --> 00:04:40,810
multiple instances of objects that are arranged into collections or sets.

88
00:04:40,810 --> 00:04:44,470
And what you typically do, the best piece of advice I can give you when you're

89
00:04:44,470 --> 00:04:45,700
working with SQL is

90
00:04:45,700 --> 00:04:49,740
describe what it is you want. When you're writing a query that brings data from

91
00:04:49,740 --> 00:04:50,350
the tables,

92
00:04:50,350 --> 00:04:54,230
you're going to use the language to express what the result you want to get.

93
00:04:54,230 --> 00:04:57,820
Don't express how you want to get that result or what the steps are.

94
00:04:57,820 --> 00:05:02,030
So you're not doing a row a time. No, typically you are not.

95
00:05:02,030 --> 00:05:05,450
There's the odd occasion when you might do something iterative,

96
00:05:05,450 --> 00:05:06,800
and later in the course we'll 

97
00:05:06,800 --> 00:05:11,600
talk about how we can write programs effectively

98
00:05:11,600 --> 00:05:12,630
using SQL

99
00:05:12,630 --> 00:05:16,330
in a procedural style. But if you're doing that a lot, if your building a database

100
00:05:16,330 --> 00:05:19,810
solution and you're writing a lot of procedural code in SQL, 

101
00:05:19,810 --> 00:05:23,110
chances are there's a better way you could be doing things that will perform

102
00:05:23,110 --> 00:05:24,530
better, will work better

103
00:05:24,530 --> 00:05:28,720
if you use this declarative set based approach. And that's what we're going to be

104
00:05:28,720 --> 00:05:33,540
focusing on through the course. So

105
00:05:33,540 --> 00:05:38,570
relational databases, Jeff, tell me what you know about relational databases?

106
00:05:38,570 --> 00:05:42,040
Typically you've got multiple tables and

107
00:05:42,040 --> 00:05:45,130
being relational, they relate to each other. So

108
00:05:45,130 --> 00:05:48,640
each table represents a type

109
00:05:48,640 --> 00:05:52,419
thing. So what we call an entity would be a 

110
00:05:52,419 --> 00:05:56,700
person, it could be, but it could be something you can say is well,

111
00:05:56,700 --> 00:05:57,610
it can be an order.

112
00:05:57,610 --> 00:06:03,240
Okay. And each table is designed to contain one type of

113
00:06:03,240 --> 00:06:07,660
thing. Okay. And we tend to talk about these entities in the 

114
00:06:07,660 --> 00:06:12,150
kind of classical academic world, they're referred to as relations, day-to-day we call 

115
00:06:12,150 --> 00:06:14,490
them tables. So if you see somebody talking relations we're

116
00:06:14,490 --> 00:06:18,100
talking about tables. And similarly, sometimes people will say that

117
00:06:18,100 --> 00:06:19,770
relations have domains.

118
00:06:19,770 --> 00:06:23,080
Well, really those are columns. That's the  term we generally mean.

119
00:06:23,080 --> 00:06:26,380
And what we're saying is those are the attributes of that entity.

120
00:06:26,380 --> 00:06:30,000
So a product entity would represent as a table

121
00:06:30,000 --> 00:06:33,610
and that table may have columns that represent different attributes of a

122
00:06:33,610 --> 00:06:34,590
product. So

123
00:06:34,590 --> 00:06:36,100
its name, its price

124
00:06:36,100 --> 00:06:41,570
its color, that type of thing. The other thing and we're not going to dive into this too

125
00:06:41,570 --> 00:06:44,830
deeply, but you mentioned they have relationships and that's interesting as well.

126
00:06:44,830 --> 00:06:49,200
Most databases are what we call normalized, not all of them are, there

127
00:06:49,200 --> 00:06:53,480
some special types of databases were we deliberately denormalize. But

128
00:06:53,480 --> 00:06:56,620
we're not going to get into any of that in any detail. What we are going to talk about

129
00:06:56,620 --> 00:06:57,670
is the idea that

130
00:06:57,670 --> 00:07:02,030
we have these multiple tables. What we try and do in each table is

131
00:07:02,030 --> 00:07:05,130
have a unique identifier called its primary key

132
00:07:05,130 --> 00:07:08,930
that uniquely identifies a row in that table. An instance of the entity.

133
00:07:08,930 --> 00:07:12,840
So we can uniquely identify a product perhaps by some sort of unique product

134
00:07:12,840 --> 00:07:13,420
id.

135
00:07:13,420 --> 00:07:16,770
And similarly, we might uniquely identify sales order by a

136
00:07:16,770 --> 00:07:21,940
sales order number or something like that. We then relate those entities to 

137
00:07:21,940 --> 00:07:24,980
each other. We related in this case a product to a sales order,

138
00:07:24,980 --> 00:07:28,490
which order did you buy, using a relationship between

139
00:07:28,490 --> 00:07:32,740
the primary key that uniquely identifies a product and a key value

140
00:07:32,740 --> 00:07:36,930
in the order table which we will call foreign key. And that foreign key,

141
00:07:36,930 --> 00:07:40,760
it's just effectively a look up to the other table. So if you think about how

142
00:07:40,760 --> 00:07:41,830
you might

143
00:07:41,830 --> 00:07:45,670
track orders in something like Excel, well you've got a spreadsheet,

144
00:07:45,670 --> 00:07:49,560
it might just be one massive table and you keep writing in the product name

145
00:07:49,560 --> 00:07:51,270
every time somebody buys a product.

146
00:07:51,270 --> 00:07:54,970
The point of a relational database is to not have that duplication.

147
00:07:54,970 --> 00:07:58,000
So we have one product table, one order table

148
00:07:58,000 --> 00:08:01,240
and we look up the product key to get the details of the product.

149
00:08:01,240 --> 00:08:04,460
Does that makes sense? I think that's probably enough in terms of

150
00:08:04,460 --> 00:08:08,190
understanding normalization for our purposes. The only think I'd say is 

151
00:08:08,190 --> 00:08:08,720
that if

152
00:08:08,720 --> 00:08:13,090
people are coming entirely new to databases,

153
00:08:13,090 --> 00:08:17,100
that it's really important to look into database design as another topic.

154
00:08:17,100 --> 00:08:20,680
Because like many people, my first database

155
00:08:20,680 --> 00:08:23,940
project many many many years ago, I hadn't done that

156
00:08:23,940 --> 00:08:28,400
and then down the line you cause  yourself a lot of work.

157
00:08:28,400 --> 00:08:32,200
Yeah, absolutely understanding the principles on which it is based is 

158
00:08:32,200 --> 00:08:35,479
really important. For the purpose of learning Transact-SQL

159
00:08:35,479 --> 00:08:40,750
the key thing to understand is the data might be spread across multiple

160
00:08:40,750 --> 00:08:43,210
tables and you might have to somehow create

161
00:08:43,210 --> 00:08:46,680
relationships, figure out how to do those look ups between the tables,

162
00:08:46,680 --> 00:08:51,730
and we'll cover all that as we look at the syntax. So

163
00:08:51,730 --> 00:08:55,889
getting more specific about the way that we work with things in SQL Server

164
00:08:55,889 --> 00:08:57,519
and Azure SQL Database.

165
00:08:57,519 --> 00:09:01,350
We have this notion of something called schemas and we have objects. Now the

166
00:09:01,350 --> 00:09:05,000
objects we'll deal with mostly are tables. There are other types of objects, there's

167
00:09:05,000 --> 00:09:06,850
views and stored procedures and

168
00:09:06,850 --> 00:09:10,620
various things that will meet as we we travel through this. But most of the

169
00:09:10,620 --> 00:09:11,779
time we're talking about tables.

170
00:09:11,779 --> 00:09:16,730
And those tables are arranged into schemas. So the schema is our namespace for the

171
00:09:16,730 --> 00:09:17,660
tables. They're a way of 

172
00:09:17,660 --> 00:09:21,720
having some sort extended name that helps us identify that table.

173
00:09:21,720 --> 00:09:26,160
And actually the fully qualified name of on object in a

174
00:09:26,160 --> 00:09:29,279
databases is the server name

175
00:09:29,279 --> 00:09:33,040
followed by the database name. Because a server could have multiple databases,

176
00:09:33,040 --> 00:09:36,620
followed by the schema name because the database could have multiple schemas,

177
00:09:36,620 --> 00:09:40,290
followed by the individual object names. So that's how we uniquely

178
00:09:40,290 --> 00:09:44,660
identify someone. So, do you have to type all of that in every time you refer to a table?

179
00:09:44,660 --> 00:09:48,050
You can do, and certainly with SQL Server you can.

180
00:09:48,050 --> 00:09:52,069
With Azure SQL Database, there's really no point because in Azure you can

181
00:09:52,069 --> 00:09:53,850
only work in one database at a time.

182
00:09:53,850 --> 00:09:58,319
You can't traversed different databases. So you generally don't use that full

183
00:09:58,319 --> 00:10:02,449
syntax. What you actually do within the context of working in a database, you

184
00:10:02,449 --> 00:10:05,430
usually just take it down to the schema name and the object name.

185
00:10:05,430 --> 00:10:11,019
Now you can omit the schema name, it's an optional part the syntax. You can

186
00:10:11,019 --> 00:10:14,380
just directly refer to an individual table. But that

187
00:10:14,380 --> 00:10:17,939
can cause some problems because tables can be ambiguously

188
00:10:17,939 --> 00:10:21,250
named. So if we have a look at an example here,

189
00:10:21,250 --> 00:10:25,660
I've got two schemas. I've got the Sales schema and the Production schema.

190
00:10:25,660 --> 00:10:29,189
And in the Sales schema, I have a table called Order,

191
00:10:29,189 --> 00:10:33,720
so it's name is Sales.Order because that's the schema. And, I have a table called customer, 

192
00:10:33,720 --> 00:10:34,689
Sales.Customer.

193
00:10:34,689 --> 00:10:37,829
And in the Production table I have a

194
00:10:37,829 --> 00:10:40,970
Production.Product but I also have Production.Order.

195
00:10:40,970 --> 00:10:46,100
Now, that's going to be confusing because over two tables called Order and

196
00:10:46,100 --> 00:10:48,069
if I didn't specify the schema then

197
00:10:48,069 --> 00:10:51,970
it's ambiguous which table I'm going to  connect to. And SQL Server in Azure Database 

198
00:10:51,970 --> 00:10:53,889
use various techniques to

199
00:10:53,889 --> 00:10:57,439
figure out which one should be the default one that goes to/

200
00:10:57,439 --> 00:10:59,779
But generally the general rule is be

201
00:10:59,779 --> 00:11:04,389
explicit. Specify the schema and the object and that way you'll be pretty 

202
00:11:04,389 --> 00:11:05,920
clear about what object you want to 

203
00:11:05,920 --> 00:11:09,720
access. So

204
00:11:09,720 --> 00:11:13,439
we've talked a little bit about the design of the database in the multiple

205
00:11:13,439 --> 00:11:14,879
tables and the way that they relate. 

206
00:11:14,879 --> 00:11:18,209
We've talked a little bit about schemas and and how we use the schema to

207
00:11:18,209 --> 00:11:23,319
uniquely identify the table we want to  access. There are a number of different

208
00:11:23,319 --> 00:11:24,240
elements

209
00:11:24,240 --> 00:11:27,559
or statement types within the sequel language. And

210
00:11:27,559 --> 00:11:30,970
Jeff I'm sure you're familiar with most of these across-the-board.

211
00:11:30,970 --> 00:11:34,910
We're focusing on what we call DML,

212
00:11:34,910 --> 00:11:37,990
Data Manipulation Language, in this course. And that's the

213
00:11:37,990 --> 00:11:41,209
the part of the language that's used for

214
00:11:41,209 --> 00:11:44,800
querying databases. So getting information out of tables, or

215
00:11:44,800 --> 00:11:48,559
updating data that's in tables, or deleting data, that type of thing.

216
00:11:48,559 --> 00:11:53,240
There are some of these other ones, there's DDL, Data Definition Language, which you

217
00:11:53,240 --> 00:11:55,769
would use if you were creating a database or creating

218
00:11:55,769 --> 00:11:59,040
tables. You would use CREAT and ALTER, and DROP 

219
00:11:59,040 --> 00:12:03,079
to do these types of operations. And then there's another kind a branch of the

220
00:12:03,079 --> 00:12:04,600
language that you can think of as

221
00:12:04,600 --> 00:12:09,029
DCL, Data Control Language, where you're setting permissions. Your securing the

222
00:12:09,029 --> 00:12:12,019
objects by assigning which users can access which objects.

223
00:12:12,019 --> 00:12:16,309
We don't cover those, we don't really cover DDL or DCL in this course.

224
00:12:16,309 --> 00:12:19,899
There are other courses if you want to be a database developer,

225
00:12:19,899 --> 00:12:23,910
then you really want to learn the DDL type of stuff, there are database development

226
00:12:23,910 --> 00:12:24,930
courses available.

227
00:12:24,930 --> 00:12:28,329
And if you want to learn how to secure objects, if you want to be a database

228
00:12:28,329 --> 00:12:29,360
administrator,

229
00:12:29,360 --> 00:12:33,360
then we have administration courses in books. And there's all sorts of

230
00:12:33,360 --> 00:12:34,290
information on that.

231
00:12:34,290 --> 00:12:37,899
We're going to focus on DML. Do you have to tell it which one you're going

232
00:12:37,899 --> 00:12:38,829
to be using?

233
00:12:38,829 --> 00:12:41,970
No, the language is pretty rich and the

234
00:12:41,970 --> 00:12:45,529
the same query engine is used for all these different types of

235
00:12:45,529 --> 00:12:50,579
statements. Even although technically CREAT isn't a query, I'm not querying anything 

236
00:12:50,579 --> 00:12:51,569
to get data,

237
00:12:51,569 --> 00:12:54,670
it's the query engine that runs it and it understands

238
00:12:54,670 --> 00:12:57,800
what a SELECT statement is verses what a CREATE statement is.

239
00:12:57,800 --> 00:13:01,620
So you use the same interface and you can mix and match these different types of language.

