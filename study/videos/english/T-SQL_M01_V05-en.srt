0
00:00:04,900 --> 00:00:07,300
So the first thing I'm going to do is use that CAST

1
00:00:07,300 --> 00:00:11,170
function that we talked about. And I'm going to take the ProductID

2
00:00:11,170 --> 00:00:14,570
which is a number, and I'm going to change into a

3
00:00:14,570 --> 00:00:19,890
character, into varchar. So a five character -- maximum five character length of string. 

4
00:00:19,890 --> 00:00:23,020
And I'm going to concatenate that using the plus operator

5
00:00:23,020 --> 00:00:28,640
to the ProductName. So if I just select, and again one of the advantages of using a tool is 

6
00:00:28,640 --> 00:00:30,370
I can select which statements I want to 

7
00:00:30,370 --> 00:00:33,440
run, so it's not going to run anything else other than what is selected.

8
00:00:33,440 --> 00:00:37,140
Quick execute, and sure enough it comes back 

9
00:00:37,140 --> 00:00:41,109
ProductName and it has concatenated the the ProductID. Now if I tried that without

10
00:00:41,109 --> 00:00:42,699
the CAST it would fail because

11
00:00:42,699 --> 00:00:46,339
it would be trying to concatenate a number to a string. So the CAST

12
00:00:46,339 --> 00:00:50,819
is crucial in this case. And, excuse me, the CAST syntax is 

13
00:00:50,819 --> 00:00:54,219
ProductID as and then whatever data type.

14
00:00:54,219 --> 00:00:58,969
CONVERT it does pretty much the same thing. So in this case I'm converting

15
00:00:58,969 --> 00:01:02,780
to a character, the syntax is slightly different: CONVERT 

16
00:01:02,780 --> 00:01:06,370
to a varchar the ProductID and

17
00:01:06,370 --> 00:01:10,759
again I'm just concatenating it. So we'll run that, and it works in exactly the same

18
00:01:10,759 --> 00:01:11,700
way. So

19
00:01:11,700 --> 00:01:15,899
I can see how that works. I mentioned earlier on that the advantage of

20
00:01:15,899 --> 00:01:19,909
CONVERT really comes into its own when you work with dates. You've got 

21
00:01:19,909 --> 00:01:21,340
these options for different date types.

22
00:01:21,340 --> 00:01:25,509
So you can see I'm selecting a column called SellStartDate which is the date we

23
00:01:25,509 --> 00:01:30,079
started selling the product. I'm also then -- so I'm selecting it just as it is in the

24
00:01:30,079 --> 00:01:30,590
database -- 

25
00:01:30,590 --> 00:01:34,969
I'm then selecting it using CONVERT to a character. So converting it to 

26
00:01:34,969 --> 00:01:38,869
an nvarchar, a Unicode character string,

27
00:01:38,869 --> 00:01:43,549
and just calling it ConvertedDate. So just accepting the default conversion options.

28
00:01:43,549 --> 00:01:47,960
And then I've got another column where I'm selecting again SellStartDate, 

29
00:01:47,960 --> 00:01:50,390
converting it, and I've specified this 

30
00:01:50,390 --> 00:01:54,539
code here,126. There are various different codes that reference

31
00:01:54,539 --> 00:01:58,329
formats for dates. And these are documented in the documentation

32
00:01:58,329 --> 00:02:01,320
when you doing the labs, you'll find a the link to that documentation for what the

33
00:02:01,320 --> 00:02:02,200
different codes are.

34
00:02:02,200 --> 00:02:05,469
I happen to know that 126 is the code  for

35
00:02:05,469 --> 00:02:08,619
the ISO standard for date format.

36
00:02:08,619 --> 00:02:12,390
To format this in a way that is standardized across different systems

37
00:02:12,390 --> 00:02:17,890
across the world. And if I run that code

38
00:02:17,890 --> 00:02:21,500
I can see the date, the default formatting for that day is just

39
00:02:21,500 --> 00:02:25,160
simply the the year, the month, the day, and then the time.

40
00:02:25,160 --> 00:02:29,310
When I've just converted it without specifying any sort of format, notice

41
00:02:29,310 --> 00:02:31,180
what is done, it has actually converted to a string that

42
00:02:31,180 --> 00:02:34,630
includes the name of the month, the day, the year

43
00:02:34,630 --> 00:02:40,080
and then you know whether the time is AM or PM. And the ISO format

44
00:02:40,080 --> 00:02:43,099
is the year dash month dash

45
00:02:43,099 --> 00:02:46,700
day, T to indicate we're now going to look at the time,

46
00:02:46,700 --> 00:02:49,940
and then the time in the kind of standard hours, minutes and

47
00:02:49,940 --> 00:02:53,269
seconds. And those second two, they're actually text. Yes.

48
00:02:53,269 --> 00:02:56,370
So if I'm presenting them to Excel or something,

49
00:02:56,370 --> 00:02:59,590
it would see them as text; whereas, the first one it sees as a date.

50
00:02:59,590 --> 00:03:03,090
Yeah. Excel is actually probably not the best example, because Excel is pretty 

51
00:03:03,090 --> 00:03:05,630
smart about recognizing hey that's a date, and it 

52
00:03:05,630 --> 00:03:08,610
then goes and changes its data type. But yeah you're right, a client application

53
00:03:08,610 --> 00:03:12,209
that didn't know would just see that as a string of characters. It wouldn't

54
00:03:12,209 --> 00:03:13,489
identify it as a date.

55
00:03:13,489 --> 00:03:17,470
So the other thing I talked about was

56
00:03:17,470 --> 00:03:21,510
the business of casting numbers. So if I try to CAST 

57
00:03:21,510 --> 00:03:25,040
Size as Integer. You might remember earlier on that size had

58
00:03:25,040 --> 00:03:28,670
both numbers but also M, L, L

59
00:03:28,670 --> 00:03:32,299
medium, S for small, L for large &lt;laughter&gt; I got that wrong. 

60
00:03:32,299 --> 00:03:36,910
So if I just go ahead and say right, I'm assuming everything in that column can be

61
00:03:36,910 --> 00:03:38,459
Cast as an Integer,

62
00:03:38,459 --> 00:03:42,540
then it's going to fail and it tells me why it fails. It failed because I tried to

63
00:03:42,540 --> 00:03:42,970
convert

64
00:03:42,970 --> 00:03:46,180
M for medium to an integer. Well, there isn't an integer

65
00:03:46,180 --> 00:03:49,560
M. So what I might do is

66
00:03:49,560 --> 00:03:54,500
say well look, try to cast it and where you can

67
00:03:54,500 --> 00:03:58,400
cast it as an integer, where you can't return a null.

68
00:03:58,400 --> 00:04:03,170
So if Ido that, sure enough the ones that are numeric

69
00:04:03,170 --> 00:04:06,230
come back as numbers, the ones the aren't come back as

70
00:04:06,230 --> 00:04:10,170
a null value. So it's kind of useful

71
00:04:10,170 --> 00:04:13,700
in that it has got me the numbers. I now need to know how to deal with nulls,

72
00:04:13,700 --> 00:04:17,180
and we'll talk about that a little later. But hopefully in this demo you've

73
00:04:17,180 --> 00:04:18,539
seen some examples of

74
00:04:18,539 --> 00:04:22,520
ways that we can explicitly cast one data type to another and sometimes

75
00:04:22,520 --> 00:04:23,800
that's an important thing that you

76
00:04:23,800 --> 00:04:27,360
have to do when you're working with and tables and you want to start

77
00:04:27,360 --> 00:04:28,729
manipulating the values in

78
00:04:28,729 --> 00:04:28,979
there.

