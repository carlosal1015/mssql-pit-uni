0
00:00:02,020 --> 00:00:05,610
So let's start with the SELECT statement. It's probably the cornerstone of

1
00:00:05,610 --> 00:00:07,990
working with Transact-SQL. It's the

2
00:00:07,990 --> 00:00:12,640
core statement that you would use when you're querying tables to get data

3
00:00:12,640 --> 00:00:17,900
out of the database. And it has a number of clauses, don't worry too much about the

4
00:00:17,900 --> 00:00:20,980
details at this point. We are going to cover each of these different clauses as we go

5
00:00:20,980 --> 00:00:21,759
through the course.

6
00:00:21,759 --> 00:00:24,819
But I wanted to kind of put an example there that kind of shows

7
00:00:24,819 --> 00:00:29,050
almost the full syntax of the SELECT statement.

8
00:00:29,050 --> 00:00:33,170
And you see that it starts with a SELECT. So SELECT

9
00:00:33,170 --> 00:00:36,760
is what we used to specify what columns do we want back. 

10
00:00:36,760 --> 00:00:40,460
What do we want to see in the results. And that could be

11
00:00:40,460 --> 00:00:44,160
all the columns in a table, or it could be specific columns in a table,

12
00:00:44,160 --> 00:00:47,950
or could be columns from multiple tables, or it even be literal values or

13
00:00:47,950 --> 00:00:50,140
expressions. But it is what do I want to  see

14
00:00:50,140 --> 00:00:53,660
in the result. The result of the SELECT statement is always a virtual table.

15
00:00:53,660 --> 00:00:58,450
Next is FROM. So okay well I want to see 

16
00:00:58,450 --> 00:01:01,920
this data. If it's coming from existing tables, which tables is it coming from?

17
00:01:01,920 --> 00:01:05,159
So there's the FROM clause that I use to specify the source of

18
00:01:05,159 --> 00:01:10,130
were that data comes from. And then I might want to filter. I might not want all the rows from

19
00:01:10,130 --> 00:01:12,100
those tables, so I might filter. So I have a 

20
00:01:12,100 --> 00:01:15,170
WHERE clause were I can say only bring back the rows

21
00:01:15,170 --> 00:01:19,960
where this condition is true. So we have a predicate statement as it's called

22
00:01:19,960 --> 00:01:24,159
that filters the rows that come back. Now a lot

23
00:01:24,159 --> 00:01:27,219
of statements are just simply SELECT FROM WHERE, that's a fairly

24
00:01:27,219 --> 00:01:28,219
common pattern.

25
00:01:28,219 --> 00:01:32,359
You might also want to aggregate and group data. So I might want to bring back

26
00:01:32,359 --> 00:01:37,100
not just the individual sales orders, but perhaps the totals for each month.

27
00:01:37,100 --> 00:01:41,200
So I might have a GROUP BY clause where I'm grouping the results by

28
00:01:41,200 --> 00:01:46,600
order date or order month. And then I might want to filter those groups. So I might

29
00:01:46,600 --> 00:01:47,759
want to say HAVING 

30
00:01:47,759 --> 00:01:53,039
a count of orders of over a hundred. So only bring back the months where we sold

31
00:01:53,039 --> 00:01:54,179
more than a hundred items.

32
00:01:54,179 --> 00:01:58,049
So there's an additional HAVING clause. And then the final cause that you usually see in a

33
00:01:58,049 --> 00:01:59,609
statement, is an ORDER BY clause.

34
00:01:59,609 --> 00:02:03,759
By default SQL does not return the results in any particular order.

35
00:02:03,759 --> 00:02:07,520
It just -- whatever is the easiest way for it to get the data, the fastest way, it

36
00:02:07,520 --> 00:02:08,310
will use that

37
00:02:08,310 --> 00:02:12,530
and return the data in whatever order it happens to be in. If you want an explicit

38
00:02:12,530 --> 00:02:13,710
order, you want the

39
00:02:13,710 --> 00:02:17,860
data to be sorted in a particular order, you specify the ORDER BY clause.

40
00:02:17,860 --> 00:02:21,510
And we'll look at that in a bit more detail later on, but that's how we do that. 

41
00:02:21,510 --> 00:02:25,910
So there's an example on the screen. Jeff, talk us through, what does that 

42
00:02:25,910 --> 00:02:26,870
example actually do?

43
00:02:26,870 --> 00:02:30,600
So we're selecting OrderDate

44
00:02:30,600 --> 00:02:34,600
and a count of OrderID. So we're not just displaying individual order IDs,

45
00:02:34,600 --> 00:02:36,400
we're going to count how many there are. So

46
00:02:36,400 --> 00:02:39,910
for each order date, we're going to count the order ids

47
00:02:39,910 --> 00:02:44,240
and because of that, if I jump forward a bit, we've got a GROUP BY OrderDate. So it's

48
00:02:44,240 --> 00:02:44,880
saying

49
00:02:44,880 --> 00:02:48,860
group up the order dates, and for each one of those, shows us a count of the order ID.

50
00:02:48,860 --> 00:02:51,940
So we see how many orders on that date.

51
00:02:51,940 --> 00:02:55,380
From the SalesOrder table, so you can see the schema that we

52
00:02:55,380 --> 00:02:56,390
mentioned before.

53
00:02:56,390 --> 00:03:00,430
So we've got the Sales.SalesOrder so it's the Sales schema, SalesOrder table.

54
00:03:00,430 --> 00:03:04,730
But we've filtered it, now we've filtered it in  two ways though. So, we've got a WHERE 

55
00:03:04,730 --> 00:03:08,030
clause, that saying before we've grouped it up,

56
00:03:08,030 --> 00:03:11,780
we're going to say where the status is shipped. And then we've also got a HAVING

57
00:03:11,780 --> 00:03:12,360
clause

58
00:03:12,360 --> 00:03:16,210
and that's after you've aggregated it -- that's working on the aggregate data.

59
00:03:16,210 --> 00:03:19,530
So where having already counted up the order IDs,

60
00:03:19,530 --> 00:03:22,880
I'm only interest if somebody's got an order, so I want order ID greater than one, 

61
00:03:22,880 --> 00:03:26,170
so I know there's more than one order on  that date so it will 

62
00:03:26,170 --> 00:03:30,370
clear out all the empty dates, like Saturdays and Sundays or whatever, and handle 

63
00:03:30,370 --> 00:03:31,930
the dates where there is only one in fact.

64
00:03:31,930 --> 00:03:35,580
Oh, greater than one! So if there is only one, it wouldn't display that.

65
00:03:35,580 --> 00:03:40,140
And finally we want to have it in order by order because

66
00:03:40,140 --> 00:03:44,370
it can be actually -- sometimes you can not put order by in and it

67
00:03:44,370 --> 00:03:45,930
comes out correctly because the

68
00:03:45,930 --> 00:03:49,160
that's the order of the rows in the table, but actually 

69
00:03:49,160 --> 00:03:52,280
it might work now. That's a happy coincidence.

70
00:03:52,280 --> 00:03:55,360
You might deliver this to someone and then suddenly it stops working down the line,

71
00:03:55,360 --> 00:03:58,650
so always put in an order by even if it appears to order by.

72
00:03:58,650 --> 00:04:02,730
And it's order by descending, so we don't want ascending. For whatever reason,  we want our dates 

73
00:04:02,730 --> 00:04:03,290
backwards.

74
00:04:03,290 --> 00:04:06,340
Yeah we want the most recent ones at the top. Yeah, okay

75
00:04:06,340 --> 00:04:09,340
so that's seems pretty straightforward then. I mean 

76
00:04:09,340 --> 00:04:13,459
I just specify and presumably the database engine then just

77
00:04:13,459 --> 00:04:16,880
executes each of those clauses in turn, is that right?  Well not

78
00:04:16,880 --> 00:04:20,320
strictly in turn, no, that's the

79
00:04:20,320 --> 00:04:24,950
slightly confusing thing about SQL in terms of it being a language.

80
00:04:24,950 --> 00:04:29,410
It is actually very English-based. You can read SQL,

81
00:04:29,410 --> 00:04:33,980
it kind of makes sense in this sort of odd way once you get used to that way of 

82
00:04:33,980 --> 00:04:34,750
thinking.

83
00:04:34,750 --> 00:04:38,450
But it doesn't work from top to bottom like other languages. So we're actually 

84
00:04:38,450 --> 00:04:42,160
going to start with the table. So the FROM 

85
00:04:42,160 --> 00:04:45,910
is the first thing it's going to do. Saying what tables do I need to get. And it's

86
00:04:45,910 --> 00:04:46,930
going out to the tables

87
00:04:46,930 --> 00:04:51,990
as the very first thing. And then we're going to go through filter those tables.

88
00:04:51,990 --> 00:04:55,800
So we're going to say okay which rows do we want from that table.

89
00:04:55,800 --> 00:04:58,900
So before we've even looked at what rows

90
00:04:58,900 --> 00:05:03,730
or rather columns, sorry, we're selecting, we've actually gone out and said ok

91
00:05:03,730 --> 00:05:06,110
what are the rows we are interested in going to get

92
00:05:06,110 --> 00:05:10,030
for that data. Then we're going to go through and

93
00:05:10,030 --> 00:05:13,900
aggregate, well not aggregate as such, we'll group up 

94
00:05:13,900 --> 00:05:17,430
those rows by in that case OrderDate. So we're going to 

95
00:05:17,430 --> 00:05:21,090
segment them down to those order date buckets

96
00:05:21,090 --> 00:05:24,260
essentially. And we want to filter that

97
00:05:24,260 --> 00:05:27,480
so we can say OK, then we want to look at only the ones that have got more than one

98
00:05:27,480 --> 00:05:28,680
order on that day.

99
00:05:28,680 --> 00:05:33,880
And then we can now get to the SELECT. So now finally we're actually looking at

100
00:05:33,880 --> 00:05:34,380
what

101
00:05:34,380 --> 00:05:37,450
we wanted it to display. It is relevant actually,

102
00:05:37,450 --> 00:05:40,970
as we've only got one left, then we'll sort it. But it is relevant 

103
00:05:40,970 --> 00:05:42,940
to think about it that way because

104
00:05:42,940 --> 00:05:46,680
if you think of creating a query in that way,

105
00:05:46,680 --> 00:05:50,660
logically that's how it's doing it. So if you think about tables first,

106
00:05:50,660 --> 00:05:54,000
and go through, rather than thinking about the 

107
00:05:54,000 --> 00:05:57,510
select column list first, actually think about what tables am I 

108
00:05:57,510 --> 00:05:58,120
interested.

109
00:05:58,120 --> 00:06:03,100
So once we start doing joins and things like that, it becomes more of a set up to be done

110
00:06:03,100 --> 00:06:04,010
on that table

111
00:06:04,010 --> 00:06:07,800
portion before you then move on to the selects later on. 

112
00:06:07,800 --> 00:06:12,490
So the principal is the query engine will first of all figure out the

113
00:06:12,490 --> 00:06:15,530
rows that it needs and then group them into whatever rows it needs, and it's grabbing them

114
00:06:15,530 --> 00:06:17,110
from the various source tables.

115
00:06:17,110 --> 00:06:20,920
Grouping them, filtering those rows based on the WHERE clause and then

116
00:06:20,920 --> 00:06:24,600
if they're grouped on the HAVING clause,  and only at that point does it then start to worry

117
00:06:24,600 --> 00:06:26,510
about which individual columns it's going to

118
00:06:26,510 --> 00:06:30,810
actually bring back. So yeah as you say, later on when we look at some examples,

119
00:06:30,810 --> 00:06:32,820
that becomes quite significant

120
00:06:32,820 --> 00:06:36,300
the fact that it does it in that order. So it's worth be aware of that right from the

121
00:06:36,300 --> 00:06:42,470
very beginning. So a couple of basic examples on the slide. Let's just look

122
00:06:42,470 --> 00:06:43,360
at some examples.

123
00:06:43,360 --> 00:06:46,370
Here's probably the first query everybody writes. This is the

124
00:06:46,370 --> 00:06:49,520
the Transact-SQL equivalent of the Hello World application. This is 

125
00:06:49,520 --> 00:06:53,349
the first thing you to SELECT * from and then the name of a table.

126
00:06:53,349 --> 00:06:56,789
Could we do SELECT Hello World? We could 

127
00:06:56,789 --> 00:06:59,430
actually literally SELECT Hello World.

128
00:06:59,430 --> 00:07:03,340
It has no table, it just literally brings back the string. But if you're querying the 

129
00:07:03,340 --> 00:07:03,740
table,

130
00:07:03,740 --> 00:07:08,180
which let's face it, is generally the point, we'd have SELECT * as being 

131
00:07:08,180 --> 00:07:11,849
an interesting starting point. And what that will do is bring back every column

132
00:07:11,849 --> 00:07:12,650
from that table.

133
00:07:12,650 --> 00:07:16,729
So, Jeff would you expect an application to use a lot of those types of queries?

134
00:07:16,729 --> 00:07:20,949
Well, it is one you run a lot because sometimes you just want to see

135
00:07:20,949 --> 00:07:22,560
what's in the table, it's quick.

136
00:07:22,560 --> 00:07:26,300
But the problem you've got is that someone might change the design of the

137
00:07:26,300 --> 00:07:26,879
table,

138
00:07:26,879 --> 00:07:31,340
and that could change. So if I'm presenting this is a finished record set

139
00:07:31,340 --> 00:07:32,110
to a developer

140
00:07:32,110 --> 00:07:35,509
of a web application say, then

141
00:07:35,509 --> 00:07:38,699
what they get out of my query might change.

142
00:07:38,699 --> 00:07:41,830
So I would be much happier to present them,

143
00:07:41,830 --> 00:07:46,400
even if it listed every single column, actually explicitly listing every single

144
00:07:46,400 --> 00:07:46,990
column,

145
00:07:46,990 --> 00:07:51,210
would mean I know what they're getting, what order they're getting them in. So it's about being 

146
00:07:51,210 --> 00:07:53,620
explicit and that makes it easier to keep track of any,

147
00:07:53,620 --> 00:07:57,229
if someone does change the design of the table, I at least can look at 

148
00:07:57,229 --> 00:07:57,699
the

149
00:07:57,699 --> 00:08:01,599
query and know, okay those are the columns I wanted. Yes. I guess the other point

150
00:08:01,599 --> 00:08:02,729
here as well as that

151
00:08:02,729 --> 00:08:06,319
applications are usually built across networks.

152
00:08:06,319 --> 00:08:09,659
So the client application might not be on the same server as the database.

153
00:08:09,659 --> 00:08:12,199
And if we're hosting it in Azure, it probably won't be.

154
00:08:12,199 --> 00:08:16,340
And if I do SELECT *, but actually I only need the product name and the price,

155
00:08:16,340 --> 00:08:20,520
then I might be passing an awful lot of data unnecessarily across those network

156
00:08:20,520 --> 00:08:21,270
connections. And

157
00:08:21,270 --> 00:08:24,389
for one query that might not matter but if I'm doing this

158
00:08:24,389 --> 00:08:27,229
a couple hundred thousand times, then that's going to add up. There's a lot of data 

159
00:08:27,229 --> 00:08:28,580
that's unnecessarily being

160
00:08:28,580 --> 00:08:33,180
transferred so I guess for that reason I'd also probably avoid SELECT *.

161
00:08:33,179 --> 00:08:36,430
So the alternative then is to

162
00:08:36,429 --> 00:08:40,430
specify the specific columns you want. So I might SELECT, well there,

163
00:08:40,429 --> 00:08:44,070
the exact example I used a minute ago, the Name and the ListPrice

164
00:08:44,070 --> 00:08:48,130
from the Production.Product table, bring that back. And then that way, I've just got the

165
00:08:48,130 --> 00:08:49,250
Name column and

166
00:08:49,250 --> 00:08:54,079
the ListPrice column. There's a couple other things I might do as well here.

167
00:08:54,079 --> 00:08:59,529
There's a few little sort of additional things you might do in the SELECT clause.

168
00:08:59,529 --> 00:09:02,949
You'll see I've got SELECT Name as Product

169
00:09:02,949 --> 00:09:06,240
and what I'm doing is saying bring back  the Name column, that's what it is called in the

170
00:09:06,240 --> 00:09:06,860
table,

171
00:09:06,860 --> 00:09:10,490
but in the result set that I'm going to  create from this SELECT statement

172
00:09:10,490 --> 00:09:11,050
and the

173
00:09:11,050 --> 00:09:14,500
virtual table that gets generated, I want to call that column Product.

174
00:09:14,500 --> 00:09:18,390
So you're changing the name if you like of that Product. And that's

175
00:09:18,390 --> 00:09:21,440
in that example we're just simply changing it because it

176
00:09:21,440 --> 00:09:24,959
may be confusing to column called Name because we might have a customer column 

177
00:09:24,959 --> 00:09:25,240
with a

178
00:09:25,240 --> 00:09:28,660
customer name or something like that. But the other reason that you might do this is

179
00:09:28,660 --> 00:09:29,199
because

180
00:09:29,199 --> 00:09:32,709
you don't have to just select the value that's in a column.

181
00:09:32,709 --> 00:09:35,569
If you look at the second thing we're selecting from there, we've got

182
00:09:35,569 --> 00:09:36,220
ListPrice

183
00:09:36,220 --> 00:09:40,380
multiplied by 0.9, a sale price. So we're applying a 10 percent discount.

184
00:09:40,380 --> 00:09:43,790
We've got an expression in there that's doing a calculation.

185
00:09:43,790 --> 00:09:48,270
And by default if didn't have that as SalePrice what we call an alias,

186
00:09:48,270 --> 00:09:52,240
what I would get is a column that doesn't have a name because it just gives me the result of

187
00:09:52,240 --> 00:09:54,589
the calculation. So

188
00:09:54,589 --> 00:09:58,890
you might use these aliases and we'll see some examples of these in the

189
00:09:58,890 --> 00:09:59,390
demo.

190
00:09:59,390 --> 00:10:03,180
But it's worth being aware that sometimes you can use this

191
00:10:03,180 --> 00:10:06,720
as cause. The as itself is optional, you can miss out the word as.

192
00:10:06,720 --> 00:10:10,920
There's a lot of things in Transact-SQL  where individual words are

193
00:10:10,920 --> 00:10:15,220
optional. For various reasons I'm going to recommend that you keep using

194
00:10:15,220 --> 00:10:16,699
as and we'll see why

195
00:10:16,699 --> 00:10:19,829
when we have a look at the demo. But

196
00:10:19,829 --> 00:10:23,350
just to be aware that you might have an alias that names the column in this

197
00:10:23,350 --> 00:10:24,459
virtual result set,

198
00:10:24,459 --> 00:10:28,649
virtual table that is produced. And again it is just worth being cognizant all of the

199
00:10:28,649 --> 00:10:29,209
time

200
00:10:29,209 --> 00:10:32,360
that whenever you have a SELECT statement, the result of that

201
00:10:32,360 --> 00:10:36,020
is a virtual table. It's not a table that is stored in the database but it's a

202
00:10:36,020 --> 00:10:37,730
row sets, it's a record set of

203
00:10:37,730 --> 00:10:38,430
data.

