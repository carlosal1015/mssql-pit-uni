0
00:00:02,320 --> 00:00:06,130
So data types and Transact-SQL. There's lots of different data types in a

1
00:00:06,130 --> 00:00:07,150
big table here.

2
00:00:07,150 --> 00:00:11,410
So we've got all these exact numerics, 

3
00:00:11,410 --> 00:00:14,639
Jeff what's an exact numeric? Well those

4
00:00:14,639 --> 00:00:19,359
ones we've got there are, they will appear actually

5
00:00:19,359 --> 00:00:22,630
for all intents and purposes to be much the same, but

6
00:00:22,630 --> 00:00:25,789
you actually get this floating point numbers and 

7
00:00:25,789 --> 00:00:29,199
in a computer it can have a level of accuracy.

8
00:00:29,199 --> 00:00:33,420
So pi for example there's an unknown number, and no one knows 

9
00:00:33,420 --> 00:00:37,570
actually what pi is, ultimately. But we can say well, it is roughly 3.14.

10
00:00:37,570 --> 00:00:42,180
So to the extent of where we actually are bothered to look at the decimal places it 

11
00:00:42,180 --> 00:00:43,450
probably will be the same. But

12
00:00:43,450 --> 00:00:47,450
if we need to be absolutely specific, we know 

13
00:00:47,450 --> 00:00:52,050
absolutely what that number equals down to x many decimal places, 

14
00:00:52,050 --> 00:00:53,950
then it's an exact numeric number.

15
00:00:53,950 --> 00:00:58,000
And actually some those are integers as well, so integers don't have decimal places.

16
00:00:58,000 --> 00:00:58,350
 

17
00:00:58,350 --> 00:01:01,570
They're obviously the same. But the floats and reals

18
00:01:01,570 --> 00:01:06,030
they're like pi, once you're getting out there, then they're not..

19
00:01:06,030 --> 00:01:09,880
after so many decimal places, it could be inaccurate to a

20
00:01:09,880 --> 00:01:10,920
certain extent.

21
00:01:10,920 --> 00:01:15,030
And I guess there are cases where we're prepared to accept that. Well and

22
00:01:15,030 --> 00:01:17,189
we have to accept that.

23
00:01:17,189 --> 00:01:20,929
I noticed this tinyint, smallint, 

24
00:01:20,929 --> 00:01:24,569
int and bigint. Seems to be a lot of ints going on there,

25
00:01:24,569 --> 00:01:28,259
what's that about? Why do I need so many different types of int?

26
00:01:28,259 --> 00:01:31,780
Essentially, it's how much storage space you're allocating

27
00:01:31,780 --> 00:01:34,850
to that

28
00:01:34,850 --> 00:01:40,100
that number. Because everything actually on a computer is binary

29
00:01:40,100 --> 00:01:41,490
ultimately, then

30
00:01:41,490 --> 00:01:45,069
how many bits we assign to that

31
00:01:45,069 --> 00:01:48,749
number is going to define what we can put in that number. So actually we start out

32
00:01:48,749 --> 00:01:49,319
with a

33
00:01:49,319 --> 00:01:52,979
binary number, and that would just be a 0 or 1, and then we can go up

34
00:01:52,979 --> 00:01:58,350
say okay, if we had a whole byte of that, then we can 0 to 255 

35
00:01:58,350 --> 00:02:02,709
and that would be great. It's really small, it's really efficient, except if you suddenly find you've actually got

36
00:02:02,709 --> 00:02:05,280
257 that you need to put in there, it won't work.

37
00:02:05,280 --> 00:02:09,259
So then we go up through the stages, so then

38
00:02:09,258 --> 00:02:12,920
as you move up, you start to use multiple bytes.

39
00:02:12,920 --> 00:02:16,450
And actually it starts to use not just 0 to 

40
00:02:16,450 --> 00:02:21,480
it will let you go up to 65535. Oddly I remember that number. 

41
00:02:21,480 --> 00:02:25,560
But it doesn't use all of that as a positive because it says oh, you may want some 

42
00:02:25,560 --> 00:02:26,730
negative numbers as well,

43
00:02:26,730 --> 00:02:30,989
so you get plus or minus half the numbers. And then as you go up

44
00:02:30,989 --> 00:02:32,799
the numbers become bigger and bigger and bigger.

45
00:02:32,799 --> 00:02:36,569
Okay, so it's a factor of how much space is going to be used. Keep them as small as you can 

46
00:02:36,569 --> 00:02:38,049
but obviously if you're 

47
00:02:38,049 --> 00:02:41,959
pushing the limit, if you think well I'm probably going to have a maximum of 255,

48
00:02:41,959 --> 00:02:45,000
probably you would use the next one after.

49
00:02:45,000 --> 00:02:48,730
Right, you would overestimate yeah. And I guess the important 

50
00:02:48,730 --> 00:02:52,329
point, and this is all important stuff if you're designing and building tables

51
00:02:52,329 --> 00:02:56,079
choosing the right data types, from the point of view of querying though, 

52
00:02:56,079 --> 00:03:00,280
it can be important to know what data type you've got. At the very least

53
00:03:00,280 --> 00:03:01,200
it is important know

54
00:03:01,200 --> 00:03:04,480
is it numeric or is it character or is it date or is it 

55
00:03:04,480 --> 00:03:08,269
you know, what is it? Because that determines how you can combine these

56
00:03:08,269 --> 00:03:09,329
things in expressions.

57
00:03:09,329 --> 00:03:13,690
We saw previously how I could concatenate strings using plus

58
00:03:13,690 --> 00:03:18,380
or I could add numbers using plus, but I can't add a number to a string and I can't 

59
00:03:18,380 --> 00:03:20,000
concatenate a number to a string

60
00:03:20,000 --> 00:03:24,940
using a plus because SQL Server doesn't know how that behavior works.

61
00:03:24,940 --> 00:03:29,799
Even though you had a 58 and a load of those sizes, yeah, even though some my sizes were 58 because 

62
00:03:29,799 --> 00:03:33,380
in the table it's defined as being string. It's actually one of these character types,

63
00:03:33,380 --> 00:03:38,680
I believe it's actually varchar which a variable length character.

64
00:03:38,680 --> 00:03:39,150
 

65
00:03:39,150 --> 00:03:42,720
You don't set aside a specific amount of space, you set aside the maximum amount of space and 

66
00:03:42,720 --> 00:03:44,780
fill it with whatever you need.

67
00:03:44,780 --> 00:03:47,640
But it couldn't add 

68
00:03:47,640 --> 00:03:51,280
the number 28 as a character to another number because

69
00:03:51,280 --> 00:03:55,780
even although it's 2 8, it's not actually a number. The other important ones to be aware

70
00:03:55,780 --> 00:03:56,110
of

71
00:03:56,110 --> 00:04:00,060
you'll occasionally see character data with n in front of it and that generally

72
00:04:00,060 --> 00:04:01,490
means, well it doesn't generally mean, 

73
00:04:01,490 --> 00:04:05,459
it means that we've set aside two bytes for each character.

74
00:04:05,459 --> 00:04:09,130
And the reason we might do that is if you've got

75
00:04:09,130 --> 00:04:13,950
characters that perhaps aren't in  standard written English, perhaps they're Kanji 

76
00:04:13,950 --> 00:04:16,130
or Arabic characters that are 

77
00:04:16,130 --> 00:04:19,950
not stored in the standard sort of ASCII table

78
00:04:19,950 --> 00:04:23,990
of text. So we set aside two bytes to store those types of characters

79
00:04:23,990 --> 00:04:27,550
and that quite often makes up a lot of data that's in databases. So

80
00:04:27,550 --> 00:04:31,410
it's Unicode. It's Unicode, yeah, and that's the terminology that's used.

81
00:04:31,410 --> 00:04:35,890
So what we do is we prefix an n to the datatype to indicate that

82
00:04:35,890 --> 00:04:36,390
it's 

83
00:04:36,390 --> 00:04:39,870
a wide character as we call it, it's a Unicode character.

84
00:04:39,870 --> 00:04:44,040
Some interesting dates and times, or date and time

85
00:04:44,040 --> 00:04:48,030
data types that you can have.There's datetime and datetime2

86
00:04:48,030 --> 00:04:51,919
which have some slight differences. Datetime2 was obviously added later

87
00:04:51,919 --> 00:04:53,780
on to a different level of accuracy.

88
00:04:53,780 --> 00:04:58,030
And we've got individual date, individual time, all sorts of things

89
00:04:58,030 --> 00:04:59,130
there that we can store

90
00:04:59,130 --> 00:05:03,300
dates and times with. We can store binary data, you could store

91
00:05:03,300 --> 00:05:08,260
straightforward binary from an audio file or something like that.

92
00:05:08,260 --> 00:05:11,990
There's a data type called image which is effectively binary, it's for storing

93
00:05:11,990 --> 00:05:15,750
image type things. And there's various other

94
00:05:15,750 --> 00:05:19,360
more esoteric data types like XML, where you can store an XML

95
00:05:19,360 --> 00:05:24,010
document or string. Or geography and geometry where we can store

96
00:05:24,010 --> 00:05:27,680
location-based data and shapes that are defined by points on a

97
00:05:27,680 --> 00:05:31,150
plane or on the global. All sorts of different data types.

98
00:05:31,150 --> 00:05:35,860
The important thing is understanding the compatibility between those types from

99
00:05:35,860 --> 00:05:36,530
the point of view

100
00:05:36,530 --> 00:05:39,780
of using Transact-SQL. 

101
00:05:39,780 --> 00:05:42,960
And that really takes us on to talk about data type conversion.

102
00:05:42,960 --> 00:05:47,460
How do I convert between these two different data types? The obvious one is

103
00:05:47,460 --> 00:05:49,910
implicit conversion. And we saw earlier on

104
00:05:49,910 --> 00:05:56,030
I'm able to concatenate two strings. One might be a varchar one might be a

105
00:05:56,030 --> 00:05:59,030
char, or one might be a varchar and one might be a nvarchar,

106
00:05:59,030 --> 00:06:02,690
but I can still concatenate them because those are compatible. They're both

107
00:06:02,690 --> 00:06:07,250
fundamentally strings. And the same with numbers. I could have an integer and a

108
00:06:07,250 --> 00:06:08,700
real number that has

109
00:06:08,700 --> 00:06:12,440
you know decimal place, and if I try and add them together SQL Server knows 

110
00:06:12,440 --> 00:06:13,740
that they're numbers, it knows how to add

111
00:06:13,740 --> 00:06:17,440
numbers, so it's able to do that. So there's implicit conversion

112
00:06:17,440 --> 00:06:19,460
between those very specific

113
00:06:19,460 --> 00:06:25,050
subtypes if you like. But then you've got issues where perhaps you need to

114
00:06:25,050 --> 00:06:29,460
convert a number to a string in order to add it together and at that point

115
00:06:29,460 --> 00:06:31,150
it's not going to work implicitly.

116
00:06:31,150 --> 00:06:36,750
So we have to use explicit conversion. And there is a number ways we can do this,

117
00:06:36,750 --> 00:06:37,220
there's

118
00:06:37,220 --> 00:06:40,570
a number of functions in SQL Server that you can use to convert

119
00:06:40,570 --> 00:06:44,170
data from one type to another as long as you know

120
00:06:44,170 --> 00:06:47,920
the value that's in there is actually compatible. So

121
00:06:47,920 --> 00:06:52,930
there's CAST and CONVERT which as we'll see when we will have a look at

122
00:06:52,930 --> 00:06:53,880
the demo are

123
00:06:53,880 --> 00:06:57,470
pretty much the same thing to be honest. They have the same functionality, they 

124
00:06:57,470 --> 00:06:58,530
effectively let us change

125
00:06:58,530 --> 00:07:02,450
one data type explicitly to another data type. 

126
00:07:02,450 --> 00:07:07,180
CONVERT I tend to favor using CONVERT especially when I'm working with dates

127
00:07:07,180 --> 00:07:09,580
because it has a number of options for formatting

128
00:07:09,580 --> 00:07:12,640
dates that I'm converting between strings.

129
00:07:12,640 --> 00:07:15,640
And incidently dates is an interesting one. Date

130
00:07:15,640 --> 00:07:18,800
you can have an implicit conversion between the date and

131
00:07:18,800 --> 00:07:23,570
a string. Jeff can you think of any reasons why I might not want to rely on implicit

132
00:07:23,570 --> 00:07:24,240
conversion between

133
00:07:24,240 --> 00:07:27,650
dates and string? Well, probably the biggest one is the order of dates.

134
00:07:27,650 --> 00:07:31,810
If I put in 05 05 2015,

135
00:07:31,810 --> 00:07:37,040
actually, no that would be the same way, &lt;laughter&gt;.  If I put in 05 06 2015 do I mean

136
00:07:37,040 --> 00:07:37,650
the 5th

137
00:07:37,650 --> 00:07:42,050
June or do I mean the 6th of May and that will depend on where I am in the world.

138
00:07:42,050 --> 00:07:45,500
Yeah. And actually it would work

139
00:07:45,500 --> 00:07:48,680
because it's going to work in either system but

140
00:07:48,680 --> 00:07:52,080
is it correct? You might store the wrong thing. 

141
00:07:52,080 --> 00:07:56,210
So exactly. Date order it varies by

142
00:07:56,210 --> 00:07:59,740
locale. There are a number of standards for writing down dates that

143
00:07:59,740 --> 00:08:00,950
are used in

144
00:08:00,950 --> 00:08:04,470
multinational systems and we'll talk a little bit about those in a while.

145
00:08:04,470 --> 00:08:08,199
But if I just have somebody enter a date as a string and then

146
00:08:08,199 --> 00:08:11,220
rely on that implicitly being converted to a date,

147
00:08:11,220 --> 00:08:14,389
I might get the month and day 

148
00:08:14,389 --> 00:08:17,520
order wrong or there are various other things in there as well that might

149
00:08:17,520 --> 00:08:18,139
trip me up.

150
00:08:18,139 --> 00:08:21,720
The time factor might be different as well. So

151
00:08:21,720 --> 00:08:26,639
you usually don't rely on implicit conversion of dates. You can to a certain

152
00:08:26,639 --> 00:08:29,710
extent, but you usually don't, you usually use explicit conversion.

153
00:08:29,710 --> 00:08:33,380
And the CONVERT function itself has a number of options for saying I want to 

154
00:08:33,380 --> 00:08:34,029
convert,

155
00:08:34,029 --> 00:08:38,180
especially if you're going from date to string, I want to convert this date to a string

156
00:08:38,179 --> 00:08:43,169
in this specific format. So perhaps the ISO format or the 

157
00:08:43,169 --> 00:08:44,350
ANSI format for dates.

158
00:08:44,350 --> 00:08:48,110
There's also PARSE which is used to

159
00:08:48,110 --> 00:08:51,990
to parse something that you believe to be a number in order to convert it into

160
00:08:51,990 --> 00:08:52,510
a number.

161
00:08:52,510 --> 00:08:55,630
And there's STR which is kind of the opposite. It's to

162
00:08:55,630 --> 00:08:58,970
take a a number and make into a string

163
00:08:58,970 --> 00:09:02,050
effectively. You'll notice that with CAST, TRY, sorry

164
00:09:02,050 --> 00:09:06,680
CAST, CONVERT, and PARSE, that there are TRY variants of those things.

165
00:09:06,680 --> 00:09:11,050
What we're saying there is if I try to CAST the string

166
00:09:11,050 --> 00:09:14,980
o n e, the word you know, one,

167
00:09:14,980 --> 00:09:18,360
and try and CAST that as a number, of course it's going to fail because

168
00:09:18,360 --> 00:09:23,040
written out o n e isn't actually a number. And if I just use CAST, I'm going to get an error at

169
00:09:23,040 --> 00:09:25,520
that point. If I use TRY_CAST

170
00:09:25,520 --> 00:09:28,640
it will try to convert it and if it fails it will return null.

171
00:09:28,640 --> 00:09:32,480
So it doesn't cause the statement to fail, it just doesn't actually

172
00:09:32,480 --> 00:09:35,390
convert it, it just returns I don't know what that is, it's a null.

173
00:09:35,390 --> 00:09:40,040
So all those sizes you had that were 58s, those could be converted over and even

174
00:09:40,040 --> 00:09:40,450
though there's

175
00:09:40,450 --> 00:09:44,620
some larges and smalls in there, they'll just get rid of those -- yeah I can try to cast them, it will convert

176
00:09:44,620 --> 00:09:45,950
the ones that it can and

177
00:09:45,950 --> 00:09:47,279
ignore the ones it can't effectively.

