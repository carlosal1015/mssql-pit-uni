-- https://docs.microsoft.com/en-us/sql/t-sql/language-reference?view=sql-server-2017
/*
Transact-SQL is the language used to query data in Microsoft SQL Server and Azure SQL Database.
Data is stored in tables, which may be related to one another through common key fields.
Objects in a database are organized into schemas.
The fully qualified naming syntax for an object is server_name.database_name.schema_name.object_name, but in most cases you can abbreviate this to schema_name.object_name.
*/

USE TSQLV4;
SELECT country
FROM HR.Employees;