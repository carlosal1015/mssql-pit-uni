DROP DATABASE TestDB
CREATE DATABASE TestDB ON PRIMARY (
    NAME='UNI_DAT',
    FILENAME='D:\SQL\UNI_DAT.mdf',
    SIZE=10MB,
    MAXSIZE=20MB,
    FILEGROWTH=20%
)
SELECT Name
from sys.Databases

GO
--https://en.wikipedia.org/wiki/Hungarian_notation
