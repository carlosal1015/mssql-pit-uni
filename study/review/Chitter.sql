USE my_data_base;

CREATE TABLE chitter_user (
    user_id,
    username,
    encrypted_password,
    email,
    date_joined
);
GO

CREATE TABLE post_id(
    post_id,
    user_id,
    post_text,
    posted_on
) -- PRIMARY

CREATE TABLE follower()

-- ALTER TABLE chitter_user
-- DROP COLUMN t;

DROP TABLE chitter_user;

SELECT COUNT(*) FROM chitter_user;

INSERT INTO chitter_user
    (user_id, username, encrypted_password, email, date_joined)
VALUES
    (DEFAULT 10 FOR t, 'firstuser', 'd63dc919e2dc30d2',
    'fakemail@fakedomain.fake, '2019-02-25');

SELECT * FROM Customers;