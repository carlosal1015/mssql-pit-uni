/*
Element         Expression          Role
--------------------------------------------------------------------
SELECT          <select list>       Defines which columns to return.
FROM            <table source>      Defines table(s) to query.
WHERE           <search condition>  Filters rows using a predicate.
GROUP BY        <group by list>     Arranges rows by groups.
HAVING          <search condition>  Filters groups using a predicate.
ORDER BY        <order by list>     Sorts the output.
*/

-- Example:

SELECT OrderDate, COUNT(OrderID)
FROM Sales.SalesOrder
WHERE Status = 'Shipped'
GROUP BY OrderDate
HAVING COUNT(OrderID) > 1
ORDER BY OrderDate DESC;

-- Example:

FROM Sales.SalesOrder