

## Getting started SQL Server on Linux

In order to see the hostname and the logged user write:

```console
~/Git_Projects/SQL/mssql-pit-uni(master*) » hostname; whoami
Oromion
carlosal1015
```

Consider the following program:

```sql
SELECT HOST_NAME()

SELECT @@VERSION
```

The first will be output with the instruction `SELECT HOST_NAME()`.

**Results:**

|(No column name)|
| ---            |
|Oromion|

**Messages:**

    [1:51:54 PM]	Started executing query at Line 1
    (1 row affected) 
    Total execution time: 00:00:00.004

**Results:**

|(No column name)|
| ---            |
|Microsoft SQL Server 2019 (RTM-CU4) (KB4548597) - 15.0.4033.1 (X64) 	Mar 14 2020 16:10:35 	Copyright (C) 2019 Microsoft Corporation	Express Edition (64-bit) on Linux (Arch Linux) <X64>|

**Messages:**

    [2:04:40 PM]	Started executing query at Line 3
    (1 row affected) 
    Total execution time: 00:00:00.015
Before
```console
~/Git_Projects/SQL/mssql-pit-uni(master*) » tail /etc/group
rpc:x:32:
ceph:x:340:
libvirt:x:968:carlosal1015
dnsmasq:x:967:
mongodb:x:966:carlosal1015
docker:x:965:carlosal1015
brlapi:x:963:
dhcpcd:x:962:
named:x:40:
mssql:x:961:
```

```console
~/Git_Projects/SQL/mssql-pit-uni(master*) » sudo usermod -aG mssql $USER
```

Then, relogged to do effect.

```console
~/Git_Projects/SQL/mssql-pit-uni(master*) » groups
mssql docker mongodb libvirt users wheel
```

```console
~/Git_Projects/SQL/mssql-pit-uni(master*) » ls -l /var/opt/mssql
total 16
drwxr-xr-x 2 mssql mssql 4096 May 10 12:21 data
drwxr-xr-x 2 mssql mssql 4096 May 10 14:15 log
-rw-rw-r-- 1 mssql mssql   23 May 10 12:21 mssql.conf
drwxr-xr-x 2 mssql mssql 4096 May 10 12:21 secrets
```

After
```console
~/Git_Projects/SQL/mssql-pit-uni(master*) » tail /etc/group
rpc:x:32:
ceph:x:340:
libvirt:x:968:carlosal1015
dnsmasq:x:967:
mongodb:x:966:carlosal1015
docker:x:965:carlosal1015
brlapi:x:963:
dhcpcd:x:962:
named:x:40:
mssql:x:961:carlosal1015
```

> Learn more how Microsoft used the ideas of the [Drawbridge project](https://www.microsoft.com/en-us/research/project/drawbridge) for port SQL server without rewrite in [Visual C++](https://docs.microsoft.com/en-us/cpp/?view=vs-2019) from Windows to Linux on [Introduction to SQL Server on Linux](https://docs.microsoft.com/en-us/learn/modules/introduction-sql-server-linux).

For a quick brief https://www.w3schools.com/sql/default.asp

https://techcrunch.com/2017/07/17/how-microsoft-brought-sql-server-to-linux
https://docs.microsoft.com/en-us/sql/sql-server/?view=sql-server-ver15