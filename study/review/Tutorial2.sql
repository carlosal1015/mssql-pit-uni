/*
CREATE DATABASE social_network;
GO

CREATE TABLE users (
    user_id int,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    email VARCHAR(255)
);
GO

ALTER TABLE users ADD encrypted_password VARCHAR(1000);
GO

ALTER TABLE users DROP COLUMN email;
GO

DROP TABLE users;
GO

DROP DATABASE social_network;
GO
*/

CREATE DATABASE my_data_base;
GO

CREATE TABLE users (
    user_id int,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    email VARCHAR(255),
    encrypted_password VARCHAR(1000)
);
GO

CREATE TABLE movies (
    movie_id int,
    title VARCHAR(100),
    description VARCHAR(100),
    price VARCHAR(255)
);
GO

CREATE TABLE purchases (
    user_id int,
    movie_id int,
    purchase_date VARCHAR(250),
    purchase_price VARCHAR(100)
);
GO

INSERT INTO movies (movie_id, title, description, price)
VALUES (1, 'Gattaca', 'Movie or documentary?', 4.99);
GO

SELECT * FROM movies;
GO

SELECT title FROM movies;
GO

INSERT INTO movies (title, price) VALUES ('Star Wars', 8.99);
INSERT INTO movies (title, price) VALUES ('Logans Run', 3.99);
INSERT INTO movies (title, price) VALUES ('Solaris', 2.99);
INSERT INTO movies (title, price) VALUES ('Jaws', 5.25);
INSERT INTO movies (title, price) VALUES ('Silent Running', 3.00);
GO

SELECT title, price FROM movies;
GO

SELECT title, price FROM movies ORDER BY price;
GO

SELECT title, price FROM movies ORDER BY price DESC;
GO

UPDATE movies SET price = 0.99 WHERE title = 'Jaws';
GO

SELECT title, price FROM movies ORDER BY price;
GO

DELETE FROM movies WHERE title = 'Star Wars';
GO

SELECT title, price FROM movies ORDER BY price;
GO

/*
SELECT * FROM table_name WHERE data > 0;
GO
*/