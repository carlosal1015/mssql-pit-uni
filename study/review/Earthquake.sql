/*
* Tutorial from https://www.youtube.com/watch?v=YufocuHbYZo
* Dataset from https://github.com/socratica/data
*/

USE my_data_base;
SELECT *
FROM dbo.earthquake;
GO

SELECT COUNT(*)
FROM dbo.earthquake;
GO

SELECT magnitude, place, occurred_on
FROM dbo.earthquake;
GO

SELECT place, magnitude, occurred_on
FROM dbo.earthquake;
GO

SELECT *
FROM dbo.earthquake
WHERE occurred_on >= '2000-01-01';
GO

SELECT *
FROM dbo.earthquake
WHERE occurred_on >= '2010-01-01' AND occurred_on <= '2010-12-31';
GO

SELECT *
FROM dbo.earthquake
WHERE occurred_on >= '2010-01-01' AND occurred_on <= '2010-12-31'
ORDER BY magnitude;
GO

SELECT *
FROM dbo.earthquake
WHERE occurred_on >= '2010-01-01' AND occurred_on <= '2010-12-31'
ORDER BY magnitude DESC;
GO

-- LIMIT 1 non exists on SQL Server
SELECT TOP 1 *
FROM dbo.earthquake
WHERE occurred_on >= '2010-01-01' AND occurred_on <= '2010-12-31'
ORDER BY magnitude DESC;
GO

