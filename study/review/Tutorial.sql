-- CREATE DATABASE MyAdventureWorks   
--     ON (FILENAME = 'C:\MySQLServer\AdventureWorks_Data.mdf'),   
--     (FILENAME = 'C:\MySQLServer\AdventureWorks_Log.ldf')   
--     FOR ATTACH;
PRINT 'Hola mundo'
USE master;
GO
IF DB_ID (N'mytest') IS NOT NULL
DROP DATABASE mytest;
GO
CREATE DATABASE mytest;
GO
-- Verify the database files and sizes
SELECT name, size, size*1.0/128 AS [Size in MBs]
FROM sys.master_files
WHERE name = N'mytest';
GO