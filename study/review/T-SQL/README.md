SQL Server runs on Windows, Linux and Docker containers.

## T-SQL

Is a dialect of SQL.

```plantuml
(T-SQL) <|-- (SQL) <|-- (Relational Model) <|-- (Mathematics -- Logic)
```

A [common table expression](https://en.wikipedia.org/wiki/Hierarchical_and_recursive_queries_in_SQL)
is a temporary named result set, derived from a simple query and
defined within the execution scope of a `SELECT`, `INSERT`, `UPDATE`,
or `DELETE` statement.

SQL is based on the relational model, which is a mathematical model for data
management and manipulation.

https://chartio.com/resources/tutorials/using-common-table-expressions

The main differences between SQL and T-SQL are:
* The use of semicolon (`;`) is optional in T-SQL.
* T-SQL uses the `GO` and `USE` statement.

The query processing process

1. Parsing and binding
2. Query optimization
  a. Generation of possible execution plans
  b. Cost assessment of each plan
3. Query execution and plan caching