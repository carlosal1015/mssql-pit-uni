References

https://docs.microsoft.com/en-us/sql/t-sql/language-reference?redirectedfrom=MSDN&view=sql-server-ver15

https://www.microsoftpressstore.com/store/microsoft-sql-server-2012-t-sql-fundamentals-9780735658141

<details>
  <summary markdown="span">This is the summary text, click me to expand</summary>

  This is the detailed text.

  We can still use markdown, but we need to take the additional step of using the `parse_block_html` option as described in the [Mix HTML + Markdown Markup section](#mix-html--markdown-markup).

  You can learn more about expected usage of this approach in the [GitLab UI docs](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/base-collapse--default) though the solution we use above is specific to usage in markdown.
</details>

<details>
<summary markdown="span">First level collapsible item</summary>
**Lorem ipsum dolor sit amet...**
<details>
<summary markdown="span">Second level collapsible item</summary>
*Sed ut perspiciatis unde omnis iste natus...*
</details>
</details>

```math
a^2+b^2=c^2
```