SELECT HOST_NAME()

-- Creación de Base de datos
-- [SERVIDOR].[BASEDATOS].[ESQUEMA]

-- 1) Script para creación de Base datos

-- Facebook es una base de datos relacionales y no relacionales y carga consultan prediseñadas.
-- Los eventos se lanzan al servidor y se disponen en un Layout en una página web

-- 1.1) Eliminamos todas las bases de datos del Sistema
-- Asegurarnos que la base de datos no exista
CREATE DATABASE UNI
CREATE DATABASE CTIC
CREATE DATABASE FIIS

-- mientras me devuelva un valor, seguirá funcionando, se ejecuta 3 veces porque hay 3 bases de datos
WHILE EXISTS( select name from sysdatabases
              where name not in ( 'master',
                                  'tempdb',
                                  'model',
                                  'msdb',
                                  'ReportServer',
                                  'ReportServerTempDB')
                                  )

-- Inicializa un lote
-- Elimina la base de datos diferentes de las principales
BEGIN
DECLARE @strSQL as varchar(100)
SET @strSQL = ( SELECT TOP 1 ' DROP DATABASE' + name from sysdatabases
                where name not in ( 'master',
                                    'tempdb',
                                    'model',
                                    'msdb',
                                    'ReportServer',
                                    'ReportServerTempDB'))

EXECUTE (@strSQL)
END
GO
-- DROP DATABASE siginifica elimina la base de datos

-- 1.2)

IF exists ( select name from sysdatabases
            where name='UNI')
DROP DATABASE UNI
GO

CREATE DATABASE UNI
ON PRIMARY -- TABLA FÍSICA
(NAME='UNI_DAT',
FILENAME='/home/carlosal1015/Git_Projects/SQL/UNI.mdf',
SIZE=30MB,
MAXSIZE=50MB,
FILEGROWTH=20%)
LOG ON
(NAME='UNI_LOG',
FILENAME='/home/carlosal1015/Git_Projects/SQL/UNI.ldf',
SIZE=10MB,
MAXSIZE=UNLIMTED, -- este lote de transacciones puede crecer de manera ilimitada
FILEGROWTH=10%)-- si supera su tamaño, solo puede incrementarse en ese %
GO
-- se le puede cambiar el tamaño de la base de datos
-- pero no es recomendable, hay una pérdida de performance
-- de lectura y escritura, para que funciona durante los próximos 10, 20 años

-- por estimación es la tercera parte del espacio físico.
-- Aprender qué hace MDF LDF

-- Tod o pasa por el log y lo valida
-- y luego pasa el mdf, cualquier modificación o inserción y va al log y luego se valida al mdf
-- por eso necesitamos dos archivos
-- Log es el registro temporal donde se valida

-- Hay una estrategia para estimar el tamaño de una base de datos.

-- http://ctic.somee.com/clase1.html

-- 2) Creación de Tablas
USE UNI
GO
CREATE TABLE Empleado(
    cod_emp_in integer identity(1, 1) not null,
    nom_emp_vc varchar(30) NULL,
    ape_pat_emp_vc varchar(30) null,
    ape_mat_emp_vc varchar(30) null,
    dni_emp_ch char(8) not null,
    obs_emp_vc NULL,
    sex_emp_vc bit NOT NULL)
    
    -- entero pesa 4 bytes + char 30 bytes +  30 bytes + 8 bytes
    -- tenemos 102 bytes por registros
    -- puede pesar unos megas
    -- el tamaño de la base y almacenamiento va a aumentar

-- INSERTANDO PRIMER REGISTRO
INSERT INTO Empleado
(nom_emp_vc, ape_pat_emp_vc, ape_mat_emp_vc, dni_emp_ch)
values ('JAVIER', 'LOAYZA', 'JIMENEZ', '0404523')

    -- hay dnis que empiezan con un 012, dni, ruc y de tamaño fijo debe ser de tipo char
    -- cada vez que inicie un registro empiece en 1 e incrementa en 1
    -- pepe 1, raul 2, etc.

-- siempre es conveniente acotar con null
-- identity es una caracterisitica que le brinda la autogeneración a las tablas
-- identity es una propiedad, va a ser un correlativo hasta la que se defina.

-- tenemos una tabla y un único registro
-- el autoincrement

-- guids
-- identificador unico global

-- si hay repetición de tablas, los guids no se repite en ninguna
-- tabla ni en un ninguna base de datos

SELECT*FROM Empleado

-- autoincrement(...) // ver el vídeo
-- estudiar las propiedades del atributo
-- la nomenclatura hungara es para darle el alias al atributo

-- es asegurar la consistencia de inserción de datos

-- el 15 es el parcial y el 22 es el final