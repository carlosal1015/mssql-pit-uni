SELECT HOST_NAME()

use FourLeaves

insert into Test
select Id+2, SomeData from [Test]

select * from [Test]

~ » sqlcmd -S localhost -U sa                                                                                                                            carlosal1015@Oromion
Password: 
1> create database FourLeaves
2> go
1> use FourLeaves
2> go
Changed database context to 'FourLeaves'.
1> create table Test ( Id INT NOT NULL, SomeData NVARCHAR(MAX), PRIMARY KEY(Id))
2> GO
1> select * from Test
2> go
Id          SomeData                                                                                                                                                                                                                                                        
----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

(0 rows affected)
1> insert into test (Id, SomeData) VALUES(1, 'This is a test')
2> INSERT INTO TEST (Id, SomeData) VALUES(2, 'This is another test')
3> go

(1 rows affected)

(1 rows affected)
1> select * from Test
2> go
Id          SomeData                                                                                                                                                                                                                                                        
----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
          1 This is a test                                                                                                                                                                                                                                                  
          2 This is another test                                                                                                                                                                                                                                            

(2 rows affected)
1> 

Sqlcmd: Warning: The last operation was terminated because the user pressed CTRL+C.
-- https://docs.microsoft.com/en-us/sql/visual-studio-code/sql-server-develop-use-vscode?view=sql-server-ver15
-- https://docs.microsoft.com/en-us/sql/sql-server/editions-and-components-of-sql-server-2017?view=sql-server-ver15
-- https://docs.microsoft.com/en-us/sql/visual-studio-code/sql-server-develop-use-vscode?view=sql-server-ver15