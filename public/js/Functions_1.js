﻿///Calls Method on the same page (public static, marked with [WebMethod] attribute);
function AjaxServiceSamePage(MethodName, DataStructure, successFn, errorFn, alwaysShowError) {
    alwaysShowError = alwaysShowError || false;

    var sPath = window.location.pathname;
    var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

    var jData = JSON.stringify(DataStructure);

    $.ajax({
        type: "POST",
        url: sPage + "/" + MethodName,
        data: jData,
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    })
        .done(function (msg) {
            if (typeof successFn == 'function') {
                userInputEnable();
                successFn(msg);
            }
        })
        .fail(function (xhr, ajaxOptions, error) {
            userInputEnable();
            if (typeof errorFn == 'function')
                errorFn(xhr, ajaxOptions, error, this.data)

        })
        .fail(((typeof errorFn != 'function' || alwaysShowError)
            ? function (xhr, ajaxOptions, error) {
                var showAlert = true;
                errorMsg = "Unknown error during AJAX request!";
                errorMatch = xhr.responseText.match("<h2>.*</h2>") ///Global HTTP errors
                if (errorMatch != null && errorMatch.length > 0)
                    errorMsg = errorMatch[0].replace("<br>", "\r\n").replace("<h2>", "").replace("<i>", "").replace("</h2>", "").replace("</i>", "").trim();
                else if (xhr.responseText.indexOf("following validation erros have occured") >= 0) {
                    showAlert = !DoValidationExceptionHandler(JSON.parse(xhr.responseText).Message);
                }
                else {
                    error = JSON.parse(xhr.responseText); ///ASP.Net errors
                    if (error.Message != null)
                        errorMsg = error.Message + "\r\n\r\n" + error.StackTrace;
                }
                if (showAlert)
                    alert(unescape(errorMsg));
            }
            : function () { }))
        ;
}

function LoadFromServiceSamePage(MethodName, DataStructure, LoadingElement, successFn) {

    AjaxServiceSamePage(MethodName
        , DataStructure
        , function (msg) {
            LoadingElement.html(msg.d);
            if (typeof successFn == 'function') {
                successFn(msg);
            }

        });

}

//// DOValidationException parser
function DoValidationExceptionHandler(Text) {
    var lines = Text.split('\r\n');

    for (var i = 0; i < lines.length; i++ )
    {
        var line = lines[i];

        var matches = line.match("Property '(.+)', error: (.+)");

        if (matches) {
            var property = matches[1];
            var error = matches[2];

            var el = $(".DOValidationError.Property_" + property);

            if (el.length)
                el.text(error);
            else
                return false;
        }
    };
    return true;
}

function DoValidationExceptionReset() {
    $(".DOValidationError").text("");
}


///////Modal dialogs
function showModalDialog(dialogtarget, dialogdatakey, dialogtitle) {
   
    if (dialogtarget) {
        $(document).data('dialogdatakey', dialogdatakey);

        var de = $("#" + dialogtarget)

        var dialogonload = de.attr("dialogonload");
        if (dialogonload)
            eval(dialogonload);

        DoValidationExceptionReset();

        var dw = (de.attr("dialogwidth") == undefined ? "300px" : de.attr("dialogwidth"));
        var dh = (de.attr("dialogheight") == undefined ? "auto" : de.attr("dialogheight"));
        de.dialog({
            title: (dialogtitle) ? dialogtitle : de.attr("dialodtitle"),
            appendTo: "form",
            width: dw,
            height: dh,
            modal: true
        });
    }
    return false;
}

$(document).ready(function () {
    ///Modal dialogs
    $(".modalDialogCaller").on("click", function () {
        return showModalDialog($(this).attr("dialogtarget"), $(this).attr("dialogdatakey"), $(this).attr("dialodtitle"));
    });
});

/////// User Input control.

function userInputDisable() {
    $("body").addClass("loadingspinner");
}

function userInputEnable() {
    $("body").removeClass("loadingspinner");
}
