﻿///Calls Method on the same page (public static, marked with [WebMethod] attribute);
function AjaxServiceSamePage(MethodName, DataStructure, successFn, errorFn) {
    var sPath = window.location.pathname;
    var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

    var jData = JSON.stringify(DataStructure);

    $.ajax({
        type: "POST",
        url: sPage + "/" + MethodName,
        data: jData,

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            successFn(msg);
        },
        error: function (xhr, ajaxOptions, error) {
            if (typeof errorFn == 'function') {
                errorFn(xhr, ajaxOptions, error)
            }
            else {
                alert(unescape(xhr.responseText.Message));
            }
        }
    });
}

///Calls Method 
function AjaxService(MethodName, DataStructure, successFn, errorFn) {
//    var sPath = window.location.pathname;
//    var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

    var jData = JSON.stringify(DataStructure);

    $.ajax({
        type: "POST",
        url: MethodName,
        data: jData,

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            successFn(msg);
        },
        error: function (xhr, ajaxOptions, error) {
            if (typeof errorFn == 'function') {
                errorFn(xhr, ajaxOptions, error)
            }
            else {
                alert(unescape(xhr.responseText.Message));
            }
        }
    });
}


///Analog to jQuery Load but loads not from URL, but from [WebMethod] on the local page
function LoadFromServiceSamePage(MethodName, DataStructure, LoadingElement, successFn) {
    AjaxServiceSamePage(MethodName
        , DataStructure
        , function (msg) {
            LoadingElement.html(msg.d);
            if (typeof successFn == 'function') {
                successFn(msg)
            }
        });
    }

///save cookies
function writeCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

///read cookies
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

