body {
  margin: 0;font - size: .7 em;font - family: Verdana,
  Arial,
  Helvetica,
  sans - serif;
}
code {
  margin: 0;color: #006600;font-size:1.1em;font-weight:bold;}
.config_source code{font-size:.8em;color:# 000000;
}
pre {
  margin: 0;font - size: 1.4 em;word - wrap: break -word;
}
ul, ol {
  margin: 10 px 0 10 px 5 px;
}
ul.first, ol.first {
  margin - top: 5 px;
}
fieldset {
  padding: 0 15 px 10 px 15 px;word - break: break -all;
}
.summary - container fieldset {
  padding - bottom: 5 px;
  margin - top: 4 px;
}
legend.no - expand - all {
  padding: 2 px 15 px 4 px 10 px;margin: 0 0 0 - 12 px;
}
legend {
  color: #333333;;margin:4px 0 8px -12px;_margin-top:0px;
font-weight:bold;font-size:1em;}
a:link,a:visited{color:# 007 EFF;font - weight: bold;
}
a: hover {
  text - decoration: none;
}
h1 {
  font - size: 2.4 em;
  margin: 0;
  color: #FFF;
}
h2 {
  font - size: 1.7 em;
  margin: 0;
  color: #CC0000;
}
h3 {
  font - size: 1.4 em;
  margin: 10 px 0 0 0;
  color: #CC0000;
}
h4 {
  font - size: 1.2 em;
  margin: 10 px 0 5 px 0;
}#
header {
  width: 96 % ;margin: 0 0 0 0;padding: 6 px 2 % 6 px 2 % ;font - family: "trebuchet MS",
  Verdana,
  sans - serif;
  color: #FFF;background - color: #5C87B2;
}# content {
      margin: 0 0 0 2 % ;position: relative;
    }
    .summary - container,
  .content - container {
    background: #FFF;width: 96 % ;margin - top: 8 px;padding: 10 px;position: relative;
  }
  .content - container p {
    margin: 0 0 10 px 0;
  }#
  details - left {
    width: 35 % ;float: left;margin - right: 2 % ;
  }#
  details - right {
    width: 63 % ;float: left;overflow: hidden;
  }#
  server_version {
    width: 96 % ;_height: 1 px;min - height: 1 px;margin: 0 0 5 px 0;padding: 11 px 2 % 8 px 2 % ;color: #FFFFFF;
    background - color: #5A7FA5;border-bottom:1px solid # C1CFDD;border - top: 1 px solid #4A6C8E;font-weight:normal;
 font-size:1em;color:# FFF;text - align: right;
  }#
  server_version p {
    margin: 5 px 0;
  }
  table {
    margin: 4 px 0 4 px 0;width: 100 % ;border: none;
  }
  td,
  th {
    vertical - align: top;
    padding: 3 px 0;
    text - align: left;
    font - weight: normal;
    border: none;
  }
  th {
    width: 30 % ;text - align: right;padding - right: 2 % ;font - weight: bold;
  }
  thead th {
    background - color: #ebebeb;
    width: 25 % ;
  }#
  details - right th {
    width: 20 % ;
  }
  table tr.alt td,
  table tr.alt th {}
  .highlight - code {
    color: #CC0000;font - weight: bold;font - style: italic;
  }
  .clear {
    clear: both;
  }
  .preferred {
    padding: 0 5 px 2 px 5 px;font - weight: normal;background: #006633;color:# FFF;font - size: .8 em;
  }