mssql-PIT-UNI
-------------
![Anuncio](img/pit2020.png)

* Instructor: José Luis Garavito Alejos [jgaravitoa@uni.pe](mailto:jgaravitoa@uni.pe)
* [Q&A forum](https://groups.google.com/forum/#!forum/grupo-uni-sql)
* [Unofficial course's website](https://carlosal1015.gitlab.io/mssql-pit-uni)

## Overview

Microsoft SQL Server is a relational database management system
developed by Microsoft. As a database server, it is a software product
with the primary function of storing and retrieving data as requested
by other software applications—which may run either on the same
computer or on another computer across a network (including the
Internet).

## Lessons

| Dates | Topics | Programs | Notebooks |
|:-----:|:-------|:---------|:----------|
| [$`06/05/2020`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/first) | History of relational databases,<br> Structured Query Language,<br> Relational Database Management System,<br> Create, read, update and delete,<br> Gartner Magic Quadrant,<br> Conceptural, logical and phyisical model,<br>Online analytical processing,<br>Online transaction processing,<br> Multi-dimensional online analytical processing,<br> Relational Online analytical processing,<br> Components of SQL Server. | [`CLS01.sql`](https://gitlab.com/carlosal1015/mssql-pit-uni/raw/master/lessons/first/CLS01.sql) | [`CLS01.ipynb`](https://gitlab.com/carlosal1015/mssql-pit-uni/blob/master/lessons/first/CLS01.ipynb) |
| [$`08/05/2020`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/second)| ASCII, UNICODE, No UNICODE, Data types,<br> Querying data with T-SQL. | [`CLS02.sql`](https://gitlab.com/carlosal1015/mssql-pit-uni/raw/master/lessons/second/CLS02.sql) | [`CLS02.ipynb`](https://gitlab.com/carlosal1015/mssql-pit-uni/blob/master/lessons/second/CLS02.ipynb) |
| [$`11/05/2020`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/third) |                                 | [`CLS03.sql`](https://gitlab.com/carlosal1015/mssql-pit-uni/raw/master/lessons/third/CLS03.sql) | [`CLS03.ipynb`](https://gitlab.com/carlosal1015/mssql-pit-uni/blob/master/lessons/third/CLS03.ipynb) |
| [$`13/05/2020`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/fourth)|                                 | [`CLS04.sql`](https://gitlab.com/carlosal1015/mssql-pit-uni/raw/master/lessons/fourth/CLS04.sql)| [`CLS04.ipynb`](https://gitlab.com/carlosal1015/mssql-pit-uni/blob/master/lessons/fourth/CLS04.ipynb)|
| [$`15/05/2020`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/fifth) | Partial exam.                   | [`CLS05.sql`](https://gitlab.com/carlosal1015/mssql-pit-uni/raw/master/lessons/fitth/CLS05.sql) | [`CLS05.ipynb`](https://gitlab.com/carlosal1015/mssql-pit-uni/blob/master/lessons/fifth/CLS05.ipynb) |
| [$`18/05/2020`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/sixth) |                                 | [`CLS06.sql`](https://gitlab.com/carlosal1015/mssql-pit-uni/raw/master/lessons/sixth/CLS06.sql) | [`CLS06.ipynb`](https://gitlab.com/carlosal1015/mssql-pit-uni/blob/master/lessons/sixth/CLS06.ipynb) |
| [$`20/05/2020`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/seventh)|                                | [`CLS07.sql`](https://gitlab.com/carlosal1015/mssql-pit-uni/raw/master/lessons/seventh/CLS07.sql) | [`CLS07.ipynb`](https://gitlab.com/carlosal1015/mssql-pit-uni/blob/master/lessons/seventh/CLS07.ipynb)|
| [$`22/05/2020`$](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/eighth)| Final exam and substitute exam. | [`CLS08.sql`](https://gitlab.com/carlosal1015/mssql-pit-uni/raw/master/lessons/sixth/CLS08.sql) | [`CLS08.ipynb`](https://gitlab.com/carlosal1015/mssql-pit-uni/blob/master/lessons/eighth/CLS08.ipynb) |