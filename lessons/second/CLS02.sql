
SELECT HOST_NAME() AS [HOST];
SELECT @@VERSION AS [VERSION];

-- Creación de Base de datos 
-- [SERVIDOR].[ESQUEMA].[BASEDATOS].[TABLA]

-- 1) Script para la creación de bases de datos.
--  1.1) Eliminamos todas las bases de datos del sistema.

WHILE EXISTS( SELECT name
FROM sysdatabases
WHERE name NOT IN ( 'master',
					'tempdb',
					'model',
					'msdb',
					'ReportServer',
					'ReportServerTempDB'))
BEGIN
	DECLARE @strSQL AS VARCHAR(100)
	SET @strSQL =  ( SELECT TOP 1
		' DROP DATABASE ' + name
	FROM sysdatabases
	WHERE name NOT IN ( 'master',
						'tempdb',
						'model',
						'msdb',
						'ReportServer',
						'ReportServerTempDB'))
	EXECUTE (@strSQL)
END
GO


-- 1.2) Script para crear la base de datos
USE MASTER
GO

IF exists( SELECT name
FROM sysdatabases
WHERE name='UNI')
DROP DATABASE UNI
GO

CREATE DATABASE UNI
ON PRIMARY -- TABLA FÍSICA
(NAME='UNI_DAT',
 FILENAME='/var/opt/mssql/data/UNI_DAT.mdf',
 SIZE=30MB,
 MAXSIZE=50MB,
 FILEGROWTH=20%
 )
 LOG ON
 (NAME='UNI_LOG',
  FILENAME='/var/opt/mssql/data/UNI_LOG.ldf',
  SIZE=10MB,
  MAXSIZE=UNLIMITED,
  FILEGROWTH=10%)
  GO

--2) Creación de Tabla  
USE UNI
GO

-- List all tables from database.
SELECT
	name,
	crdate
FROM
	SYSOBJECTS
WHERE
  xtype = 'U';
GO

--3) Creación de Tabla con constraints

CREATE TABLE Rol
(
	cod_rol_ti TINYINT IDENTITY (1, 1) NOT NULL
		CONSTRAINT  pk_rol_cod_rol_ti PRIMARY KEY CLUSTERED (cod_rol_ti ASC),
	nom_rol_vc VARCHAR(20) NULL
)
GO

CREATE TABLE Empleado
(
	cod_emp_in integer IDENTITY (1, 1) NOT NULL
		CONSTRAINT pk_empleado_cod_emp_in PRIMARY KEY CLUSTERED (cod_emp_in ASC),
	cod_rol_ti TINYINT NOT NULL
		CONSTRAINT fk_rol_empleado_cod_rol_ti FOREIGN KEY  REFERENCES Rol(cod_rol_ti),
	nom_emp_vc VARCHAR(30) NULL ,
	ape_pat_emp_vc VARCHAR(40) NULL ,
	ape_mat_emp_vc VARCHAR(40) NULL ,
	dni_emp_ch char(8) NULL
		CONSTRAINT  ck_empleado_dni_emp_ch CHECK ( dni_emp_ch LIKE ('[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')),
	ruc_emp_ch CHAR(11) NULL ,
	CONSTRAINT ck_empleado_ruc_emp_ch CHECK ( ruc_emp_ch LIKE ('[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')),
	fec_nac_emp_dt DATE NOT NULL ,
	cod_emp_vc VARCHAR(30) NULL ,
	dir_emp_vc VARCHAR(150) NULL ,
	cel_emp_vc VARCHAR(20) NULL ,
	obs_emp_vc VARCHAR(150) NULL ,
	est_emp_bi BIT NULL ,
	ema_emp_vc VARCHAR(50) NULL ,
	CONSTRAINT ck_empleado_ema_emp_vc CHECK (ema_emp_vc LIKE ('%_@_%._%')),
	pwd_emp_vb varbinary(128) NULL ,
	fec_cre_emp_dt DATE NULL
		CONSTRAINT df_empleado_fec_cre_emp_dt DEFAULT GETDATE(),
	cod_cre_emp_in INTEGER NULL ,
	fec_mod_emp_dt DATE NULL
		CONSTRAINT df_empleado_fec_mod_emp_dt DEFAULT CURRENT_TIMESTAMP,
	cod_mod_emp_in INTEGER NULL
)
GO

--  Autorizar acceso a diagrama 
ALTER AUTHORIZATION ON DATABASE::UNI to [SA]

--5) Revisar los objetos que hemos creado  

SELECT xtype, case xtype 
WHEN 'PK' THEN 'LLave primaria'
WHEN 'F'  THEN 'LLave foranea'
WHEN 'U'  THEN 'Tabla'
WHEN 'D'  THEN 'Restricción Default'
WHEN 'C'  THEN 'Restricción Check'
END Descripción , name
FROM sysobjects
WHERE xtype IN ('PK','F' ,'U','D','C')
ORDER BY xtype

--7) Ingresando datos en rol.

SELECT *
FROM ROl

INSERT INTO Rol
Values
	('Almacén'),
	('Personal'),
	('Ventas'),
	('Transporte'),
	('Sistemas'),
	('Facturación'),
	('Compras')

-- Ingresando registro mediatne TRY-CATCH

BEGIN TRY
INSERT INTO [dbo].[Empleado]
	([cod_rol_ti],[nom_emp_vc],[ape_pat_emp_vc],[ape_mat_emp_vc]
	,[dni_emp_ch],[ruc_emp_ch],[fec_nac_emp_dt],[cod_emp_vc]
	,[dir_emp_vc],[cel_emp_vc],[obs_emp_vc],[est_emp_bi]
	,[ema_emp_vc],[pwd_emp_vb],[cod_cre_emp_in])
VALUES
	(7, 'Carlos', 'Valdiviezo', 'Contreras',
		'2345678', '12345678912', CONVERT(date,'02/04/1992'), 'CVCONTRERAS',
		'Calle los alisos 222', '987678876', 'Empleado suele llegar tarde', 1,
		'charlie@mail.com', PWDENCRYPT('uni'), 1)
END TRY
BEGIN CATCH
	SELECT ERROR_NUMBER() as ErrorNumber,
	ERROR_STATE() as ErrorState,
	ERROR_SEVERITY() as ErrorSeverity,
	ERROR_PROCEDURE() as ErrorProcedure,
	ERROR_LINE() as  ErrorLine,
	ERROR_MESSAGE() as ErrorMessage;
END CATCH