Segunda clase $`08/05/2020`$
----------------------------

[CLS02.sql](CLS02.sql)

[CLS02.ipynb](CLS02.ipynb)

![Preparación](../../study/videos/spanish/2_preparation.mp4)

![Primera parte](../../study/videos/spanish/2_firstPart.mp4)

![Segunda parte](../../study/videos/spanish/2_secondPart.mp4)

![Tercera parte](../../study/videos/spanish/2_thirdPart.mp4)

![Cuarta parte](../../study/videos/spanish/2_fourthPart.mp4)

![Quinta parte](../../study/videos/spanish/2_fifthPart.mp4)

![Sexta parte](../../study/videos/spanish/2_sixthPart.mp4)