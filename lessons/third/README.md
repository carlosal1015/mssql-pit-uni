Tercera clase $`11/05/2020`$
----------------------------

[CLS03.sql](CLS03.sql)

[CLS03.ipynb](CLS03.ipynb)

![Preparación](../../study/videos/spanish/3_preparation.mp4)

![Primera parte](../../study/videos/spanish/3_firstPart.mp4)

![Segunda parte](../../study/videos/spanish/3_secondPart.mp4)

![Tercera parte](../../study/videos/spanish/3_thirdPart.mp4)

![Cuarta parte](../../study/videos/spanish/3_fourthPart.mp4)

![Quinta parte](../../study/videos/spanish/3_fifthPart.mp4)