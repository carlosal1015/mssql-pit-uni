USE UNI;
GO

SELECT *
FROM DATA

--PROCESO DE NORMALIZACIÓN
--1) Revisar la longitud de los tipos de Dato

SELECT
	MAX(LEN(ACCESORIO)) AS MAX_LONG_ACCESORIO, -- VARCHAR(25)
	MAX(LEN(CODIGO)) AS MAX_LONG_CODIGO, -- VARCHAR(15)
	MAX(LEN(DESCRIPCION)) AS MAX_LONG_DESCRIPCION, -- VARCHAR(85)
	MAX(LEN(STOCK)) AS MAX_LONG_STOCK, -- TINYINT
	MAX(LEN(PRECIO)) AS MAX_LONG_PRE, -- SMALLMONEY 
	MAX(LEN(MARCA)) AS MAX_LONG_MARCA
-- VARCHAR(26)
FROM DATA

SELECT *
FROM DATA

--2) Creando la tabla Producto  para la migración
CREATE TABLE Producto
(
	cod_prd_in INT NOT NULL IDENTITY(1, 1)
		CONSTRAINT pk_producto_cod_prd_in PRIMARY KEY clustered,
	cod_prd_vc VARCHAR(15) NOT NULL,
	acs_prd_in INT NOT NULL,
	des_prd_vc VARCHAR(85) NULL,
	stk_prd_ti TINYINT NOT NULL,
	pre_prd_sm SMALLMONEY NULL,
	mar_prd_in INT NOT NULL
)


--3) Revisar las repeticiones  marca, accesorio y código
SELECT COUNT(*) Cantidad_Registros
FROM DATA

SELECT ACCESORIO, COUNT(*) as Cantidad
FROM data
GROUP BY ACCESORIO
ORDER BY Cantidad DESC
-- En total 254 registros únicos.

SELECT MARCA, COUNT(*) as Cantidad
FROM data
GROUP BY MARCA
ORDER BY Cantidad DESC
--117  registros únicos de Marca 

--4) Creando tabla tipologia
CREATE TABLE Tipologia
(
	cod_tip_in INT NOT NULL IDENTITY(1, 1)
		CONSTRAINT pk_tipologia_cod_tip_in PRIMARY KEY clustered,
	nom_tip_vc VARCHAR(26) NOT NULL,
	cod_pad_tip_in INT NOT NULL
)

-- SELECT * FROM Tipologia
-- SELECT * FROM Producto

-- 5) Ver los valores distintos de Marca y Accesorio (distinct)

INSERT INTO  Tipologia
	( nom_tip_vc,cod_pad_tip_in)
	SELECT 'PRINCIPAL' as nom_tip_vc, 1 as cod_pad_tip_in
UNION ALL
	SELECT 'MARCA' , 2
UNION ALL
	SELECT 'ACCESORIO' , 3
UNION ALL
	SELECT distinct marca, 2
	FROM data
UNION ALL
	SELECT distinct accesorio, 3
	FROM data

--6)Viendo la tabla tipologia
SELECT *
FROM Tipologia

--7) Normalización de la tabla Data,
-- Cambiar los nombres de ACCESORIO Y MARCA  por sus códigos enteros  
-- (Provienen de la tabla TIPOLOGIA)
-- Script para actualizar esta data 

MERGE [DATA] as TARGET
USING Tipologia as SOURCE
ON ( TARGET.ACCESORIO=SOURCE.nom_tip_vc)
WHEN MATCHED THEN
UPDATE SET TARGET.ACCESORIO=SOURCE.cod_tip_in
OUTPUT $action,
Deleted.ACCESORIO as TargetAccesorio,
Inserted.ACCESORIO as SourceCodigo;
SELECT @@ROWCOUNT
GO


MERGE [DATA] as TARGET
USING Tipologia as SOURCE
ON ( TARGET.MARCA=SOURCE.nom_tip_vc)
WHEN MATCHED THEN
UPDATE SET TARGET.MARCA=SOURCE.cod_tip_in
OUTPUT $action,
Deleted.MARCA as TargetAccesorio,
Inserted.MARCA as SourceCodigo;
SELECT @@ROWCOUNT
GO

--8 ) Migrar la data a la tabla Producto ( la cual ya está optimizada en Tipo de Datos


INSERT INTO PRODUCTO
	(acs_prd_in,cod_prd_vc,des_prd_vc,stk_prd_ti,pre_prd_sm,mar_prd_in)
SELECT
	CONVERT(INT, ACCESORIO) AS acs_prd_in,
	CONVERT(VARCHAR(15),upper(ltrim(rtrim(CODIGO)))) AS cod_prd_vc,
	CASE 
WHEN DESCRIPCION LIKE '%@@@%' THEN   UPPER(LTRIM(RTRIM(CONVERT(VARCHAR(85),REPLACE(DESCRIPCION,'@@@','')))))
WHEN DESCRIPCION LIKE '%@@%' THEN   UPPER(LTRIM(RTRIM(CONVERT(VARCHAR(85),REPLACE(DESCRIPCION,'@@','')))))
WHEN DESCRIPCION LIKE '%[@%' THEN   UPPER(LTRIM(RTRIM(CONVERT(VARCHAR(85),REPLACE(DESCRIPCION,'[@','')))))
WHEN DESCRIPCION LIKE '%.[%' THEN   UPPER(LTRIM(RTRIM(CONVERT(VARCHAR(85),REPLACE(DESCRIPCION,'.[','')))))
ELSE 
UPPER(LTRIM(RTRIM(CONVERT(VARCHAR(85), DESCRIPCION)))) 
END  AS des_prd_vc,
	CASE ISNULL(STOCK,0)
WHEN '>20' THEN 21
WHEN '' THEN 0
ELSE CONVERT(TINYINT, STOCK)
END AS stk_prd_ti,
	CASE ISNULL(PRECIO, 0)
WHEN '9999999.99' THEN '1000'
else CONVERT(SMALLMONEY, PRECIO)
END AS pre_prd_sm,
	CONVERT(INT, marca) AS mar_prd_in
FROM DATA

-- En vez de stock '>20' cambiaremos a 21.
-- En PRECIO cambiaremos a 1000.
-- En vez de Valores vacíos de Stock cambiamos a 0.

--9)  Generando las relaciones.

ALTER TABLE Producto ADD CONSTRAINT fk_tipologia_producto_acs_prd_in
FOREIGN KEY (acs_prd_in) REFERENCES  Tipologia(cod_tip_in)
ALTER TABLE Producto ADD CONSTRAINT fk_tipologia_producto_mar_prd_in
FOREIGN KEY (mar_prd_in) REFERENCES Tipologia(cod_tip_in)
ALTER TABLE Tipologia  ADD CONSTRAINT fk_tipologia_tipologia_cod_pad_tip_in
FOREIGN KEY (cod_pad_tip_in) REFERENCES Tipologia(cod_tip_in)

--10) Creación de esquemas 

IF EXISTS( SELECT name
FROM sys.schemas
WHERE name='Almacen')
DROP SCHEMA Almacen
GO
CREATE SCHEMA Almacen

IF EXISTS( SELECT name
FROM sys.schemas
WHERE name='Sistemas')
DROP SCHEMA Sistemas
GO
CREATE SCHEMA Sistemas


-- Migrar las tablas a los esquemas.

ALTER SCHEMA Almacen TRANSFER dbo.[Producto]
ALTER SCHEMA Sistemas TRANSFER dbo.[Tipologia]

ALTER AUTHORIZATION ON DATABASE::UNI to [SA]