
SELECT HOST_NAME() AS [HOST NAME];
-- Creación de base de datos 
-- [SERVIDOR].[ESQUEMA].[BASEDATOS].[TABLA]

-- 1) Script para creación de base de datos.
--  1.1) Eliminamos todas las bases de datos del sistema.

WHILE EXISTS(
  SELECT name
  FROM sysdatabases
  WHERE name NOT IN ( 'master',
                      'tempdb',
                      'model',
                      'msdb',
                      'ReportServer',
                      'ReportServerTempDB'))
BEGIN
  DECLARE @strSQL AS VARCHAR(100)
  SET @strSQL =  (
    SELECT TOP 1 ' DROP DATABASE ' + name
    FROM sysdatabases
    WHERE name NOT IN ( 'master',
                        'tempdb',
                        'model',
                        'msdb',
                        'ReportServer',
                        'ReportServerTempDB'))
  EXECUTE (@strSQL)
END
GO

-- 1.2) Script para crear la base de datos

USE MASTER
GO

IF exists (
  SELECT name
FROM sysdatabases
WHERE name='UNI')
DROP DATABASE UNI
GO

CREATE DATABASE UNI
ON PRIMARY ( -- TABLA FÍSICA
  NAME='UNI_DAT',
  FILENAME='/var/opt/mssql/data/UNI_DAT.mdf',
  SIZE=30MB,
  MAXSIZE=50MB,
  FILEGROWTH=20%)
LOG ON (
  NAME='UNI_LOG',
  FILENAME='/var/opt/mssql/data/UNI_LOG.ldf',
  SIZE=10MB,
  MAXSIZE=UNLIMITED,
  FILEGROWTH=10%)
GO

--2) Creación de Tabla  

USE UNI
GO

CREATE TABLE Empleado
(
  cod_emp_in INTEGER IDENTITY(1,1) NOT NULL,
  nom_emp_vc VARCHAR(30) NULL,
  ape_pat_emp_vc VARCHAR(30) NULL,
  ape_mat_emp_vc VARCHAR(30) NULL,
  dni_emp_ch char(8) NOT NULL,
  -- obs_emp_vc VARCHAR(30) NOT NULL,
  -- sex_emp_vc BIT NOT NULL
)
GO

-- 3) INSERTANDO PRIMER REGISTRO

INSERT INTO Empleado
  (nom_emp_vc,ape_pat_emp_vc,ape_mat_emp_vc,dni_emp_ch)
VALUES
  ('JAVIER', 'LOAYZA', 'JIMENEZ', '04504523')

SELECT * FROM Empleado;