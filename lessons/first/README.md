Primera clase $`06/05/2020`$
----------------------------

[CLS01.sql](CLS01.sql)

[CLS01.ipynb](CLS01.ipynb)

![Preparación](../../study/videos/spanish/1_preparation.mp4)

![Primera parte](../../study/videos/spanish/1_firstPart.mp4)

![Segunda parte](../../study/videos/spanish/1_secondPart.mp4)

![Tercera parte](../../study/videos/spanish/1_thirdPart.mp4)

![Cuarta parte](../../study/videos/spanish/1_fourthPart.mp4)

![Quinta parte](../../study/videos/spanish/1_fifthPart.mp4)

![Sexta parte](../../study/videos/spanish/1_sixthPart.mp4)

![Séptima parte](../../study/videos/spanish/1_seventhPart.mp4)

![Octava parte](../../study/videos/spanish/1_eighthPart.mp4)

![Novena parte](../../study/videos/spanish/1_ninthPart.mp4)