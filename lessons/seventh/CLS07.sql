
CREATE table  tbl_error_usp
(ERROR_LINE int null,
 ERROR_MESSAGE  varchar(4000) null,
 ERROR_NUMBER int null,
 ERROR_PROCEDURE varchar(128) null,
 ERROR_SEVERITY int null,
 ERROR_STATE int null)


 IF EXISTS( select*from sys.schemas s inner join sys.procedures p
			 on  s.schema_id=P.schema_id
			 WHERE s.name='Almacen' and p.name='usp_detalle_de_pedido_ingreso')
DROP PROCEdure Almacen.usp_detalle_de_pedido_ingreso
GO
CREATE PROC  Almacen.usp_detalle_de_pedido_ingreso
(@cod_ped_in int,--30
 @cod_prd_in int,--85
 @cnt_ped_det_de decimal(18,4),
 @pre_ven_ped_det_de money,
 @des_ped_det_de float,
 @cod_est_ped_det_ti int,
 @fec_ent_ped_det_dt datetime,
 @cod_ped_com_in int,
 @cod_tra_inv_in int)
 AS
 BEGIN
	BEGIN TRY
			BEGIN
				 BEGIN TRAN btDetalledePedido -- CHEKPOINT 
				 INSERT INTO Ventas.PedidoDetalle
				 (cod_ped_in,cod_prd_in,cnt_ped_det_de,pre_ven_ped_det_de,des_ped_det_de,cod_est_ped_det_ti,
				  fec_ent_ped_det_dt,cod_ped_com_in,cod_tra_inv_in)
				 values
				 (@cod_ped_in ,@cod_prd_in ,@cnt_ped_det_de ,@pre_ven_ped_det_de ,@des_ped_det_de , @cod_est_ped_det_ti ,
				 @fec_ent_ped_det_dt ,@cod_ped_com_in ,@cod_tra_inv_in)
				 COMMIT TRAN btDetalledePedido  
			END
	END TRY
	BEGIN CATCH
		  DECLARE @ERROR_LINE AS int=ERROR_LINE()
		  DECLARE @ERROR_MESSAGE as nvarchar(4000)=ERROR_MESSAGE()
		  DECLARE @ERROR_NUMBER as int =ERROR_NUMBER()
		  DECLARE @ERROR_PROCEDURE as NVARCHAR(128)= ERROR_PROCEDURE()
		  DECLARE @ERROR_SEVERITY as int =ERROR_SEVERITY()
		  DECLARE @ERROR_STATE  AS INT =ERROR_STATE()
		  IF @@TRANCOUNT=1
		     ROLLBACK TRAN btDetalledePedido
			 INSERT INTO tbl_error_usp
			 (   [ERROR_LINE] ,
				 [ERROR_MESSAGE]  ,
				 [ERROR_NUMBER] ,
				 [ERROR_PROCEDURE] ,
				 [ERROR_SEVERITY] ,
				 [ERROR_STATE])
			VALUES
			(    @ERROR_LINE ,
				 @ERROR_MESSAGE  ,
				 @ERROR_NUMBER ,
				 @ERROR_PROCEDURE ,
				 @ERROR_SEVERITY ,
				 @ERROR_STATE)
	END CATCH
 END
 GO


 ALTER TABLE Ventas.Pedidodetalle add constraint ak_ventas_detalles_de_pedido
 UNIQUE (cod_ped_in,cod_prd_in) 


select*from ventas.Pedido
where cod_ped_in=30

select*from ventas.PedidoDetalle
where cod_ped_in=30

SELECT*FROM Almacen.Producto
WHERE cod_prd_in IN (34,80)

EXEC Almacen.usp_detalle_de_pedido_ingreso 30,85,4,15,0,2,'20-05-2020',100,12

select*from tbl_error_usp

SET LANGUAGE SPANISH

--- Revisando ejemplo de eliminación
-- Un procedimiento para intentar eliminar un cliente  
--  haciendo uso de constraints

alter proc  Ventas.usp_cliente_cod_cli_in
(@cod_cli_in INT)
AS
BEGIN
	 BEGIN TRY
		BEGIN
			BEGIN TRAN btnEliminarCliente
			DELETE FROM Ventas.Cliente
			WHERE cod_cli_in=@cod_cli_in
			COMMIT TRAN btnEliminarCliente
		END
	 END TRY
	 BEGIN CATCH
		 DECLARE @ERROR_LINE AS int=ERROR_LINE()
		  DECLARE @ERROR_MESSAGE as nvarchar(4000)=ERROR_MESSAGE()
		  DECLARE @ERROR_NUMBER as int =ERROR_NUMBER()
		  DECLARE @ERROR_PROCEDURE as NVARCHAR(128)= ERROR_PROCEDURE()
		  DECLARE @ERROR_SEVERITY as int =ERROR_SEVERITY()
		  DECLARE @ERROR_STATE  AS INT =ERROR_STATE()
		  IF @@TRANCOUNT=1
		     ROLLBACK TRAN btnEliminarCliente
			 INSERT INTO tbl_error_usp
			 (   [ERROR_LINE],
				 [ERROR_MESSAGE],
				 [ERROR_NUMBER],
				 [ERROR_PROCEDURE],
				 [ERROR_SEVERITY] ,
				 [ERROR_STATE])
			VALUES
			(    @ERROR_LINE ,
				 @ERROR_MESSAGE  ,
				 @ERROR_NUMBER ,
				 @ERROR_PROCEDURE ,
				 @ERROR_SEVERITY ,
				 @ERROR_STATE)
	 END CATCH
END
GO



SELECT*FROM Ventas.Pedido
ORDER BY cod_cli_in

--Cliente 5 nunca realizó ninguan compra
--Cliente 6 sí realizó hasta 6 compras

ALTER TABLE Ventas.Pedido ADD constraint fk_cliente_ventas_pedido_cod_cli_in
FOREIGN KEY (cod_cli_in) REFERENCES Ventas.Cliente(cod_cli_in)

SELECT*FROM Ventas.Cliente
SELECT*FROM tbl_error_usp
EXEC Ventas.usp_cliente_cod_cli_in 6

--  



CREATE VIEW v_ganancia_por_factura
AS
select nom_cli_vc+ ' ' + pat_cli_vc as cliente,p.cod_ped_in,fec_ped_dt,
-- sum(Q*(Precio venta*(1-descuento) - Precio de COmpra) )
(sum(pd.cnt_ped_det_de*(pd.pre_ven_ped_det_de*(1-pd.des_ped_det_de)))- gas_env_ped_mo) as Ganancia
from Ventas.PedidoDetalle pd 
inner join Ventas.Pedido p on p.cod_ped_in=pd.cod_ped_in
inner join Ventas.Cliente c on c.cod_cli_in=p.cod_cli_in
inner join Almacen.Producto prd on prd.cod_prd_in=pd.cod_prd_in
group by nom_cli_vc+ ' ' + pat_cli_vc,p.cod_ped_in,fec_ped_dt,gas_env_ped_mo
order by cliente 
go

-- Convirtiendo tabla física a tabla de Vista 
select*into  GANANCIA_POR_FACTURA FROM  v_ganancia_por_factura

SELECT*FROM GANANCIA_POR_FACTURA
ORDER BY CLIENTE,GANANCIA

exec sp_help 'GANANCIA_POR_FACTURA'


-- uSO DE fUNCIONES   Windows 
create VIEW v_ganancia_por_factura
AS
       select  nom_cli_vc+ ' '+ pat_cli_vc as cliente,p.cod_ped_in,fec_ped_dt,
        --sum( q* (precio de venta - precio de compra))
        sum(pd.cnt_ped_det_de*(pd.pre_ven_ped_det_de*(1-pd.des_ped_det_de)-prd.pre_com_prd_mo))
        as Ganancia
        from  Ventas.PedidoDetalle  pd
        inner join Ventas.Pedido p on pd.cod_ped_in=p.cod_ped_in
        inner join Ventas.Cliente c on c.cod_cli_in=p.cod_cli_in
        inner join Almacen.Producto prd on prd.cod_prd_in=pd.cod_prd_in
        group by nom_cli_vc+ ' '+ pat_cli_vc,p.cod_ped_in,fec_ped_dt
SELECT*FROM v_ganancia_por_factura
---  Ranking total , ranking realtivo ,  Pctje de las ventas de cada, que tanto representa
--- el porcenta de ventas a nivel de persona  por cada factura
SELECT cliente,cod_ped_in,Ganancia,fec_ped_dt,
LAST_VALUE(ganancia)  OVER (partition by cliente order by cod_ped_in, fec_ped_dt
							ROWS BETWEEN current ROW and
							UNBOUNDED FOLLOWING) as [CotaInferiorRelativa],
FIRST_VALUE(ganancia)  OVER (PARTITION BY cliente  order by cod_ped_in,fec_ped_dt
						     ROWS between UNBOUNDED PRECEDING AND CURRENT ROW) as  [CotaSuperiorRelativa] ,
NTILE(2)  OVER (ORDER BY ganancia)  AS P50,
NTILE(4)  OVER (ORDER BY ganancia)  AS P25,
NTILE(10)  OVER (ORDER BY ganancia)  AS P10,CAST(100.0*ganancia/SUM(ganancia) OVER(PARTITION by cliente) as numeric(5,2)) pctje_relativo,
CAST(100.0*ganancia/SUM(ganancia) OVER() as numeric(5,2)) as pcte_acumulado,
LAG(fec_ped_dt) over (order by cliente) as valor_siguiente,
LEAD(fec_ped_dt) over (order by cliente) as siguiente_valor,
-- contador, ranking relativo
ROW_NUMBER() OVER ( ORDER BY cliente) Contador,
ROW_NUMBER()  OVER( PARTITION BY CLIENTE ORDER BY ganancia   desc)  as [Ranking Relativo],
RANK() OVER ( ORDER BY ganancia desc) as [Ranking Total],
SUM(ganancia) OVER(PARTITION BY cliente order by cod_ped_in 
				   ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS Acumulado_Relativo,
SUM(ganancia) OVER(order by Cliente 
				   ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS Acumulado_total
FROM v_ganancia_por_factura 
order by cliente




-- Funciones  Recursivas 
CREATE 

-- MODELAMIENTO DE DATOS
-- ADM DE BASE DE DATOS
-- IMPLEMENTACIÓN FÍSICA
-- PROGRAMACIÓN 
-- ADMINISTRACIÓN DE LA SEGURIDAD


USE CTIC

CREATE TABLE dbo.Empleado (
    cod_emp_in int NOT NULL primary key clustered,
    cod_jef_emp_in int NULL,
    nom_emp_vc varchar(25) NOT NULL,
    sal_emp_mo money NOT NULL)








ALTER TABLE dbo.Empleado  ADD  CONSTRAINT fk_empleado_empleado_sal_emp_mo FOREIGN KEY(cod_jef_emp_in)
REFERENCES dbo.Empleado (cod_emp_in)


GO
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (1, NULL, N'Carlos', 10000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (2, 1, N'Pepe', 7000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (3, 1, N'Raul', 7500.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (4, 2, N'Roberto', 5000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (5, 2, N'Juan', 5500.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (6, 2, N'Yostin', 4500.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (7, 3, N'Isabel', 5000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (8, 5, N'Carmen', 3500.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (9, 7, N'Cristian', 3000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (10, 5, N'Abigail', 3000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (11, 7, N'Laura', 3000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (12, 9, N'Clara', 2000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (13, 9, N'Maria Cecilia', 2000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (14, 9, N'Dario', 1500.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (15, 9, N'Junior', 1400.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (16, 11, N'Edgar', 1200.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (17, 11, N'Valentín', 1000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (18, 11, N'Fiorella', 1000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (19, 12, N'Apolinario', 1000.0000)
INSERT [dbo].[Empleado] ([cod_emp_in], [cod_jef_emp_in], [nom_emp_vc], [sal_emp_mo]) VALUES (20, 13, N'Rosa', 950.0000)
select*from Empleado



-- Empleando una función  empleando CTEs
CREATE  function dbo.Subarbol1(@cod_jef_emp_in as int , @maxlevels as int= null)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
	WITH EmpsCTE as 
	(select cod_emp_in, CAST(NULL  as int) as cod_jef_emp_in,nom_emp_vc, sal_emp_mo, 0 as lvl,
	 cast('.' AS varchar(900)) as puestos
	 FROM dbo.Empleado
	 WHERE cod_emp_in=@cod_jef_emp_in
	 UNION ALL 
	 SELECT  S.cod_emp_in,S.cod_jef_emp_in, S.nom_emp_vc,S.sal_emp_mo,M.lvl+1 as lvl,
	 CAST(m.puestos+CAST(S.cod_emp_in as varchar(10))+ '.' as  varchar(900)) as puestos
	 FROM  EmpsCTE  as M  INNER JOIN  dbo.Empleado as S 
	 ON s.cod_jef_emp_in=M.cod_emp_in AND (M.lvl<@maxlevels or @maxlevels is null)
	 )


	 SELECT cod_emp_in,cod_jef_emp_in, nom_emp_vc,sal_emp_mo,lvl,puestos
	 FROM EmpsCTE;

select cod_emp_in, REPLICATE(' | ',lvl) + nom_emp_vc as Empleado, cod_jef_emp_in, sal_emp_mo,lvl,puestos
FROM dbo.Subarbol1(1,null)


-- Función recursiva empleado  tablas con indices agrupados


