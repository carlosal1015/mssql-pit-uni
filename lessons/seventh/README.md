Séptima clase $`20/05/2020`$
----------------------------

[CLS07.sql](CLS07.sql)

[CLS07.ipynb](CLS07.ipynb)

![Primera parte](../../study/videos/spanish/7_firstPart.mp4)

![Segunda parte](../../study/videos/spanish/7_secondPart.mp4)

![Tercera parte](../../study/videos/spanish/7_thirdPart.mp4)

![Cuarta parte](../../study/videos/spanish/7_fourthPart.mp4)

![Quinta parte](../../study/videos/spanish/7_fifthPart.mp4)

![Sexta parte](../../study/videos/spanish/7_sixthPart.mp4)