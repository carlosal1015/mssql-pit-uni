Sexta clase $`18/05/2020`$
--------------------------

[CLS06.sql](CLS06.sql)

[CLS06.ipynb](CLS06.ipynb)

![Primera parte](../../study/videos/spanish/6_firstPart.mp4)

![Segunda parte](../../study/videos/spanish/6_secondPart.mp4)

![Tercera parte](../../study/videos/spanish/6_thirdPart.mp4)

![Cuarta parte](../../study/videos/spanish/6_fourthPart.mp4)

![Quinta parte](../../study/videos/spanish/6_fifthPart.mp4)

![Sexta parte](../../study/videos/spanish/6_sixthPart.mp4)

![Séptima parte](../../study/videos/spanish/6_seventhPart.mp4)

![Octava parte](../../study/videos/spanish/6_eighthPart.mp4)