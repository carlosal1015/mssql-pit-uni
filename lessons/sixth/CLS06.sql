ALTER AUTHORIZATION ON DATABASE::NorthWindSQL to [SA]


USE NorthWindSQL
GO

-- Trabajando con fechas 
--cONVERTIR A formato estándar Perú

SELECT replace(SUBSTRING(convert(varchar(10), GETDATE(), 103),1,2),'0','')
+  ' De ' + CASE  SUBSTRING(convert(varchar(10), GETDATE(), 103),4,2)
			WHEN '05' then 'Mayo'
			END
 +
' del ' + substring(convert(varchar(10), getdate(),103) ,7,4)

--Operadores matematicos 
SELECT 5+2 as suma,
	5-2 as diferencia,
	5*2 as Multip,
	5/2 as 'Division Entera',
	5/2.0 as 'División Real',
	CAST( 5 AS float)/2 AS  'Division real',
	CONVERT(float,5)/2  'Division real'

-- Funciones  
-- Funciones escalares un único resultado con su tipo de datos

-- Crear una función escalar para convertir cualquier fecha a cadena  
if exists (select name
from sysobjects
where name='fn_convertir_fecha')
DROP FUNCTION fn_convertir_fecha
GO

create function dbo.fn_convertir_fecha
(@parametro as date)
RETURNS varchar(100)
AS
BEGIN

	declare   @resultado  as varchar(100)
	SET @resultado= (SELECT replace(SUBSTRING(convert(varchar(10), @parametro, 103),1,2),'0','')
+  ' De ' + CASE  SUBSTRING(convert(varchar(10), @parametro, 103),4,2)
			WHEN '01' then 'ENERO'
			WHEN '02' then 'FEBRERO'
			WHEN '03' then 'MARZO'
			WHEN '04' then 'ABRIL'
			WHEN '05' then 'MAYO'
			WHEN '06' then 'JUNIO'
			WHEN '07' then 'JULIO'
			WHEN '08' then 'AGOSTO'
			WHEN '09' then 'SETIEMBRE'
			WHEN '10' then 'OCTUBRE'
			WHEN '11' then 'NOVIEMBRE'
			WHEN '12' then 'DICIEMBRE'
			END+ ' del ' + substring(convert(varchar(10),@parametro,103) ,7,4) )
	RETURN @resultado
END

select dbo.fn_convertir_fecha(convert(date,'2006-01-22')) AS [Fecha convertida]


-- FUnci�n  de Tabla en linea
-- Funci�n para hallar los valores de angulos de un triangulo rectangulo  a partir de sus 2 catetos

if exists (select name
from sysobjects
where name='fn_triangulo_angulos')
DROP FUNCTION fn_triangulo_angulos
GO
CREATe function fn_triangulo_angulos
(@a as float , @b as float)
RETURNS  @TABLE table
([H] float,
	[A] float,
	[B] float)
AS
BEGIN

	DECLARE @H AS  FLOAT=SQRT(POWER(@a,2)+ power(@b,2))
	INSERT INTO @TABLE
		([H],[A],[B])
	values
		(round( @H,2,0), round(ATAN(@a/@b)/PI()*180,0), round(ATAN(@b/@a)/PI()*180,0) )
	RETURN
END

select*
from fn_triangulo_angulos (2,3)
select*
from fn_triangulo_angulos (7,24)
select*
from fn_triangulo_angulos (10,200)


-- Procedimiento almacenado 
-- Realizar un procedimiento almacenado  que  me permite
-- obtener el volumen de un tronco de cono e indicar su  radio mayor y radio menor 


if exists (select name
from sysobjects
where name='sp_volumen_cono')
DROP proc sp_volumen_cono
GO
CREATE PROC sp_volumen_cono
	(@H as float,
	@a as float,
	@b as float)
AS
BEGIN
	declare @V  float
	DECLARE @rspta varchar(100)=''
	SET @V=ROUND(@H*PI()*(POWER(@a,2)+power(@b,2)+@a*@b)/3,2)

	IF @a>@b	
BEGIN
		SET @rspta='El volumen del tronco de Cono  es ' +convert(varchar(10),@V)+ ' El radio Menor '+ convert(varchar(10),@b)+ ' El radio Mayor es '+convert(varchar(10),@a)
	END
ELSE
BEGIN
		SET @rspta='El volumen del tronco de Cono  es ' +convert(varchar(10),@V)+ ' El radio Menor '+ convert(varchar(10),@a)+ ' El radio Mayor es '+convert(varchar(10),@b)
	END
	SELECT @rspta AS [Respuesta]
END

EXEC sp_volumen_cono 2,3,4

select cod_ped_in, fec_ped_dt, dbo.fn_convertir_fecha(fec_ped_dt)
from Ventas.Pedido


--- NorthindSQL 
--- Ver catalogo de Producto
Select*
from Almacen.Producto
-- Cuantos productos por cateoria existen

select nom_cat_prd_vc [Categor�a], count(*) [Cantidad de Productos x Cat]
from Almacen.Producto
group by nom_cat_prd_vc

Select*
from Almacen.Producto
where nom_cat_prd_vc='Bebidas'

-- Revisando stock real o existencias

Select*
From Almacen.TransaccionInventario
select*
from Almacen.TipoTransaccionInventario

-- Contando el actual stock del Parque de Productos 
select p.cod_prd_in, p.nom_prd_vc,
	sum(convert(int,CASE  TI.cod_tip_tra_inv_ti
				when 2 then '-'
				ELSE  '+'
				END  + LTRIM(RTRIM((TI.cnt_prd_in))))) as cantidad
from Almacen.Producto p inner join Almacen.TransaccionInventario ti
	on p.cod_prd_in=ti.cod_prd_in
GROUP BY p.cod_prd_in,p.nom_prd_vc

-- COmprobaci�n
select*
from Almacen.TransaccionInventario
where  cod_prd_in=5


CREATE VIEW v_stock_actual_x_categoria
AS
	select p.nom_cat_prd_vc,
		sum(convert(int,CASE  TI.cod_tip_tra_inv_ti
				when 2 then '-'
				ELSE  '+'
				END  + LTRIM(RTRIM((TI.cnt_prd_in))))) as cantidad
	from Almacen.Producto p inner join Almacen.TransaccionInventario ti
		on p.cod_prd_in=ti.cod_prd_in
	GROUP BY  p.nom_cat_prd_vc

SELECT*
FROM v_stock_actual_x_categoria

-- Pivot  para transponer los valores 






select [Aceite], [Bebidas], [Carne enlatada], [Condimentos], [Frutas y verduras enlatadas],
	[Frutos secos], [Golosinas], [Granos], [Mermeladas y confituras], [Pasta], [Productos horneados],
	[Productos l�cteos], [Salsas], [Sopas]
from v_stock_actual_x_categoria
pivot (SUM(cantidad) for nom_cat_prd_vc IN 
([Aceite],[Bebidas],[Carne enlatada],[Condimentos],[Frutas y verduras enlatadas],
[Frutos secos],[Golosinas],[Granos],[Mermeladas y confituras],[Pasta],[Productos horneados],
[Productos l�cteos],[Salsas],[Sopas])) AS TBL


-- Reportes de Ventas (informaci�n) 
SELECT
	p.cod_cli_in  as [C�digo de Cliente],
	dbo.fn_convertir_fecha(fec_ped_dt)  as [Fecha de Venta],
	c.nom_cli_vc as [Nombre de CLiente],
	e.nom_emp_vc as [Nombre empleado],
	c.dir_cli_vc as [Direcci�n de Cliente],
	t.raz_soc_tra_vc as [Nomre de la EMpresa de transporte],
	prd.nom_prd_vc as [Nombre Producto],
	pd.cnt_ped_det_de as [Cantidad de Producto]
FROM VENTAS.Pedido p
	inner join Personal.Empleado e
	ON P.cod_emp_in=e.cod_emp_in
	inner join Ventas.Cliente c
	on P.cod_cli_in=c.cod_cli_in
	INNER JOIN Transporte.Transportista T
	on  p.cod_tra_in=t.cod_tra_in
	inner join ventas.PedidoDetalle pd
	on p.cod_ped_in=pd.cod_ped_in
	inner join Almacen.Producto prd
	on pd.cod_prd_in=prd.cod_prd_in

-- Crear una consulta para calcular el Margen  de Utilidad por cada CLiente
create view  v_clientes_con_mayor_margen_ganancia
AS
	select
		nom_cli_vc+ ' ' + pat_cli_vc as cliente,
		-- sum(Q*(Precio venta*(1-descuento) - Precio de COmpra) )
		(sum(pd.cnt_ped_det_de*(pd.pre_ven_ped_det_de*(1-pd.des_ped_det_de)))) as [Utilidad x cliente]
	from Ventas.PedidoDetalle pd
		inner join Ventas.Pedido p on p.cod_ped_in=pd.cod_ped_in
		inner join Ventas.Cliente c on c.cod_cli_in=p.cod_cli_in
		inner join Almacen.Producto prd on prd.cod_prd_in=pd.cod_prd_in
	group by nom_cli_vc+ ' ' + pat_cli_vc

-- En las vistas 
select TOP 1
	*
from v_clientes_con_mayor_margen_ganancia
order by  2 desc

-- Ganancia  por cada Factura 

select nom_cli_vc+ ' ' + pat_cli_vc as cliente, p.cod_ped_in, fec_ped_dt,
	-- sum(Q*(Precio venta*(1-descuento) - Precio de COmpra) )
	(sum(pd.cnt_ped_det_de*(pd.pre_ven_ped_det_de*(1-pd.des_ped_det_de)))- gas_env_ped_mo) as [Utilidad x Factura]
from Ventas.PedidoDetalle pd
	inner join Ventas.Pedido p on p.cod_ped_in=pd.cod_ped_in
	inner join Ventas.Cliente c on c.cod_cli_in=p.cod_cli_in
	inner join Almacen.Producto prd on prd.cod_prd_in=pd.cod_prd_in
group by nom_cli_vc+ ' ' + pat_cli_vc,p.cod_ped_in,fec_ped_dt,gas_env_ped_mo
order by [Utilidad x Factura]



select*
from ventas.Pedido
where cod_ped_in=73


-- Procedimientos de inserci�n 
-- TRANSACCIONES, CONTROLES DE ERROR, BITACORAS DE  MANIPULACI�N , TRY CATCH , VALIDACI�N DE EXISTENCIA DEL PROCEDIMIENTO


CREATE table tbl_error_usp
(
	ERROR_LINE int null,
	ERROR_MESSAGE varchar(4000) null,
	ERROR_NUMBER int null,
	ERROR_PROCEDURE varchar(128) null,
	ERROR_SEVERITY int null,
	ERROR_STATE int null
)

SELECT*
FROM tbl_error_usp


IF EXISTS( select*
from sys.schemas s inner join sys.procedures p
	on  s.schema_id=P.schema_id
WHERE s.name='Almacen' and p.name='usp_detalle_de_pedido_ingreso')
DROP PROCEdure Almacen.usp_detalle_de_pedido_ingreso
GO
CREATE PROC  usp_detalle_de_pedido_ingreso
	(@cod_ped_in int,
	@cod_prd_in int,
	@cnt_ped_det_de decimal(18,4),
	@pre_ven_ped_det_de money,
	@des_ped_det_de float,
	@cod_est_ped_det_ti int,
	@fec_ent_ped_det_dt datetime,
	@cod_ped_com_in int,
	@cod_tra_inv_in int)
AS
BEGIN
	BEGIN TRY
			BEGIN
		BEGIN TRAN btDetalledePedido
		-- CHEKPOINT 
		INSERT INTO Ventas.PedidoDetalle
			(cod_ped_in,cod_prd_in,cnt_ped_det_de,pre_ven_ped_det_de,des_ped_det_de,cod_est_ped_det_ti,
			fec_ent_ped_det_dt,cod_ped_com_in,cod_tra_inv_in)
		values
			(@cod_ped_in , @cod_prd_in , @cnt_ped_det_de , @pre_ven_ped_det_de , @des_ped_det_de , @cod_est_ped_det_ti ,
				@fec_ent_ped_det_dt , @cod_ped_com_in , @cod_tra_inv_in)
		COMMIT TRAN btDetalledePedido
	END
	END TRY
	BEGIN CATCH
		  DECLARE @ERROR_LINE AS int=ERROR_LINE()
		  DECLARE @ERROR_MESSAGE as nvarchar(4000)=ERROR_MESSAGE()
		  DECLARE @ERROR_NUMBER as int =ERROR_NUMBER()
		  DECLARE @ERROR_PROCEDURE as NVARCHAR(128)= ERROR_PROCEDURE()
		  DECLARE @ERROR_SEVERITY as int =ERROR_SEVERITY()
		  DECLARE @ERROR_STATE  AS INT =ERROR_STATE()

		  IF @@TRANCOUNT=1
		     ROLLBACK TRAN btDetalledePedido

			 INSERT INTO tbl_error_usp
		( [ERROR_LINE] ,
		[ERROR_MESSAGE] ,
		[ERROR_NUMBER] ,
		[ERROR_PROCEDURE] ,
		[ERROR_SEVERITY] ,
		[ERROR_STATE])
	VALUES
		( @ERROR_LINE ,
			@ERROR_MESSAGE  ,
			@ERROR_NUMBER ,
			@ERROR_PROCEDURE ,
			@ERROR_SEVERITY ,
			@ERROR_STATE)

	END CATCH
END
 GO

ALTER TABLE Ventas.Pedidodetalle add constraint ak_ventas_detalles_de_pedido
 UNIQUE (cod_ped_in,cod_prd_in)

select*
from ventas.PedidoDetalle
where cod_ped_in=30

EXEC usp_detalle_de_pedido_ingreso 30,3,4,27,0,2,'2020-04-22',90,4

select*
from tbl_error_usp
