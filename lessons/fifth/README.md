Quinta clase $`15/05/2020`$
---------------------------

[CLS04.sql](CLS05.sql)

[CLS04.ipynb](CLS05.ipynb)

![Primera parte](../../study/videos/spanish/5_firstPart.mp4)

![Segunda parte](../../study/videos/spanish/5_secondPart.mp4)

![Tercera parte](../../study/videos/spanish/5_thirdPart.mp4)

![Cuarta parte](../../study/videos/spanish/5_fourthPart.mp4)

![Quinta parte](../../study/videos/spanish/5_fifthPart.mp4)

![Post partial exam](../../study/videos/spanish/5_post_partial_exam.mp4)