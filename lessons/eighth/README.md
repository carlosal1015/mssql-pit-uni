Octava clase $`22/05/2020`$
---------------------------

[CLS08.sql](CLS08.sql)

[CLS08.ipynb](CLS08.ipynb)

![Primera parte](../../study/videos/spanish/8_firstPart.mp4)

![Segunda parte](../../study/videos/spanish/8_secondPart.mp4)

![Tercera parte](../../study/videos/spanish/8_thirdPart.mp4)

![Cuarta parte](../../study/videos/spanish/8_fourthPart.mp4)

![Quinta parte](../../study/videos/spanish/8_fifthPart.mp4)

![Sexta parte](../../study/videos/spanish/8_sixthPart.mp4)

![Séptima parte](../../study/videos/spanish/8_seventhPart.mp4)

![Octava parte](../../study/videos/spanish/8_eighthPart.mp4)

![Novena parte](../../study/videos/spanish/8_ninthPart.mp4)