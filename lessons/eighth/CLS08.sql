

-- PARTE 1: RESOLUCI�N TAREA     
-- Problema Sumatoria
USE MASTER
IF EXISTS
( SELECT name FROM sysobjects
	WHERE  xtype='fn'  AND name='fn_sumatoria'
)
DROP FUNCTION fn_sumatoria
GO
CREATE FUNCTION fn_sumatoria
(@X as float, @n as int)
RETURNS float
AS
BEGIN
DECLARE @sumatoria as float=0
DECLARE @bloque as float=-1
DECLARE @cnt as int=1
WHILE @cnt<@n+1 BEGIN
                SET @bloque=((-1)*@bloque*POWER(@X,2))/((2*@cnt-1)*(2*@cnt))
                SET @sumatoria=@sumatoria+@bloque
                SET @cnt=@cnt+1
                END
                RETURN(@sumatoria)
                END
                --Pruebas del resultado
    select dbo.fn_sumatoria(3,2)
    select dbo.fn_sumatoria(345,6)
               
			   
			   
			   
			   
			    -- Problema conversi�n Palabras
                IF EXISTS
                (
                SELECT name
                FROM sysobjects
                WHERE
                xtype='fn'
                AND
                name='fn_num_pal'
                )
                DROP FUNCTION fn_num_pal
                GO
                CREATE FUNCTION fn_num_pal
                (
                @num AS INT
                )
                RETURNS VARCHAR(200)
                AS
                BEGIN
                DECLARE @pal AS VARCHAR(200)=''
                IF @num>=0 AND @num <=9999
	BEGIN
		IF @num=0
		BEGIN 
			SET @pal='Cero'
		END
		ELSE
		BEGIN
			DECLARE @M AS INT
			DECLARE @C AS INT
			DECLARE @D AS INT
			DECLARE @U AS INT
			SET @M = @num / 1000
			SET @num %= 1000
			SET @C = @num / 100
			SET @num %= 100
			SET @D = @num / 10
			SET @U = @num % 10
			SET @pal =
			(
				SELECT 
					CASE @M
						WHEN 1 THEN 'Un Mil '
						WHEN 2 THEN 'Dos Mil '
						WHEN 3 THEN 'Tres Mil '
						WHEN 4 THEN 'Cuatro Mil '
						WHEN 5 THEN 'Cinco Mil '
						WHEN 6 THEN 'Seis Mil '
						WHEN 7 THEN 'Siete Mil '
						WHEN 8 THEN 'Ocho Mil '
						WHEN 9 THEN 'Nueve Mil '
						ELSE ''
					END
				+
					CASE @C
						WHEN 1 THEN 
							CASE @D + @U
								WHEN 0 THEN 'Cien '
								ELSE 'Ciento '
							END
						WHEN 2 THEN 'Doscientos '
						WHEN 3 THEN 'Trescientos '
						WHEN 4 THEN 'Cuatrocientos '
						WHEN 5 THEN 'Quinientos '
						WHEN 6 THEN 'Seiscientos '
						WHEN 7 THEN 'Setecientos '
						WHEN 8 THEN 'Ochocientos '
						WHEN 9 THEN 'Novecientos '
						ELSE ''
					END
					+
					CASE @D
						WHEN 1 THEN 
							CASE @U
								WHEN 0 THEN 'Diez'
								WHEN 1 THEN 'Once'
								WHEN 2 THEN 'Doce'
								WHEN 3 THEN 'Trece'
								WHEN 4 THEN 'Catorce'
								WHEN 5 THEN 'Quince'
								WHEN 6 THEN 'Dieciseis'
								WHEN 7 THEN 'Diecisiete'
								WHEN 8 THEN 'Dieciocho'
								WHEN 9 THEN 'Diecinueve'
							END 
						WHEN 2 THEN 
							CASE @U
								WHEN 0 THEN 'Veinte'
								ELSE 'Veinti'
							END
						WHEN 3 THEN 
							CASE @U
								WHEN 0 THEN 'Treinta'
								ELSE 'Treinta y '
							END
						WHEN 4 THEN 
							CASE @U
								WHEN 0 THEN 'Cuarenta'
								ELSE 'Cuarenta y '
							END
						WHEN 5 THEN 
							CASE @U
								WHEN 0 THEN 'Cincuenta'
								ELSE 'Cincuenta y '
							END
						WHEN 6 THEN 
							CASE @U
								WHEN 0 THEN 'Sesenta'
								ELSE 'Sesenta y '
							END
						WHEN 7 THEN 
							CASE @U
								WHEN 0 THEN 'Setenta'
								ELSE 'Setenta y '
							END
						WHEN 8 THEN 
							CASE @U
								WHEN 0 THEN 'Ochenta'
								ELSE 'Ochenta y '
							END
						WHEN 9 THEN 
							CASE @U
								WHEN 0 THEN 'Noventa'
								ELSE 'Noventa y '
							END
						ELSE ''
					END
					+
					CASE @D
						WHEN 1 THEN ''
						ELSE
						CASE @U
							WHEN 1 THEN 'Uno'
							WHEN 2 THEN 'Dos'
							WHEN 3 THEN 'Tres'
							WHEN 4 THEN 'Cuatro'
							WHEN 5 THEN 'Cinco'
							WHEN 6 THEN 'Seis'
							WHEN 7 THEN 'Siete'
							WHEN 8 THEN 'Ocho'
							WHEN 9 THEN 'Nueve'
							ELSE ''
						END
					END
			)
		END
		END
	ELSE
	BEGIN
		SET @pal = 'ERROR: N�mero fuera del rango'
	END
	RETURN(UPPER(@pal))
END
GO


SELECT dbo.fn_num_pal(11000) AS Palabras



IF EXISTS
( SELECT name FROM sysobjects
	WHERE  xtype='fn'  AND name='fn_ubicacion_pepito'
)
DROP FUNCTION fn_ubicacion_pepito
GO
create FUNCTION dbo.fn_ubicacion_pepito
(
 @XA  float,  @YA  float,-- punto a
 @XB  float,  @YB  float,-- punto b
 @XC  float,  @YC  float -- c
)
RETURNS  varchar(200)
-- Creamos una tabla donde colocaremos los 2 valores
AS
BEGIN
--Para encontrar el punto equidistante  de las 3 ubicaciones debemos  hallar el circuncentro  del Trinagulo ABC
--Debemos intersectar s�lo 2 Mediatrices (Cualquiera de las 3 existentes) en el Punto P  (dicho punto ser� el circuncentro)
-- M: mediatriz de AC   y N: mediatriz de BC
--Hallamos los  componentes de los puntos M y N
DECLARE @mx AS FLOAT = (@XA +@XC)/2
DECLARE @my AS FLOAT = (@YA +@YC)/2
DECLARE @nx AS FLOAT = (@XB +@XC)/2
DECLARE @ny AS FLOAT = (@YB +@YC)/2
--Hallando componentes del vector BC
DECLARE @BCx AS FLOAT= @XC-@XB
DECLARE @BCy AS FLOAT= @YC-@YB
--Hallando Vector AC
DECLARE @ACx as FLOAT= @XC-@XA
DECLARE @ACy as FLOAT= @YC-@YA
--Hallando componentes ortogonal de BC
DECLARE @BCox as FLOAT= @YB-@YC
DECLARE @BCoy as FLOAT= @XC-@XB
--Hallando componentes ortogonal de AC
DECLARE @ACox AS FLOAT= @YA-@YC
DECLARE @ACoy AS FLOAT= @XC-@XA
-- El vector Ortogonal  de BC lo llamaremos  BCo    y Vector Ortogonal de AC lo llamaremos  ACo
-- S� la recta mediatriz LN y LM  son las dos rectas cuyas ecuaciones vectoriales son:
-- P = N + t*BCo (Recta que pasa por N y con vector direccional BCo)
-- P = M + r*ACo (Recta que pasa por M y con vector direccional ACo)
-- Al ser un punto en com�n ambas rectas, entonces P brinda la soluci�n de los puntos equidistantes (se igualan ambas ecuaciones)
-- N+ t*BCo = M + r*ACo
-- t*BCo=NM + r*ACo
-- Multiplicamos la ecuaci�n anterior por el vector AC
-- t*BCo*AC=NM*AC + r*ACo*AC (Se anula este �ltimo componente --r*ACo*AC-- por multiplicaci�n  ortogonal )
--Finalmente debemos despejar t= (NM.AC)/(BCo.AC)       
-- Ahora hallamos  los componentes del Vector NM
DECLARE @NMx as float= @mx-@nx
DECLARE @NMy as float= @my-@ny
-- Al ser multiplicaci�n de Vectores debemos hallar el Coseno el Angulo que existe entre ellos y los m�dulos    
--COSENO(angulo)=(NM.AC)/(Modulo(NM)*Modulo(AC))
-- Hallamos los m�dulos de los Vectores NM, AC y BCo
 DECLARE @mod_NM as float = SQRT(POWER(@NMx,2) + POWER(@NMy,2))
 DECLARE @mod_AC as float = SQRT(POWER(@ACx,2) + POWER(@ACy,2))
 DECLARE @mod_BCo as float = SQRT(POWER(@BCox,2) + POWER(@BCoy,2))
-- Hallamos los cosenos  de ambos angulos 
 DECLARE @COS_NM_AC  AS FLOAT=(@NMx*@ACx+@NMy*@ACy)/(@mod_NM*@mod_AC)
 DECLARE @COS_BCo_AC AS FLOAT=(@BCox*@ACx+@BCoy*@ACy)/(@mod_BCo*@mod_AC)
 -- Calculamos el valor de t de  la ecuaci�n t= (NM.AC)/(BCo.AC) 
 DECLARE @t as float =((@mod_NM*@mod_AC*@COS_NM_AC)/(@mod_BCo*@mod_AC*@COS_BCo_AC)) 
-- Finalmente nuestro Punto  Circuncentro est� descrito por : P = N + t*BCo 
-- Insertamos los valores 
RETURN ( convert(varchar(100),( @nx + @t*(@BCox)))+ '*'+  convert(varchar(100),(@ny + @t*(@BCoy))))
END
GO


-- Probamos la consulta solicitada:
SELECT  DBO.fn_ubicacion_pepito(-12.0887795, -77.0269371,-12.0932477,-77.0637517,-12.1202762,-77.0376792)
USE UNI
GO

create table ubicacion
(XA   float not null,  YA   float not null,
 XB  float not null,  YB   float not null,
 XC  float not null,  YC  float not null,
 ResX  float  null, ResY  float  null)
 
USE [UNI]
GO
INSERT [dbo].[ubicacion] ([XA], [YA], [XB], [YB], [XC], [YC], [ResX], [ResY]) VALUES (-12.041191,	-76.931981,	    -12.013972,	    -76.938794,	    -12.022777,	    -76.909151,     NULL,NULL)

INSERT [dbo].[ubicacion] ([XA], [YA], [XB], [YB], [XC], [YC], [ResX], [ResY]) VALUES (-12.0260041,	-77.0722065,	-12.0459985,	-77.0355303,	-11.9942325,	-77.0634276,	NULL,NULL)
INSERT [dbo].[ubicacion] ([XA], [YA], [XB], [YB], [XC], [YC], [ResX], [ResY]) VALUES (-12.0887795,	-77.0269371,	-12.0932477,	-77.0637517,	-12.1202762,	-77.0376792,	NULL,NULL)


SELECT*FROM ubicacion

--MiniBatch
--Actualizando ubicaciones 
UPDATE ubicacion
SET ResX=convert(float,SUBSTRING(dbo.fn_ubicacion_pepito(XA,YA,XB,YB,XC,YC),1,8)),
    ResY=convert(float,SUBSTRING(dbo.fn_ubicacion_pepito(XA,YA,XB,YB,XC,YC),10,18))


------------------------------------------------------------------------------
------------------------------------------------------------------------------
----------------------INDICES-------------------------------------------------
------------------------------------------------------------------------------

USE MASTER
GO
IF exists( select name from sysdatabases 
           where name='UNI')
DROP DATABASE UNI
GO
CREATE DATABASE UNI
ON PRIMARY
(NAME='UNI_DAT',
 FILENAME='D:\SQL\UNI_DAT.mdf',
 SIZE=30MB,
 MAXSIZE=UNLIMITED,
 FILEGROWTH=20%
 )
 LOG ON
 (NAME='UNI_LOG',
  FILENAME='D:\SQL\UNI_LOG.ldf',
  SIZE=10MB,
  MAXSIZE=UNLIMITED,
  FILEGROWTH=10%)
  GO

  USE UNI
  GO
-- Sin indices,  Como b�squeda de Table Scan
IF EXISTS(SELECT name from sys.tables 
		   where name='TINKA_SCAN')
DROP TABLE TINKA_SCAN
GO
CREATE table TINKA_SCAN
(cod_tnk_in  int not null,
 v01 int not null,
 v02 int not null,
 v03 int not null,
 v04 int not null,
 v05 int not null,
 v06 int not null)
--  Proceso para  crear valores aleatorios
DECLARE @v01 as int
DECLARE @v02 as int
DECLARE @v03 as int
DECLARE @v04 as int
DECLARE @v05 as int
DECLARE @v06 as int
DECLARE @OP AS  BIT=1
DECLARE @T AS INT 
DECLARE @cnt as int=0
--  Insert masivo de bolillas aleatorias que no se repiten entre s� 
WHILE  (@cnt<100000) BEGIN
                   SET @OP=1
                   SET @v01=CONVERT(INT,RAND()*45+1)-- 23
                   SET @v02=CONVERT(INT,RAND()*45+1)--45
                   SET @v03=CONVERT(INT,RAND()*45+1)--1
                   SET @v04=CONVERT(INT,RAND()*45+1)--38
                   SET @v05=CONVERT(INT,RAND()*45+1)--10
                   SET @v06=CONVERT(INT,RAND()*45+1)--7
                   IF @v01=@V02 SET @OP=0
                   IF @v01=@V03 SET @OP=0
                   IF @v01=@V04 SET @OP=0
                   IF @v01=@V05 SET @OP=0
                   IF @v01=@V06 SET @OP=0
                   IF @V02=@V03 set @OP=0
                   if @v02=@v04 set @OP=0
                   if @v02=@v05 set @OP=0
                   if @v02=@v06 set @OP=0
                   if @v03=@v04 set @OP=0
                   if @v03=@v05 set @OP=0
                   if @v03=@v06 set @OP=0
                   if @v04=@v05 set @OP=0
                   if @v04=@v06 set @OP=0
                   if @v05=@v06 set @OP=0
                   -- ORDENDAMIENTO de las bolillas de manera ascendente (v01...v06)
                   --- 23 43 12 8 5 30
                   IF @OP=1
                   BEGIN
                   IF @v01>@V02
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V02
		  SET @V02=@T
	  END
	   IF @v01>@V03
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V03
		  SET @V03=@T
	  END
	  
	  IF @v01>@V04
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V04
		  SET @V04=@T
	  END
	  IF @v01>@V05
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V05
		  SET @V05=@T
	  END
	  IF @v01>@V06
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V06
		  SET @V06=@T
	  END
	  IF @v02>@V03
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V03
		  SET @V03=@T
	  END
	  IF @v02>@V04
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V04
		  SET @V04=@T
	  END
	    IF @v02>@V05
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V05
		  SET @V05=@T
	  END
	  
	   IF @v02>@V06
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V06
		  SET @V06=@T
	  END
	   IF @v03>@V04
	  BEGIN 
		  SET  @T=@v03
		  SET  @V03=@V04
		  SET @V04=@T
	  END
	   IF @v03>@V05
	  BEGIN 
		  SET  @T=@v03
		  SET  @V03=@V05
		  SET  @V05=@T
	  END
	    IF @v03>@V06
	  BEGIN 
		  SET  @T=@v03
		  SET  @V03=@V06
		  SET  @V06=@T
	  END
	   IF @v04>@V05
	  BEGIN 
		  SET  @T=@v04
		  SET  @V04=@V05
		  SET  @V05=@T
	  END
	  
	     IF @v04>@V06
	  BEGIN 
		  SET  @T=@v04
		  SET  @V04=@V06
		  SET  @V06=@T
	  END
	   IF @v05>@V06
	  BEGIN 
		  SET  @T=@v05
		  SET  @V05=@V06
		  SET  @V06=@T
	  END
	INSERT INTO dbo.TINKA_SCAN
	           (cod_tnk_in ,
				 v01 ,
				 v02 ,
				 v03 ,
				 v04 ,
				 v05 ,
				 v06 )
				VALUES
				(@cnt+1,
				@v01,
				@v02,
				@v03,
				@v04,
				@v05,
				@v06
				)
			 set @cnt+=1
	end
end

select COUNT(*)from TINKA_SCAN
select *from TINKA_SCAN

-- TABLA SCAN

USE UNI 
GO
-- TINKA_CLUSTERED CON  1 indice agrupado , Busqueda Index Seek
IF EXISTS(SELECT name from sys.tables 
		   where name='TINKA_CLUSTERED')
DROP TABLE TINKA_CLUSTERED
GO
CREATE table TINKA_CLUSTERED
(cod_tnk_in  int not null identity(1,1) primary key clustered,
 v01 int not null,
 v02 int not null,
 v03 int not null,
 v04 int not null,
 v05 int not null,
 v06 int not null)
--  Proceso para  crear valores aleatorios
DECLARE @v01 as int
DECLARE @v02 as int
DECLARE @v03 as int
DECLARE @v04 as int
DECLARE @v05 as int
DECLARE @v06 as int
DECLARE @OP AS  BIT=1
DECLARE @T AS INT 
DECLARE @cnt as int=0
--  Insert masivo de bolillas aleatorias que no se repiten entre s� 
WHILE  (@cnt<100000) BEGIN
                   SET @OP=1
                   SET @v01=CONVERT(INT,RAND()*45+1)-- 23
                   SET @v02=CONVERT(INT,RAND()*45+1)--45
                   SET @v03=CONVERT(INT,RAND()*45+1)--1
                   SET @v04=CONVERT(INT,RAND()*45+1)--38
                   SET @v05=CONVERT(INT,RAND()*45+1)--10
                   SET @v06=CONVERT(INT,RAND()*45+1)--7
                   IF @v01=@V02 SET @OP=0
                   IF @v01=@V03 SET @OP=0
                   IF @v01=@V04 SET @OP=0
                   IF @v01=@V05 SET @OP=0
                   IF @v01=@V06 SET @OP=0
                   IF @V02=@V03 set @OP=0
                   if @v02=@v04 set @OP=0
                   if @v02=@v05 set @OP=0
                   if @v02=@v06 set @OP=0
                   if @v03=@v04 set @OP=0
                   if @v03=@v05 set @OP=0
                   if @v03=@v06 set @OP=0
                   if @v04=@v05 set @OP=0
                   if @v04=@v06 set @OP=0
                   if @v05=@v06 set @OP=0
                   -- ORDENDAMIENTO de las bolillas de manera ascendente (v01...v06)
                   --- 23 43 12 8 5 30
                   IF @OP=1
                   BEGIN
                   IF @v01>@V02
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V02
		  SET @V02=@T
	  END
	   IF @v01>@V03
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V03
		  SET @V03=@T
	  END
	  
	  IF @v01>@V04
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V04
		  SET @V04=@T
	  END
	  IF @v01>@V05
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V05
		  SET @V05=@T
	  END
	  IF @v01>@V06
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V06
		  SET @V06=@T
	  END
	  IF @v02>@V03
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V03
		  SET @V03=@T
	  END
	  IF @v02>@V04
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V04
		  SET @V04=@T
	  END
	    IF @v02>@V05
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V05
		  SET @V05=@T
	  END
	  
	   IF @v02>@V06
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V06
		  SET @V06=@T
	  END
	   IF @v03>@V04
	  BEGIN 
		  SET  @T=@v03
		  SET  @V03=@V04
		  SET @V04=@T
	  END
	   IF @v03>@V05
	  BEGIN 
		  SET  @T=@v03
		  SET  @V03=@V05
		  SET  @V05=@T
	  END
	    IF @v03>@V06
	  BEGIN 
		  SET  @T=@v03
		  SET  @V03=@V06
		  SET  @V06=@T
	  END
	   IF @v04>@V05
	  BEGIN 
		  SET  @T=@v04
		  SET  @V04=@V05
		  SET  @V05=@T
	  END
	  
	     IF @v04>@V06
	  BEGIN 
		  SET  @T=@v04
		  SET  @V04=@V06
		  SET  @V06=@T
	  END
	   IF @v05>@V06
	  BEGIN 
		  SET  @T=@v05
		  SET  @V05=@V06
		  SET  @V06=@T
	  END
	INSERT INTO dbo.TINKA_CLUSTERED
	           ( v01 ,
				 v02 ,
				 v03 ,
				 v04 ,
				 v05 ,
				 v06 )
				VALUES
				(@v01,
				@v02,
				@v03,
				@v04,
				@v05,
				@v06
				)
			 set @cnt+=1
	end
end
USE UNI 
GO

SELECT COUNT(*) FROM TINKA_CLUSTERED
SELECT*FROM TINKA_CLUSTERED


-- TINKA_CLUSTERED y NONCLUSTERED CON  1 indice agrupado  Y CON 6 indices no agrupados
IF EXISTS(SELECT name from sys.tables 
		   where name='TINKA_CLUSTERED_Y_NONCLUSTERED')
DROP TABLE TINKA_CLUSTERED_Y_NONCLUSTERED
GO
CREATE table TINKA_CLUSTERED_Y_NONCLUSTERED
(cod_tnk_in  int not null identity(1,1) primary key clustered,
 v01 int not null,
 v02 int not null,
 v03 int not null,
 v04 int not null,
 v05 int not null,
 v06 int not null)
--  Proceso para  crear valores aleatorios
DECLARE @v01 as int
DECLARE @v02 as int
DECLARE @v03 as int
DECLARE @v04 as int
DECLARE @v05 as int
DECLARE @v06 as int
DECLARE @OP AS  BIT=1
DECLARE @T AS INT 
DECLARE @cnt as int=0
--  Insert masivo de bolillas aleatorias que no se repiten entre s� 
WHILE  (@cnt<100000) BEGIN
                   SET @OP=1
                   SET @v01=CONVERT(INT,RAND()*45+1)-- 23
                   SET @v02=CONVERT(INT,RAND()*45+1)--45
                   SET @v03=CONVERT(INT,RAND()*45+1)--1
                   SET @v04=CONVERT(INT,RAND()*45+1)--38
                   SET @v05=CONVERT(INT,RAND()*45+1)--10
                   SET @v06=CONVERT(INT,RAND()*45+1)--7
                   IF @v01=@V02 SET @OP=0
                   IF @v01=@V03 SET @OP=0
                   IF @v01=@V04 SET @OP=0
                   IF @v01=@V05 SET @OP=0
                   IF @v01=@V06 SET @OP=0
                   IF @V02=@V03 set @OP=0
                   if @v02=@v04 set @OP=0
                   if @v02=@v05 set @OP=0
                   if @v02=@v06 set @OP=0
                   if @v03=@v04 set @OP=0
                   if @v03=@v05 set @OP=0
                   if @v03=@v06 set @OP=0
                   if @v04=@v05 set @OP=0
                   if @v04=@v06 set @OP=0
                   if @v05=@v06 set @OP=0
                   -- ORDENDAMIENTO de las bolillas de manera ascendente (v01...v06)
                   --- 23 43 12 8 5 30
                   IF @OP=1
                   BEGIN
                   IF @v01>@V02
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V02
		  SET @V02=@T
	  END
	   IF @v01>@V03
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V03
		  SET @V03=@T
	  END
	  
	  IF @v01>@V04
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V04
		  SET @V04=@T
	  END
	  IF @v01>@V05
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V05
		  SET @V05=@T
	  END
	  IF @v01>@V06
	  BEGIN 
		  SET  @T=@v01
		  SET  @V01=@V06
		  SET @V06=@T
	  END
	  IF @v02>@V03
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V03
		  SET @V03=@T
	  END
	  IF @v02>@V04
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V04
		  SET @V04=@T
	  END
	    IF @v02>@V05
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V05
		  SET @V05=@T
	  END
	  
	   IF @v02>@V06
	  BEGIN 
		  SET  @T=@v02
		  SET  @V02=@V06
		  SET @V06=@T
	  END
	   IF @v03>@V04
	  BEGIN 
		  SET  @T=@v03
		  SET  @V03=@V04
		  SET @V04=@T
	  END
	   IF @v03>@V05
	  BEGIN 
		  SET  @T=@v03
		  SET  @V03=@V05
		  SET  @V05=@T
	  END
	    IF @v03>@V06
	  BEGIN 
		  SET  @T=@v03
		  SET  @V03=@V06
		  SET  @V06=@T
	  END
	   IF @v04>@V05
	  BEGIN 
		  SET  @T=@v04
		  SET  @V04=@V05
		  SET  @V05=@T
	  END
	  
	     IF @v04>@V06
	  BEGIN 
		  SET  @T=@v04
		  SET  @V04=@V06
		  SET  @V06=@T
	  END
	   IF @v05>@V06
	  BEGIN 
		  SET  @T=@v05
		  SET  @V05=@V06
		  SET  @V06=@T
	  END
	INSERT INTO dbo.TINKA_CLUSTERED_Y_NONCLUSTERED
	           (
				 v01 ,
				 v02 ,
				 v03 ,
				 v04 ,
				 v05 ,
				 v06 )
				VALUES
				(@v01,
				@v02,
				@v03,
				@v04,
				@v05,
				@v06
				)
			 set @cnt+=1
	end
end
CREATE NONCLUSTERED INDEX ie_tinka_v01 ON TINKA_CLUSTERED_Y_NONCLUSTERED(V01)
GO
CREATE NONCLUSTERED INDEX ie_tinka_v02 ON TINKA_CLUSTERED_Y_NONCLUSTERED(V02)
GO
CREATE NONCLUSTERED INDEX ie_tinka_v03 ON TINKA_CLUSTERED_Y_NONCLUSTERED(V03)
GO
CREATE NONCLUSTERED INDEX ie_tinka_v04 ON TINKA_CLUSTERED_Y_NONCLUSTERED(V04)
GO
CREATE NONCLUSTERED INDEX ie_tinka_v05 ON TINKA_CLUSTERED_Y_NONCLUSTERED(V05)
GO
CREATE NONCLUSTERED INDEX ie_tinka_v06 ON TINKA_CLUSTERED_Y_NONCLUSTERED(V06)
GO

-- Revisando Cantidad 
select count(*) from TINKA_SCAN
select count(*) from TINKA_CLUSTERED
select count(*) from  TINKA_CLUSTERED_Y_NONCLUSTERED
-- Realizando b�squedas 

--- Comparando potencia del Index Clustered vs Scan
select* from TINKA_SCAN
where   cod_tnk_in=100000 

select* from TINKA_CLUSTERED
where   cod_tnk_in=100000 
-----

--Validando que ambos busquedas en ambas tablas es lo mismo 
-------
select* from TINKA_CLUSTERED
where   cod_tnk_in=100000 

select * from  TINKA_CLUSTERED_Y_NONCLUSTERED
where  cod_tnk_in=100000
-------

--Se entiende que al agregar una columna adicional
-- El indice en CLustered con la primera columna hace un esfuerzo menor, pero para la 2da hace mucho esfuerzo
-------
select* from TINKA_SCAN
where   cod_tnk_in=45000 or v01=3

select* from TINKA_CLUSTERED
where   cod_tnk_in=45000 or v01=3
-------

-- Ventaja de Generar las no Clusterizadas
-- Al realizar una busqueda v�a cod tnk. son lo mismo , pero al realizar la busqueda comparando una columna scan y la otra no clusterizada
---
select* from TINKA_CLUSTERED
where   cod_tnk_in=45000 or v01=4

select * from  TINKA_CLUSTERED_Y_NONCLUSTERED
where  cod_tnk_in=45000 or v01=4



-- Cantidad de Personas con 6 aciertos
USE UNI
GO
IF EXISTS
(
	SELECT name
	FROM sys.procedures 
	WHERE name='usp_6_aciertos_cync'
)
DROP PROCEDURE usp_6_aciertos_cync
GO
CREATE PROCEDURE usp_6_aciertos_cync
(
	@n1 AS INT,
	@n2 AS INT,
	@n3 AS INT,
	@n4 AS INT,
	@n5 AS INT,
	@n6 AS INT,
	@cnt AS INT OUTPUT
)
AS
BEGIN
	SET @cnt=
	(
		SELECT COUNT(*) 
		FROM dbo.TINKA_CLUSTERED_Y_NONCLUSTERED
		WHERE 
			v01=@n1 AND
			v02=@n2 AND
			v03=@n3 AND
			v04=@n4 AND
			v05=@n5 AND
			v06=@n6
	)
END
GO




-- Cantidad de Personas con 5 aciertos
USE UNI
GO
IF EXISTS
(
	SELECT name
	FROM sys.procedures 
	WHERE name='usp_5_aciertos_cync'
)
DROP PROCEDURE usp_5_aciertos_cync
GO
CREATE PROCEDURE usp_5_aciertos_cync
(
	@n1 AS INT,
	@n2 AS INT,
	@n3 AS INT,
	@n4 AS INT,
	@n5 AS INT,
	@n6 AS INT,
	@cnt AS INT OUTPUT
)
AS
BEGIN
	SET @cnt=
	(
		SELECT COUNT(*) 
		FROM dbo.TINKA_CLUSTERED_Y_NONCLUSTERED
		WHERE 
			(
				v01<>@n1 AND
				v02=@n2 AND
				v03=@n3 AND
				v04=@n4 AND
				v05=@n5 AND
				v06=@n6
			)
			OR
			(
				v01=@n1 AND
				v02<>@n2 AND
				v03=@n3 AND
				v04=@n4 AND
				v05=@n5 AND
				v06=@n6
			)
			OR
			(
				v01=@n1 AND
				v02=@n2 AND
				v03<>@n3 AND
				v04=@n4 AND
				v05=@n5 AND
				v06=@n6
			)
			OR
			(
				v01=@n1 AND
				v02=@n2 AND
				v03=@n3 AND
				v04<>@n4 AND
				v05=@n5 AND
				v06=@n6
			)
			OR
			(
				v01=@n1 AND
				v02=@n2 AND
				v03=@n3 AND
				v04=@n4 AND
				v05<>@n5 AND
				v06=@n6
			)
			OR
			(
				v01=@n1 AND
				v02=@n2 AND
				v03=@n3 AND
				v04=@n4 AND
				v05=@n5 AND
				v06<>@n6
			)
	)
END
GO


select*From TINKA_CLUSTERED_Y_NONCLUSTERED


EXEC usp_5_aciertos_cync 6,	9,	21,	23,	30,	43
EXEc usp_6_aciertos_cync 6,	9,	21,	23,	30,	43


------------------------------------------------------------------------------
------------------------------------------------------------------------------
----------------------IMPLEMENTADO UN MODELO MULTIDIMENSIONAL-----------------
------------------------------------------------------------------------------


--1) Creamos la Base DATAMART_VENTAS
USE MASTER
GO
IF exists( select name from sysdatabases 
           where name='DM_VENTAS')
DROP DATABASE DM_VENTAS
GO
CREATE DATABASE DM_VENTAS
ON PRIMARY
(NAME='DM_VENTAS_DAT',
 FILENAME='D:\SQL\DM_VENTAS_DAT.mdf',
 SIZE=30MB,
 MAXSIZE=UNLIMITED,
 FILEGROWTH=20%
 )
 LOG ON
 (NAME='DM_VENTAS_LOG',
  FILENAME='D:\SQL\DM_VENTAS_LOG.ldf',
  SIZE=10MB,
  MAXSIZE=UNLIMITED,
  FILEGROWTH=10%)
  GO

--2) Creaci�n del Modelo Multidimensional
USE DM_VENTAS
GO

--2.1) Creaci�n de las Dimensiones
CREATE TABLE [dbo].[Dimension_CategoriaProductos](
	[ProductoCategoriaPK] [int] IDENTITY(1,1) NOT NULL,
	[ProductCategoryKey] [int] NULL,
	[EnglishProductCategoryName] [nvarchar](255) NULL,
 CONSTRAINT [PK_Dimension_categoriaProductos] PRIMARY KEY CLUSTERED 
(
	[ProductoCategoriaPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Dimension_Clientes](
	[CustomerPK] [int] IDENTITY(1,1) NOT NULL,
	[CustomerKey] [int] NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
	[BirthDate] [datetime] NULL,
	[Gender] [nvarchar](255) NULL,
	[MaritalStatus] [nvarchar](255) NULL,
 CONSTRAINT [PK_Dimension_Clientes] PRIMARY KEY CLUSTERED 
(
	[CustomerPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Dimension_Distrito](
	[DistritoPK] [int] IDENTITY(1,1) NOT NULL,
	[DistritoKey] [int] NULL,
	[NombreDistrito] NVARCHAR(255) NULL,
 CONSTRAINT [PK_Dimension_Distrito] PRIMARY KEY CLUSTERED 
(
	[DistritoPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[Dimension_Productos](
	[ProductoPK] [int] IDENTITY(1,1) NOT NULL,
	[ProductKey] [int] NULL,
	[EnglishProductName] [nvarchar](255) NULL,
 CONSTRAINT [PK_Dimension_Productos] PRIMARY KEY CLUSTERED 
(
	[ProductoPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Dimension_Promocion](
	[PromocionPK] [int] IDENTITY(1,1) NOT NULL,
	[PromotionKey] [int] NULL,
	[EnglishPromotionName] [nvarchar](255) NULL,
 CONSTRAINT [PK_Dimension_Promocion] PRIMARY KEY CLUSTERED 
(
	[PromocionPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Dimension_SubCategoriaProducto](
	[ProductSubCategoryPK] [int] IDENTITY(1,1) NOT NULL,
	[ProductSubCategoryKey] [int] NULL,
	[EnglishProductSubCategoryName] [nvarchar](255) NULL,
 CONSTRAINT [PK_Dimension_subcategoriaproductos] PRIMARY KEY CLUSTERED 
(
	[ProductSubCategoryPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[Dimension_Territorio](
	[SalesTerritoryPK] [int] IDENTITY(1,1) NOT NULL,
	[SalesTerritoryKey] [int] NULL,
	[SalesTerritoryRegion] [nvarchar](255) NULL,
	[SalesTerritoryCountry] [nvarchar](255) NULL,
	[SalesTerritoryGroup] [nvarchar](255) NULL,
 CONSTRAINT [PK_Dimension_Territorio] PRIMARY KEY CLUSTERED 
(
	[SalesTerritoryPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Dimension_TipoCambio](
	[CurrencyPK] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyKey] [int] NULL,
	[CurrencyName] [nvarchar](255) NULL,
 CONSTRAINT [PK_Dimension_TipoCambio] PRIMARY KEY CLUSTERED 
(
	[CurrencyPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--2.2) Creaci�n de Tabla de Hechos o Mediciones

CREATE TABLE [dbo].[Fact_Table](
	[ProductPK] [int] NOT NULL,
	[CustomerPK] [int] NOT NULL,
	[PromocionPK] [int] NOT NULL,
	[CurrencyPK] [int] NOT NULL,
	[SalesTerritoryPK] [int] NOT NULL,
	[OrderDateKey] [int] NOT NULL,
	[DistritoPK] [int] NOT NULL,
	[ProductoSubCategoriaPK] [int] NOT NULL,
	[ProductoCategoriaPK] [int] NOT NULL,
	[SalesOrderNumber] [nvarchar](255) NULL,
	[SalesAmount] [float] NULL,
	[TaxAmt] [float] NULL,
 CONSTRAINT [PK_Fact_Table] PRIMARY KEY CLUSTERED 
(
	[ProductPK] ASC,
	[CustomerPK] ASC,
	[PromocionPK] ASC,
	[CurrencyPK] ASC,
	[DistritoPK] ASC,
	[SalesTerritoryPK] ASC,
	[OrderDateKey] ASC,
	[ProductoSubCategoriaPK] ASC,
	[ProductoCategoriaPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--3) Creaci�n de Relaciones

ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Table_Dimension_CategoriaProductos] FOREIGN KEY([ProductoCategoriaPK])
REFERENCES [dbo].[Dimension_CategoriaProductos] ([ProductoCategoriaPK])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Fact_Table_Dimension_CategoriaProductos]
GO
ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Table_Dimension_Clientes] FOREIGN KEY([CustomerPK])
REFERENCES [dbo].[Dimension_Clientes] ([CustomerPK])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Fact_Table_Dimension_Clientes]
GO
ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Table_Dimension_Distrito] FOREIGN KEY([DistritoPK])
REFERENCES [dbo].[Dimension_Distrito] ([DistritoPK])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Fact_Table_Dimension_Distrito]
GO
ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Table_Dimension_Productos] FOREIGN KEY([ProductPK])
REFERENCES [dbo].[Dimension_Productos] ([ProductoPK])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Fact_Table_Dimension_Productos]
GO
ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Table_Dimension_Promocion] FOREIGN KEY([PromocionPK])
REFERENCES [dbo].[Dimension_Promocion] ([PromocionPK])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Fact_Table_Dimension_Promocion]
GO
ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Table_Dimension_SubCategoriaProductos] FOREIGN KEY([ProductoSubCategoriaPK])
REFERENCES [dbo].[Dimension_SubCategoriaProducto] ([ProductSubCategoryPK])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Fact_Table_Dimension_SubCategoriaProductos]
GO
ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Table_Dimension_Territorio] FOREIGN KEY([SalesTerritoryPK])
REFERENCES [dbo].[Dimension_Territorio] ([SalesTerritoryPK])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Fact_Table_Dimension_Territorio]
GO
ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Table_Dimension_TipoCambio] FOREIGN KEY([CurrencyPK])
REFERENCES [dbo].[Dimension_TipoCambio] ([CurrencyPK])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Fact_Table_Dimension_TipoCambio]
GO


--4) Agregar permisos de Sysadmin a la BD para la diagramaci�n

ALTER AUTHORIZATION ON DATABASE::[DM_VENTAS] to [SA]
--5) Agregar la Dimensi�n Fecha con Data  xls (Importando)

--6) Alteramos los Tipos de Dato de la Columna para 

BEGIN
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [DateKey] int not null
ALTER TABLE DIMENSION_FECHA  ALTER COLUMN [daynumberofweek] tinyint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [DayNumberOfMonth] tinyint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [DayNumberOfYear] smallint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [WeekNumberOfYear] tinyint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [MonthNumberOfYear] tinyint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [CalendarQuarter] tinyint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [CalendarYear] smallint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [CalendarSemester] tinyint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [FiscalQuarter] tinyint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [FiscalSemester] tinyint
ALTER TABLE DIMENSION_FECHA ALTER COLUMN [FiscalYear] smallint
END

-- 6.1)Agregamos la LLave primaria a la Dimensi�n Fecha
ALTER TABLE DIMENSION_FECHA ADD PRIMARY KEY ([DateKey]) 
-- 6.2) Agregamos la LLave for�nea a la Fact Table 
ALTER TABLE FACT_TABLE ADD FOREIGN key (orderDateKey) references Dimension_fecha(DateKey)


SELECT*fROM TIPO_CAMBIO

--7) Importamos nuestra tabla Desnormalizada de Ventas_Total (con informaci�n de todas las Tablas)

select*From VENTAS_TOTAL

-- ALterando las columnas de las LLaves principales
ALTER TABLE VENTAS_TOTAL ALTER COLUMN ProductKey INT NOT NULL
ALTER TABLE VENTAS_TOTAL ALTER COLUMN ProductSubcategoryKey INT NOT NULL
ALTER TABLE VENTAS_TOTAL ALTER COLUMN DistritoKey INT NOT NULL
ALTER TABLE VENTAS_TOTAL ALTER COLUMN ProductCategoryKey INT NOT NULL
ALTER TABLE VENTAS_TOTAL ALTER COLUMN CustomerKey INT NOT NULL
ALTER TABLE VENTAS_TOTAL ALTER COLUMN PromotionKey INT NOT NULL
ALTER TABLE VENTAS_TOTAL ALTER COLUMN CurrencyKey INT NOT NULL
ALTER TABLE VENTAS_TOTAL ALTER COLUMN SalesTerritoryKey INT NOT NULL

--8) Creamos un Paquete Services Integration (ETL)   en SQL DATA TOOLS

--8.1)  Creamos un flujo con Limpieza de Datos QUERY LIMPIEZA

USE DM_VENTAS
GO

TRUNCATE TABLE VENTAS_TOTAL

--8.2) Creamos un contenedor con los STAGE por cada Dimensi�n en el Modelo 


--8.3) Agregando los QUerys STAGE
select distinct ProductCategoryKey,EnglishProductCategoryName from VENTAS_TOTAL

select distinct Customerkey,firstname,lastname,birthDate,gender,MaritalStatus from VENTAS_TOTAL

SELECT DISTINCT DistritoKey,NombreDistrito from VENTAS_TOTAL

select distinct ProductKey,EnglishProductName from VENTAS_TOTAL

select distinct PromotionKey,EnglishPromotionName from VENTAS_TOTAL

Select DISTINCT ProductSubcategoryKey,EnglishProductSubCategoryName from VENTAS_TOTAL

Select distinct SalesTerritoryKey,SalesTerritoryRegion,SalesTerritoryCountry,SalesTerritoryGroup from VENTAS_TOTAL

select distinct CurrencyKey,CurrencyName from VENTAS_TOTAL

--8.4) Agregamos los  TRUNCATE para los STAGEs  en el QUERY LIMPIEZA

TRUNCATE TABLE [dbo].[STG_CATEORIA]
TRUNCATE TABLE [dbo].[STG_CLIENTE]
TRUNCATE TABLE [dbo].[STG_DISTRITO]
TRUNCATE TABLE [dbo].[STG_PRODUCTO]
TRUNCATE TABLE [dbo].[STG_PROMOCION]
TRUNCATE TABLE [dbo].[STG_SUBCATEGORIA]
TRUNCATE TABLE [dbo].[STG_TERRITORIO]
TRUNCATE TABLE [dbo].[TIPO_CAMBIO]


--8.5) Creamos un contenedor para las Dimensiones   
--8.6) Agregamos  la eliminaci�n de las dimensiones en el Query de Limpieza Inicial


DELETE FROM [dbo].[Dimension_CategoriaProductos]
DELETE FROM [dbo].[Dimension_Distrito]
DELETE FROM [dbo].[Dimension_Clientes]
DELETE FROM [dbo].[Dimension_Productos]
DELETE FROM [dbo].[Dimension_Promocion]
DELETE FROM [dbo].[Dimension_SubCategoriaProducto]
DELETE FROM [dbo].[Dimension_Territorio]
DELETE FROM [dbo].[Dimension_TipoCambio]
DELETE FROM [dbo].[Fact_Table]


--8.9) Creamos un Flujo LookUP para reemplazar las LLaves Key por PK (Autogeneradas) 
-- Y AS� MISMO POBLAMOS LA DATA FACT_TABLE

--9) Revisi�n de  la Fact Table 
SELECT*fROM [dbo].[Fact_Table]













