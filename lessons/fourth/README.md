Cuarta clase $`13/05/2020`$
---------------------------

[CLS04.sql](CLS04.sql)

[CLS04.ipynb](CLS04.ipynb)

![Preparación](../../study/videos/spanish/4_preparation.mp4)

![Primera parte](../../study/videos/spanish/4_firstPart.mp4)

![Segunda parte](../../study/videos/spanish/4_secondPart.mp4)

![Tercera parte](../../study/videos/spanish/4_thirdPart.mp4)

![Cuarta parte](../../study/videos/spanish/4_fourthPart.mp4)

![Quinta parte](../../study/videos/spanish/4_fifthPart.mp4)

![Sexta parte](../../study/videos/spanish/4_sixthPart.mp4)