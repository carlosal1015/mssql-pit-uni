Lessons
-------

* [First lesson](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/first)
* [Second lesson](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/second)
* [Third lesson](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/third)
* [Fourth lesson](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/fourth)
* [Fifth lesson](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/fifth)
* [Sixth lesson](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/sixth)
* [Seventh lesson](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/seventh)
* [Eighth lesson](https://gitlab.com/carlosal1015/mssql-pit-uni/tree/master/lessons/eighth)